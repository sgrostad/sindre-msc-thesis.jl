# Automatic Differentiation in Julia with Applications to Numerical Solution of PDEs

This README will explain the folder structure of my master's thesis and make it easier to quickly run and find the code you are interested in.

## Getting Started

* All scripts assume the path to all the modules it imports are added to the *LOAD_PATH* variable. This is best done by adding **push!(LOAD_PATH, "YOUR_PATH")** into the startup file to julia (startup.jl)
* All scripts also assume that the Julia packages they use are already installed. If not, run **Pkg.add("PACKAGE_NAME")**.
* A full copy of MRST 2018a is added to the root folder.  This is to easier call the MRST code from Julia.
* I have not found a way to call MATLAB functions that lie inside subfolders. Hence, all the MATLAB functions I call from Julia lie in the root folder sindre-msc-thesis.jl. These functions the call other MATLAB code that lie in subfolders.
* All scripts that call MATLAB code need to first initialize MRST. This should already be implemented for all scripts, but if not, it is activated with the call **mxcall(:runMatlabCode, 1, "initializeMRST")**

## sindre.msc-thesis.jl folder
The root folder. Contains four subfolders described below and MATLAB files called from Julia. The most interesting folders are probably **2phase_solver** and **flow_solver** inside **master_thesis**.

All files that are not modules or part of a module should be possible to run directly. Some files have different functions that do different things. Which function that runs is always decided in the bottom of the file. The other available function calls are commented out.

### master_thesis
The folder containing all the code created during writing the master thesis.
The main subfolders are *custom_jacobian_AD* with the module for CustomJacobianAD, *local_AD* with the module for local AD, and *2phase_solver* containing all the implementations for all the
#### 2phase_solver
Probably the most interesting folder. There are some files that are less important and I will only mention the main ones:
- *TwoPhaseSystems.jl* is the module that uses local_AD to assemble the residual equations for the two-phase solver.
- *TwoPhase2D.jl* First simple simulation of two-phase flow. 10x10 gris with well and injector in each corner
- *testTwoPhaseSolver.jl* test function that checks that the first calculated values and Jacobians in the simulation from *TwoPhase2D.jl* coincide with what MATLAB calculates.
- *spe10SimulationCJAD.jl* the two-phase solver for the spe10 grid using CustomJacobianAD.
- *spe10SimulationLocalAD.jl* the two-phase solver for the spe10 grid using local AD.
- *benchmarkSPE10Assembly* benchmark test of assembling of the spe10 residual equations.

#### benchmarks
Contains a benchmark test of AD for a simple vector function
#### custom_jacobian_AD
Contains the module for the CustomJacobianAD tool. The test file *testCustomJacobianAD.jl* is also provided.
#### flow_solver
- *benchmarkFlowSolverAssembly.jl* contains benchmark test for only assembling of the single-phase residual equations.
- *benchmarkFlowSolvers.jl* contains benchmark test for the full single-phase simulation
- *flowSolverCJAD.jl* script performing the single-phase simulation with CustomJacobianAD
- *flowSolverLocalAD.jl* script performing the single-phase simulation with local_AD
- *FlowSystem.jl* is the module that uses local_AD to assemble the residual equations for the flow solver.
- *SetupFlowSolver* contains help functions to the benchmark tests

#### jld_files
Contains saved Julia variables, mainly used for testing
#### local_AD
Contains the module performing local_AD and the *testLocalAD.jl* test file which uses the module with FlowSystem to assemble the residual equations from flow_solver.
#### mat_files
Contains saved MATLAB variables, mainly used for testing

### specialization_project
Code created in the specialization project building up to the master thesis.
* Contains one AD implementation in *ForwardAutoDiff.jl* with corresponding test file in *testForwardAutoDiff.jl*.
* Single-phase flow solver is implemented using ForwardAutoDiff in *flowSolverForwardAutoDiff.jl* and using forwardDiff in *flowSolverForwardDiff.jl*.
* Test of AD speed solving the single-phase solver can be found in *benchmarkFlowSolver.jl* and for a simple vector function in *benchmarkAD.jl*.

### calling_cpp
Not used in the thesis, however I left it there in case someone find it interesting how to call C from Julia. *testCallingC.jl* has a summary (as a comment) of what I found out.

### mrst-2018a
Just a full copy of MRST 2018a. Some small modifications has been made. For example not printing full startup message and some other small changes.

### trash
Not necessarily trash, but code that have been used earlier, but are no longer in use.

## Author

**Sindre Grøstad** - *Master's Thesis*
