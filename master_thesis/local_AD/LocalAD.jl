module LocalAD
export LAD, createVariable, NUM_DERIV
using StaticArrays
const NUM_DERIV = 1
struct LAD
    val::Float64
    derivatives::SVector{NUM_DERIV, Float64}
end

function createVariable(val::Number, derivativeIndex::Int)
    derivatives = @SVector [if i == derivativeIndex 1 else 0 end for i = 1:NUM_DERIV]
    return LAD(val, derivatives)
end
function createVariable(val::MArray{Tuple{1},Float64,1,1}, derivativeIndex::Int)
    return createVariable(val[1], derivativeIndex)
end

import Base: +
function +(A::LAD, B::LAD)
    return LAD(A.val + B.val, A.derivatives .+ B.derivatives)
end
+(A::LAD, B::Number) = LAD(A.val + B, A.derivatives)
+(A::Number, B::LAD) = B + A

import Base: -
function -(A::LAD, B::LAD)
    return LAD(A.val - B.val, A.derivatives .- B.derivatives)
end
-(A::LocalAD.LAD) = LAD(-A.val, -A.derivatives)
-(A::LAD, B::Number) = LAD(A.val - B, A.derivatives)
-(A::Number, B::LAD) = LAD(A - B.val, -B.derivatives)

import Base: *
function *(A::LAD, B::LAD)
    return LAD(A.val * B.val, @.(A.val * B.derivatives + B.val * A.derivatives))
end
*(A::LAD, B::Number) = LAD(A.val * B, B .* A.derivatives)
*(A::Number, B::LAD) = B * A

import Base: /
function /(A::LAD, B::LAD)
    return LAD(A.val / B.val, @.(B.val * A.derivatives - A.val * B.derivatives)./
    B.val^2)
end
/(A::LAD, B::Number) = LAD(A.val / B, A.derivatives ./ B)
/(A::Number, B::LAD) = A * B^(-1)

import Base: ^
^(A::LAD, k::Number) = LAD(A.val^k, k * A.val^(k-1) .* A.derivatives)

import Base: inv
inv(A::LAD) = LAD(A.val^(-1), -A.val^(-2) .* A.derivatives)

import Base: exp
exp(A::LAD) = LAD(exp(A.val), exp(A.val) .* A.derivatives)

end  # module LocalAD
