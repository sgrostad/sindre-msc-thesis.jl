module SimpleLocalAD
export SimpleLAD, createSimpleVariable

struct SimpleLAD
    val::Float64
    derivatives::Float64
end

function createSimpleVariable(val::Number, numDerivatives::Int, derivativeIndex::Int)
    derivatives = 0
    if derivativeIndex <= numDerivatives && derivativeIndex > 0
        derivatives = 1
    end
    return SimpleLAD(val, derivatives)
end

import Base: +
function +(A::SimpleLAD, B::SimpleLAD)
    return SimpleLAD(A.val + B.val, A.derivatives + B.derivatives)
end
+(A::SimpleLAD, B::Number) = SimpleLAD(A.val + B, A.derivatives)
+(A::Number, B::SimpleLAD) = B + A

import Base: -
function -(A::SimpleLAD, B::SimpleLAD)
    return SimpleLAD(A.val - B.val, A.derivatives - B.derivatives)
end
-(A::SimpleLAD, B::Number) = SimpleLAD(A.val - B, A.derivatives)
-(A::Number, B::SimpleLAD) = SimpleLAD(A - B.val, -B.derivatives)

import Base: *
function *(A::SimpleLAD, B::SimpleLAD)
    return SimpleLAD(A.val * B.val, (A.val * B.derivatives + B.val * A.derivatives))
end
*(A::SimpleLAD, B::Number) = SimpleLAD(A.val * B, B * A.derivatives)
*(A::Number, B::SimpleLAD) = B * A

import Base: /
function /(A::SimpleLAD, B::SimpleLAD)
    return SimpleLAD(A.val / B.val, (B.val * A.derivatives - A.val * B.derivatives)/
    B.val^2)
end
/(A::SimpleLAD, B::Number) = SimpleLAD(A.val / B, A.derivatives / B)
/(A::Number, B::SimpleLAD) = A * B^(-1)

import Base: ^
^(A::SimpleLAD, k::Number) = SimpleLAD(A.val^k, k * A.val^(k-1) * A.derivatives)

import Base: inv
inv(A::SimpleLAD) = SimpleLAD(A.val^(-1), -A.val^(-2) * A.derivatives)

import Base: exp
exp(A::SimpleLAD) = SimpleLAD(exp(A.val), exp(A.val) * A.derivatives)
end  # module SimpleLocalAD
