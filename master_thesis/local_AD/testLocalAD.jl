using Test, LocalAD, MATLAB, SetupFlowSolver
using FlowSystems, WellParameters
mxcall(:runMatlabCode, 1, "initializeMRST");
function tests()
    @testset "testing LocalAD.jl:" begin
        @testset "test full FlowSolver" begin
            mf = read_matfile("master_thesis/mat_files/flowSolverFirstValAndJac.mat");
            corrVal = jvector(mf["res"]);
            corrJac = jsparse(mf["J"]);
            dt = jscalar(mf["dt"])
            (dz, WI, wc, np, gradz, N, C, T, nf, cf, hT, p_init, pv_r, G) = setupFlowSolverLocalAD(1);
            well = WellPar(WI, wc, dz);
            fs = FlowSystem(p_init, p_init[wc[1]], 0.0, N, T, gradz, well, pv_r[1]);
            assembleFlowSystem!(fs, p_init, dt, well)
            @test fs.eqVal ≈ corrVal
            @test fs.globalJac ≈ corrJac
        end
    end
end
tests()
