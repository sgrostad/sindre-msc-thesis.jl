module ImportMatFiles
using MATLAB
# Line below needs to run each time you begin running matlab scripts/functions
# mxcall(:runMatlabCode, 1, "initializeMRST");
export f, matFileToVariables

function importMatFile(filename::String)
    return read_matfile(filename)
end

@eval function matFileToVariables(filename::AbstractString)
    d = read_matfile(Symbol(filename))
    $([:($(Symbol(key)) = $i) for (key,val) in d]...)
    ($([Symbol(key) for (key,val) in d]...),)
end

@eval function f(filename::String)
           $([:($(Symbol("x_$i")) = $i) for i in 1:10]...)
           ($([Symbol("x_$i") for i in 1:10]...),)
       end
end  # module ImportMatFiles
