using ForwardAutoDiff, ForwardDiff, CustomJacobianAD
using LinearAlgebra, Plots, Statistics, MATLAB
using BenchmarkTools, SparseArrays
# Line below needs to run each time you begin running matlab-scripts/functions
mxcall(:runMatlabCode, 1, "initializeMRST");

function analytic_solution(x,y,z)
    f = exp.(2*x.*y) - 4*x.*z.^2 + 13*x .- 7
    fx = 2*y.*exp.(2*x.*y) - 4*z.^2 .+ 13
    fy = 2*x.*exp.(2*x.*y)
    fz = -8*x.*z
end
function FAD(xi,yi,zi)
    (x_ad,y_ad,z_ad) = initialize_AD(xi,yi,zi)
    f(x,y,z) = exp(2*x*y) - 4*x*z^2 + 13*x - 7
    f_ad = f(x_ad,y_ad,z_ad)
end
function CJAD(xi,yi,zi)
    (x_ad,y_ad,z_ad) = initialize_CJAD(xi,yi,zi)
    f(x,y,z) = exp(2*x*y) - 4*x*z^2 + 13*x - 7
    f_ad = f(x_ad,y_ad,z_ad)
end
function forward_diff(xi,yi,zi)
    n = length(xi)
    xIndx = 1:n
    yIndx = (n+1):2*n
    zIndx = (2*n+1):3*n
    f(in) = exp.(2*in[xIndx].*in[yIndx]) - 4*in[xIndx].*in[zIndx].^2 +
        13*in[xIndx] .- 7
    val = f([xi; yi; zi])
    jac = ForwardDiff.jacobian(f,[xi; yi; zi])
end

## Benchmarks:
function benchmark_all_AD()
    nanoseconds = 10^(-9)
    k = 14
    lengthVectors = zeros(Int64, k)
    recordedTimes = zeros(k,6)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 0.3
    numFDTest = 9
    for i = 1:k
        println("i = ", i)
        lengthVectors[i] = 3^(i-1)
        x = rand(lengthVectors[i])
        y = rand(lengthVectors[i])
        z = rand(lengthVectors[i])
        if i <= numFDTest
            recordedTimes[i,1] =
            median(@benchmark forward_diff($x,$y,$z)).time * nanoseconds
        end
        recordedTimes[i,2] =
        median(@benchmark FAD($x,$y,$z)).time * nanoseconds
        recordedTimes[i,3] =
        median(@benchmark CJAD($x,$y,$z)).time * nanoseconds
        recordedTimes[i,6] =
        median(@benchmark analytic_solution($x,$y,$z)).time * nanoseconds
    end
    println("Matlab:")
    matlabRes = mxcall(:benchmark_mrst_ad, 1, mxarray(lengthVectors))
    recordedTimes[:,4:5] = matlabRes
    p1 = plot(lengthVectors[1:numFDTest],
    recordedTimes[1:numFDTest,1], label = "ForwardDiff", shape = [:circle])
    plot!(lengthVectors, recordedTimes[:,2:end], label=["FAD","CJAD","MRST",
    "MRST_diagonal", "Analytic"], legend=:topleft, shape = [:circle])
    xaxis!("Length vectors",:log10)
    yaxis!("Time [s]",:log10)

    j = numFDTest + 1
    p2 = plot(lengthVectors[j:end], recordedTimes[j:end,2:end], label=["FAD","CJAD",
    "MRST","MRST_diagonal", "Analytic"], legend=:topleft, shape = [:circle])
    xaxis!("Length vectors",:log10)
    yaxis!("Time [s]",:log10)
    return p1, p2, lengthVectors, recordedTimes
end

function long_vectors_benchmark()
    nanoseconds = 10^(-9)
    k = 15
    lengthVectors = zeros(Int64, k)
    recordedTimes = zeros(k,4)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 0.3
    for i = 1:k
        println("i = ", i)
        lengthVectors[i] = 3^(i-1)
        x = rand(lengthVectors[i])
        y = rand(lengthVectors[i])
        z = rand(lengthVectors[i])
        recordedTimes[i,1] = median(@benchmark analytic_solution($x,$y,$z)).time *
            nanoseconds
        recordedTimes[i,2] = median(@benchmark forward_auto_diff($x,$y,$z)).time *
            nanoseconds
    end
    println("Matlab:")
    matlabRes = mxcall(:benchmark_mrst_ad, 1, mxarray(lengthVectors))
    recordedTimes[:,3] = matlabRes[:,1]
    recordedTimes[:,4] = matlabRes[:,2]
    p = plot(lengthVectors, recordedTimes, label=["Analytic", "ForwardAutoDiff",
        "MRST old", "MRST new"], legend=:topleft, shape = [:circle])
    xaxis!("Length vectors",:log10)
    yaxis!("Time [s]",:log10)
    return p
end


p1, p2, lengthVectors, recordedTimes = benchmark_all_AD()
p1
p2
#p = for_loop_benchmark()
#p = long_vectors_benchmark()
