function res = benchmark_mrst_ad(lengthVectors)
    mrstModule add ad-core
    res = zeros(length(lengthVectors),2);
    res(:,1) = do_benchmark(lengthVectors,false);
    res(:,2) = do_benchmark(lengthVectors,true);
end

function res = do_benchmark(lengthVectors, diagonal)
    repetitions = 10;
    res = zeros(length(lengthVectors),1);
    for i = 1:length(lengthVectors)
        x = rand(lengthVectors(i),1);
        y = rand(lengthVectors(i),1);
        z = rand(lengthVectors(i),1);
        tempRes = zeros(repetitions,1);
        for j = 1:repetitions
            tic
            compute_function(x,y,z, diagonal)
            tempRes(j) = toc;
        end
        res(i) = median(tempRes);
    end
end

function compute_function(x, y, z, new)
    if new
        [x_ad,y_ad,z_ad] = initVariablesAD_diagonal(x,y,z);
    else
        [x_ad,y_ad,z_ad] = initVariablesADI(x,y,z);
    end
    f = @(x,y,z) exp(2*x.*y) - 4*x.*z.^2 + 13*x - 7;
    f(x_ad, y_ad, z_ad);
end
