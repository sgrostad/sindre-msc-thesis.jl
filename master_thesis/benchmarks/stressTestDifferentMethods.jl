using CustomJacobianAD, SparseArrays, BenchmarkTools
using SetupFlowSolver

############
#= Old is best:
New:
  51.495 ms (170000 allocations: 166.47 MiB)
Old:
  16.484 ms (130000 allocations: 57.07 MiB)
=#
function change_jacobian_old(A::NullJac, B::SparseJac, ix::AbstractVector{Int})
    newJac = spzeros(A.nRows, A.nCols)
    newJac[ix,:] = B.jac
    return SparseJac(newJac)
end

function change_jacobian_new(A::NullJac, B::SparseJac, ix::AbstractVector{Int})
    (oldRow, col, val) = findnz(B.jac)
    newRow = [ix[oldRow[i]] for i = 1:length(oldRow)]
    return SparseJac(sparse(newRow, col, val, A.nRows, A.nCols))
end

##############
function change_jacobian_old(A::SparseJac, B::SparseJac, ix::AbstractVector{Int})
    newJac = copy(A.jac)
    newJac[ix,:] = B.jac
    return SparseJac(newJac)
end

function change_jacobian_new(A::SparseJac, B::SparseJac, ix::AbstractVector{Int})
    A.jac[ix,:] = B.jac
    return A
end
#######

function loop(func, variables, rep)
    for i = 1:rep
        func(variables)
    end
end
function loopWithDot(func, variables, rep)
    for i = 1:rep
        func.(variables)
    end
end

function stressTestMultiplicationSparseJacAndVector()
    #=
    new:
    newJac = B .* A.jac
    old:
    diagIndex = 1:length(B)
    valDiag = sparse(diagIndex, diagIndex, B)
    newJac = valDiag * A.jac
    B*SparseJac
    =#
    jac = SparseJac(sprand(40,40,0.2))
    B = rand(40)
    if (B*jac).jac != B.*jac.jac
        error("NOT equal")
    end
    println("New:")
    newFunction(in) = in[1].*in[2]
    variables = [sparse(B), jac.jac]
    rep = 10000
    @btime loop($newFunction, $variables, $rep)
    println("Old:")
    variables = [B, jac.jac]
    oldFunction(in) = in[1]*in[2]
    variables = [B, jac]
    rep = 10000
    @btime loop($oldFunction, $variables, $rep)

    println("\nFlowSolver:")
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
    setup_flow_solver(2)
    (p_ad, bhp_ad, qS_ad) = initialize_CJAD(p_init, p_init[Int(wc[1])], 0)
    presEq_ad = presEq(p_ad, p_init, 0.1)
    jac = presEq_ad.customJacs[1]
    B = rand(8000)
    println("New:")
    newFunction(in) = in[1].*in[2]
    variables = [B, jac.jac]
    rep = 10
    @btime loop($newFunction, $variables, $rep)
    println("Old:")
    oldFunction(in) = in[1]*in[2]
    variables = [B, jac]
    @btime loop($oldFunction, $variables, $rep)
end

function stressTestDifferentSum()
    error("To run this test. Change sum in CustomJacobianAD to be named
    sumAlternative and export it.")
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
    setup_flow_solver(2)

    rep = 100
    (p_ad, bhp_ad, qS_ad) = initialize_CJAD(p_init, p_init[Int(wc[1])], 0)
    variables = [p_ad, bhp_ad]
    newFunction(in) = sumAlternative(q_conn(in[1],in[2]))
    oldFunction(in) = sum(q_conn(in[1],in[2]))
    if newFunction(variables) != oldFunction(variables)
        error("NOT equal")
    end
    println("FlowSolver test:")
    println("New:")
    @btime loop($newFunction, $variables, $rep)
    println("Old:")
    @btime loop($oldFunction, $variables, $rep)
    println("Random test:")
    nullJac = NullJac(400,400)
    identityJac = IdentityJac(400)
    diagJac = DiagJac(rand(400))
    sparseJac = SparseJac(sprand(400,400,0.3))
    customJacs = [nullJac, identityJac, diagJac, sparseJac]
    variables = CJAD(rand(400),customJacs)
    newFunction2(in) = sumAlternative(in)
    oldFunction2(in) = sum(in)
    println("New:")
    @btime loop($newFunction2, $variables, $1)
    println("Old:")
    @btime loop($oldFunction2, $variables, $1)
end

function vectorized(x, y, rep)
    z = similar(x)
    for i = 1:rep
        z = x .* y
    end
end
function devectorized(x, y, rep)
    z = similar(x)
    for i = 1:rep
        for j = 1:length(x)
            z[j] = x[j] .* y[j]
        end
    end
end

function stressTestVectorizedAndDevectorizedMultiplication()
    l = 10000
    x = rand(l)
    y = rand(l)
    rep = 10000
    println("\n\n\nVectorized code:")
    @btime vectorized($x, $y, $rep)
    println("Devectorized code:")
    @btime devectorized($x, $y, $rep)
end

function stressTestVecAndDevecAddition()
    l = 10000
    diagJac1 = rand(l)
    diagJac2 = rand(l)
    vectorized(in) = in[1]+in[2]
    function devectorized(in)
        jac = similar(in[1].jac)
        for i = 1:length(in[1].jac)
            jac[i] = in[1].jac[i] + in[2].jac[i]
        end
        return jac
    end
    jac1 = DiagJac(diagJac1)
    jac2 = DiagJac(diagJac2)
    variables = [jac1, jac2]
    rep = 10000
    println("\n\n\nVectorized code:")
    @btime loop($vectorized, $variables, $rep)
    #=println("Devectorized code:")
    @btime loop($devectorized, $variables, $rep)=#
end

function stressTestVecTimesCJADVectorization()
    #=Devectorized:
        219.703 ms (40000 allocations: 764.77 MiB)
        vectorized:
        198.280 ms (40000 allocations: 764.77 MiB)
        =#
    l = 10000
    val = rand(l)
    A = CJAD(val, [])
    B = rand(l)
    variables = [A, B]
    test(in) = in[2]*in[1]
    rep = 10000
    println("\n\n\nTest code:")
    @btime loop($test, $variables, $rep)
end

function stressTestDotOperator()
    f(x) = exp(x)
    g(y) = log(y)
    f_dot(x) = exp.(x)
    g_dot(y) = log.(y)
    dotsOutside(x) = f.(x) .* g.(x)
    dotsInside(x) = f_dot(x) .* g_dot(x)
    function fullyDevectorized(x)
        res = similar(x)
        for i = 1:length(x)
            res[i] = (exp(x[i])) * (log(x[i]))
        end
        return res
    end
    x = rand(100000)
    rep = 1000
    println("\n\n\nTest dotsOutside:")
    @btime loop($dotsOutside, $x, $rep)
    println("Test dotsInside:")
    @btime loop($dotsInside, $x, $rep)
    println("Test fullyDevectorized:")
    @btime loop($fullyDevectorized, $x, $rep)
end


#stressTestMultiplicationSparseJacAndVector()
#stressTestDifferentSum()
#stressTestVectorizedAndDevectorizedMultiplication()
#stressTestVecAndDevecAddition()
#stressTestVecTimesCJADVectorization()
stressTestDotOperator()
