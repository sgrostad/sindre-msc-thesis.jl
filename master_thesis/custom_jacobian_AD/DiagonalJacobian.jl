using SparseArrays
export DiagJac
import Base: size, -, ==, copy, getindex, setindex!

struct DiagJac <: CustomJac
    jac::Vector{Float64}
end

function size(A::DiagJac)
    return (length(A.jac), length(A.jac))
end

function size(A::DiagJac, dim::Int)
    if dim == 1 || dim == 2
        return length(A.jac)
    else
        error("Dimension choice of DiagJac can only be 1 or 2")
    end
end

expand_scalar_CJAD_jac(A::DiagJac, dimB) =
    SparseJac(ones(dimB).* A.jac)

function -(A::DiagJac)
    return DiagJac(-A.jac)
end

==(A::DiagJac, B::DiagJac) = A.jac ≈ B.jac

copy(A::DiagJac) = DiagJac(A.jac)

colSum(A::DiagJac) = SparseJac(sparse(ones(length(A.jac)), 1:length(A.jac), A.jac))

getindex(A::DiagJac, i::Int) = SparseJac(sparse([1], [i], [A.jac[i]], 1, size(A,2)))
getindex(A::DiagJac, v::AbstractVector{Int}) = SparseJac(sparse(1:length(v), v, A.jac[v], length(v), size(A,2)))

function faceAverage(A::DiagJac, N::AbstractArray)
    vals = 0.5 .* [A.jac[N[:,1]]; A.jac[N[:,2]]]
    indexCol = N[:]
    indexRow = repeat(1:size(N,1), outer=2)
    return SparseJac(sparse(indexRow, indexCol, vals))
end
