using SparseArrays, LinearAlgebra
export SparseJac
import Base: size, -, ==, copy, getindex, setindex!, convert

struct SparseJac <: CustomJac
    jac::SparseMatrixCSC{Float64,Int}
end

SparseJac(A::Vector) = SparseJac(sparse(A))
SparseJac(A::Float64) = SparseJac([A])
convert(::Type{SparseJac}, A::NullJac) = SparseJac(spzeros(A.nRows, A.nCols))
convert(::Type{SparseJac}, A::IdentityJac) = SparseJac(sparse(1.0I, A.size,
    A.size))
convert(::Type{SparseJac}, A::DiagJac) = SparseJac(sparse(1:size(A,1),
    1:size(A,2), A.jac))

function size(A::SparseJac)
    return size(A.jac)
end

function size(A::SparseJac, dim::Int)
    return size(A.jac, dim)
end

function expand_scalar_CJAD_jac(A::SparseJac, dimB)
     newJac = spzeros(dimB, size(A,2))
     for i = 1:dimB
         newJac[i,:] = A.jac
     end
     return SparseJac(newJac)
 end

function -(A::SparseJac)
    return SparseJac(-A.jac)
end

==(A::SparseJac, B::SparseJac) = A.jac ≈ B.jac

copy(A::SparseJac) = SparseJac(A.jac)

colSum(A::SparseJac) = SparseJac(ones(1,size(A,1)) * A.jac)

getindex(A::SparseJac, i::Int) = SparseJac(transpose(A.jac[i,:]))
getindex(A::SparseJac, v::AbstractVector{Int}) = SparseJac(A.jac[v,:])
