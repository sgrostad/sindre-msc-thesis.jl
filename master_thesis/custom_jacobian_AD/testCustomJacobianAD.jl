using CustomJacobianAD, Test, SparseArrays, LinearAlgebra

@testset "testing CustomJacobianAD.jl" begin
    @testset "initialization" begin
        x_init = collect(1.0:1000.0)
        y_init = 1001.0
        z_init = 1002.0
        (x, y, z) = initialize_CJAD(x_init, y_init, z_init)

        corrJacX = [IdentityJac(1000), NullJac(1000,1), NullJac(1000,1)]
        @test x == CJAD(x_init, corrJacX)

        corrJacY = [NullJac(1,1000), IdentityJac(1), NullJac(1,1)]
        @test y == CJAD(y_init, corrJacY)

        corrJacZ = [NullJac(1,1000), NullJac(1,1), IdentityJac(1)]
        @test z == CJAD(z_init, corrJacZ)

        @test x != y
        @test y != z
        @test z != x
    end

    x_init = collect(1:5)
    y_init = 6
    z_init = collect(7:11)
    (x, y, z) = initialize_CJAD(x_init, y_init, z_init)

    @testset "addition" begin
        corrJac = [DiagJac(vec(2*ones(5,1))), NullJac(5,1), NullJac(5,5)]
        @test x+x == CJAD(2 * x_init, corrJac)
        corrJac = [IdentityJac(5), NullJac(5,1), NullJac(5,5)]
        @test x+2 == CJAD(2 .+ x_init, corrJac)
        @test 2+x == CJAD(2 .+ x_init, corrJac)
        corrJac = [IdentityJac(5), SparseJac(2*ones(5,1)), NullJac(5,5)]
        @test x+y+y == CJAD(13:17, corrJac)
        @test y+y+x == CJAD(13:17, corrJac)
        corrJac = [IdentityJac(5), SparseJac(ones(5,1)), IdentityJac(5)]
        @test y+z+x == CJAD([14, 16, 18, 20, 22], corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [4, 5, 7, 9, 11]
        @test x + vector == CJAD(corrVal, x.customJacs)
        @test vector + x == CJAD(corrVal, x.customJacs)
    end

    @testset "subtraction" begin
        corrJac = [IdentityJac(5), NullJac(5,1), NullJac(5,5)]
        @test x-2 == CJAD(-1:3, corrJac)
        corrJac = [DiagJac(-ones(5)), NullJac(5,1), NullJac(5,5)]
        @test 2-x == CJAD(1:-1:-3, corrJac)
        corrJac = [IdentityJac(5), SparseJac(-2*ones(5)), NullJac(5,5)]
        @test x-y-y == CJAD(-11:-7, corrJac)
        corrJac = [DiagJac(-ones(5)), SparseJac(2*ones(5)), NullJac(5,5)]
        @test y+y-x == CJAD(11:-1:7, corrJac)
        corrJac = [IdentityJac(5), SparseJac(-ones(5,1)), DiagJac(-ones(5))]
        @test x-y-z == CJAD(-12 .* ones(5), corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [-2, -1, -1, -1, -1]
        @test x - vector == CJAD(corrVal, x.customJacs)
        corrVal = [2, 1, 1, 1, 1]
        @test vector - x == CJAD(corrVal, -x.customJacs)
    end

    @testset "multiplication" begin
        diagVal = [2, 4, 6, 8, 10]
        corrJac = [DiagJac(diagVal),
                NullJac(5,1), NullJac(5,5)]
        @test x*x == CJAD(x_init .* x_init, corrJac)
        corrJac = [DiagJac(6 * ones(5)), SparseJac(collect(1:5)), NullJac(5,5)]
        @test x*y == CJAD([6, 12, 18, 24, 30], corrJac)
        @test y*x == CJAD([6, 12, 18, 24, 30], corrJac)
        corrJac = [DiagJac(collect(7:11)), NullJac(5,1), DiagJac(collect(1:5))]
        @test x*z == CJAD([7, 16, 27, 40, 55], corrJac)
        @test z*x == CJAD([7, 16, 27, 40, 55], corrJac)
        corrJac = [DiagJac(collect(14:2:22)), NullJac(5,1), DiagJac(collect(2:2:10))]
        @test 2*x*z == CJAD(2 .* [7, 16, 27, 40, 55], corrJac)
        @test x*z*2 == CJAD(2 .* [7, 16, 27, 40, 55], corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [3, 6, 12, 20, 30]
        diagVal = [3, 3, 4, 5, 6]
        corrJac = [DiagJac(diagVal), NullJac(5,1),
                NullJac(5,5)]
        @test x * vector == CJAD(corrVal, corrJac)
        @test vector * x == CJAD(corrVal, corrJac)
        corrVal = [18, 18, 24, 30, 36]
        corrJac = [NullJac(5,5), SparseJac(sparse([3, 3, 4, 5, 6])),
                NullJac(5,5)]
        @test vector * y == CJAD(corrVal, corrJac)
        @test y * vector == CJAD(corrVal, corrJac)
        v = transpose(vector)
        m = [v; v; v; v; v]
        corrVal = ones(5)*71
        corrJacRow = v
        corrJac = [SparseJac([corrJacRow;corrJacRow;corrJacRow;
                corrJacRow;corrJacRow]), NullJac(5,1), NullJac(5,5)]
        @test m*x == CJAD(corrVal, corrJac)
        ##### Matrix multiplication not tested thoroughly. Not certain of how it
        # should behave, and what applications there are. Not optimized either.
    end

    @testset "power" begin
        corrVal = [1, 4, 9, 16, 25]
        corrJac = [DiagJac([2, 4, 6, 8, 10]), NullJac(5,1),
                NullJac(5,5)]
        @test x^2 == CJAD(corrVal, corrJac)
        corrVal = [1/6, 1/12, 1/18, 1/24, 1/30]
        corrJac1 = DiagJac([-0.166666666666667, -0.041666666666667,
                -0.018518518518519, -0.010416666666667, -0.006666666666667])
        corrJac2 = SparseJac([-0.027777777777778, -0.013888888888889, -0.009259259259259,
                -0.006944444444444, -0.005555555555556])
        corrJac = [corrJac1, corrJac2, NullJac(5,5)]
        @test (x*y)^(-1) == CJAD(corrVal, corrJac)
        corrVal = z_init.^0.5
        corrJac = [NullJac(5,5), NullJac(5,1), DiagJac(
                [0.188982236504614, 0.176776695296637, 0.166666666666667,
                0.158113883008419, 0.150755672288882])]
        @test z^0.5 == CJAD(corrVal, corrJac)
    end

    @testset "division" begin
        corrVal = [1, 1/2, 1/3, 1/4, 1/5]
        corrJac = [DiagJac([-1, -1/4, -1/9, -1/16, -1/25]),
                NullJac(5,1), NullJac(5,5)]
        @test 1/x == CJAD(corrVal, corrJac)
        corrVal = x_init./2
        corrJac[1] = DiagJac(0.5 .* ones(5))
        @test x/2 == CJAD(corrVal, corrJac)
        corrVal = [7, 4, 3, 2.5, 2.2]
        corrJac1 = DiagJac([-7, -2, -1, -0.625, -0.44])
        corrJac3 = DiagJac([1, 1/2, 1/3, 1/4, 1/5])
        corrJac = [corrJac1, NullJac(5,1), corrJac3]
        @test z/x == CJAD(corrVal, corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [1/3, 2/3, 3/4, 4/5, 5/6]
        diagVal = [1/3, 1/3, 1/4, 1/5, 1/6]
        corrJac = [DiagJac(diagVal), NullJac(5,1),
                NullJac(5,5)]
        @test x / vector == CJAD(corrVal, corrJac)
        corrVal = 1 ./ corrVal
        diagVal = -vector./x.val.^2
        corrJac = [DiagJac(diagVal), NullJac(5,1),
                NullJac(5,5)]
        @test vector / x == CJAD(corrVal, corrJac)
    end
    @testset "exp" begin
        corrVal = 4.034287934927351e+02
        corrJac = [NullJac(1,5), DiagJac([4.034287934927351e+02]), NullJac(1,5)]
        @test exp(y) == CJAD(corrVal, corrJac)
        (x2, y2, z2) = initialize_CJAD(-1:1, 1, -1.5:0.5:-0.5)
        corrVal = [4.48168907033806, 1, 0.606530659712633]
        corrJac1 = DiagJac([-6.722533605507097, -1, -0.303265329856317])
        corrJac2 = SparseJac([6.722533605507097, 0, -0.303265329856317])
        corrJac3 = DiagJac([-4.481689070338065, 0, 0.606530659712633])
        corrJac = [corrJac1, corrJac2, corrJac3]
        @test exp(x2*y2*z2) == CJAD(corrVal, corrJac)
    end

    @testset "== for Jacobians" begin
        @test !(NullJac(5,5) == IdentityJac(5))
        @test NullJac(3,3) == DiagJac([0, 0, 0])
        @test !(NullJac(3,3) == DiagJac([0, 0, 0, 1]))
        @test !(NullJac(3,3) == DiagJac([0, 0, 0, 0]))
        @test !(NullJac(3,3) == DiagJac([1, 0, 0]))
        @test NullJac(3,3) == SparseJac(spzeros(3,3))
        @test !(NullJac(3,5) == SparseJac(spzeros(3,4)))
        @test !(NullJac(4,3) == SparseJac(spzeros(3,3)))
        @test !(NullJac(4,3) == SparseJac([0 0 1 0; 0 0 0 0; 0 0 0 0]))
        @test IdentityJac(3) == DiagJac([1, 1, 1])
        @test !(IdentityJac(3) == DiagJac([0, 1, 1]))
        @test IdentityJac(4) == SparseJac(sparse(I,4,4))
        @test !(IdentityJac(3) == SparseJac([1 0 1; 0 1 0; 0 0 1]))
        @test !(IdentityJac(3) == SparseJac([1 0 1; 0 1 0]))
        @test !(SparseJac([1 0 0; 0 1 0; 0 0 0]) == IdentityJac(3))
        @test DiagJac([1,2,3]) == SparseJac([1 0 0; 0 2 0; 0 0 3])
        @test !(DiagJac([1,2,3]) == SparseJac([1 0 1; 0 2 0; 0 0 3]))
        @test !(DiagJac([1,2,3]) == SparseJac([1 0 0; 0 2 0]))
    end

    @testset "getindex Jacobians" begin
        nullJac = x.customJacs[3]
        @test nullJac[3] == NullJac(1,5)
        @test nullJac[3:5] == NullJac(3,5)
        @test nullJac[[1,2,4,5]] == NullJac(4,5)
        identityJac = x.customJacs[1]
        @test identityJac[4] == SparseJac([0 0 0 1 0])
        @test identityJac[4:5] == SparseJac([0 0 0 1 0; 0 0 0 0 1])
        @test identityJac[[1,3,4]] == SparseJac([1 0 0 0 0; 0 0 1 0 0; 0 0 0 1 0])
        diagJac = (x+x).customJacs[1]
        @test diagJac[2] == SparseJac([0 2 0 0 0])
        @test diagJac[1:3] == SparseJac([2 0 0 0 0; 0 2 0 0 0; 0 0 2 0 0])
        @test diagJac[[1,3,5]] == SparseJac([2 0 0 0 0; 0 0 2 0 0; 0 0 0 0 2])
        sparseJac = SparseJac([0 0 3; 4 0 6; 7 0 0])
        @test sparseJac[1] == SparseJac([0 0 3])
        @test sparseJac[2:3] == SparseJac([4 0 6; 7 0 0])
        @test sparseJac[[1,3]] == SparseJac([0 0 3; 7 0 0])
    end

    @testset "getindex CJAD" begin
        corrJac = [SparseJac([0 0 0 1 0]), NullJac(1,1), NullJac(1,5)]
        @test x[4] == CJAD(4, corrJac)
        corrVal = [2,3]
        corrJac = [SparseJac([0 1 0 0 0; 0 0 1 0 0]), NullJac(2,1), NullJac(2,5)]
        @test x[2:3] == CJAD(corrVal, corrJac)
        corrVal = [1, 3, 5]
        corrJac = [SparseJac([1 0 0 0 0; 0 0 1 0 0; 0 0 0 0 1]),NullJac(3,1),
            NullJac(3,5)]
        @test x[corrVal] == CJAD(corrVal, corrJac)
    end

    @testset "change_jacobian single index" begin
        A = NullJac(3,3)
        B = NullJac(1,3)
        @test change_jacobian(A,B,2) == NullJac(3,3)
        B = SparseJac([0 0 1])
        corrJac = spzeros(3,3)
        corrJac[3,3] = 1
        @test change_jacobian(A,B,3) == SparseJac(corrJac)
        A = IdentityJac(3)
        B = NullJac(1,3)
        corrJac = sparse(I,3,3)
        corrJac[1,1] = 0
        @test change_jacobian(A,B,1) == SparseJac(corrJac)
        B = SparseJac([2 0 3])
        corrJac = sparse(1.0I, 3, 3)
        corrJac[2,:] = [2, 0, 3]
        @test change_jacobian(A,B,2) == SparseJac(corrJac)
        dIx = collect(1:3)
        diagVal = [1,2,3]
        A = DiagJac(diagVal)
        B = NullJac(1,3)
        corrJac = sparse(dIx, dIx, diagVal)
        corrJac[1,1] = 0
        @test change_jacobian(A,B,1) == SparseJac(corrJac)
        A = DiagJac(diagVal)
        B = SparseJac([1 1 1])
        corrJac = sparse(dIx, dIx, diagVal)
        corrJac[2,:] = [1 1 1]
        @test change_jacobian(A,B,2) == SparseJac(corrJac)
        A = SparseJac([1 0 0; 0 2 3; 1 0 4])
        B = NullJac(1,3)
        corrJac = [1 0 0; 0 0 0; 1 0 4]
        @test change_jacobian(A,B,2) == SparseJac(corrJac)
        A = SparseJac([1 0 0; 0 2 3; 1 0 4])
        B = SparseJac([0 1 0])
        corrJac = [1 0 0; 0 2 3; 0 1 0]
        @test change_jacobian(A,B,3) == SparseJac(corrJac)
    end

    @testset "setindex CJAD single index" begin
        xTemp = copy(x)
        xTemp[1] = z[1]
        @test xTemp[1] == z[1] && xTemp[2:5] == x[2:5]
        yTemp = copy(y)
        yTemp[1] = x[3]
        @test yTemp[1] == x[3]
    end

    @testset "change_jacobian vector index" begin
        A = NullJac(3,3)
        B = NullJac(2,3)
        @test change_jacobian(A,B,[1,3]) == NullJac(3,3)
        B = SparseJac([0 0 1; 1 0 0])
        corrJac = spzeros(3,3)
        corrJac[1,3] = 1
        corrJac[3,1] = 1
        @test change_jacobian(A,B,[1,3]) == SparseJac(corrJac)
        A = IdentityJac(3)
        B = NullJac(2,3)
        corrJac = sparse(I,3,3)
        corrJac[1,1] = 0
        corrJac[3,3] = 0
        @test change_jacobian(A,B,[1,3]) == SparseJac(corrJac)
        B = SparseJac([2 0 3; 1 0 0])
        corrJac = sparse(1.0I, 3, 3)
        corrJac[1,:] = [2, 0, 3]
        corrJac[2,:] = [1, 0, 0]
        @test change_jacobian(A,B,[1,2]) == SparseJac(corrJac)
        dIx = collect(1:3)
        diagVal = [1,2,3]
        A = DiagJac(diagVal)
        B = NullJac(3,3)
        corrJac = sparse(dIx, dIx, diagVal)
        corrJac[1,1] = 0
        corrJac[2,2] = 0
        corrJac[3,3] = 0
        @test change_jacobian(A,B,[1,2,3]) == SparseJac(corrJac)
        A = DiagJac(diagVal)
        B = SparseJac([1 1 1;2 2 2])
        corrJac = sparse(dIx, dIx, diagVal)
        corrJac[2,:] = [1 1 1]
        corrJac[3,:] = [2 2 2]
        @test change_jacobian(A,B,[2,3]) == SparseJac(corrJac)
        A = SparseJac([1 0 0; 0 2 3; 1 0 4])
        B = NullJac(2,3)
        corrJac = [0 0 0; 0 0 0; 1 0 4]
        @test change_jacobian(A,B,[1,2]) == SparseJac(corrJac)
        A = SparseJac([1 0 0; 0 2 3; 1 0 4])
        B = SparseJac([0 1 0; 1 0 0])
        corrJac = [0 1 0; 0 2 3; 1 0 0]
        @test change_jacobian(A,B,[1,3]) == SparseJac(corrJac)
    end

    @testset "setindex CJAD" begin
        xTemp = copy(x)
        xTemp[1:5] = z[1:5]
        @test xTemp == z
        xTemp = copy(x)
        xTemp[1:3] = z[1:3]
        @test xTemp[1:3] == z[1:3] && xTemp[4:5] == x[4:5]
    end

    @testset "sum" begin
        corrVal = 15
        corrJac = [SparseJac([1 1 1 1 1]), NullJac(1,1), NullJac(1,5)]
        @test sum(x) == CJAD(corrVal, corrJac)
        corrVal = [22, 23, 24, 25, 26]
        corrJac = [SparseJac(ones(5,5)), NullJac(5,1), IdentityJac(5)]
        @test z + sum(x) == CJAD(corrVal, corrJac)
    end

    @testset "length" begin
        @test length(x) == 5
        @test length(y) == 1
    end

    @testset "cat" begin
        corrVal = [1,2,3,4,5,7,8,9,10,11,14,16,18,20,22]
        corrJac =  [SparseJac([1 0 0 0 0 0 0 0 0 0 0;
                    0 1 0 0 0 0 0 0 0 0 0;
                    0 0 1 0 0 0 0 0 0 0 0;
                    0 0 0 1 0 0 0 0 0 0 0;
                    0 0 0 0 1 0 0 0 0 0 0;
                    1 0 0 0 0 1 0 0 0 0 0;
                    0 1 0 0 0 1 0 0 0 0 0;
                    0 0 1 0 0 1 0 0 0 0 0;
                    0 0 0 1 0 1 0 0 0 0 0;
                    0 0 0 0 1 1 0 0 0 0 0;
                    1 0 0 0 0 1 1 0 0 0 0;
                    0 1 0 0 0 1 0 1 0 0 0;
                    0 0 1 0 0 1 0 0 1 0 0;
                    0 0 0 1 0 1 0 0 0 1 0;
                    0 0 0 0 1 1 0 0 0 0 1])]
        @test cat(x,x+y,x+y+z) == CJAD(corrVal, corrJac)
    end

end
x_init = collect(1:5);
y_init = 6;
z_init = collect(7:11);
(x, y, z) = initialize_CJAD(x_init, y_init, z_init);
