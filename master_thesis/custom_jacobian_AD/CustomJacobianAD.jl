module CustomJacobianAD
include("CustomJacobian.jl")
include("DiagonalJacobian.jl")
include("IdentityJacobian.jl")
include("NullJacobian.jl")
include("SparseJacobian.jl")
include("JacobianOperations.jl")


using LinearAlgebra, SparseArrays
export CJAD, initialize_CJAD, print_dim, double
import Base: -, +, *, /, ^, exp, getindex, inv, ==, copy, iterate
import Base: length, setindex!, cat, sum


struct CJAD
    val::Vector{Float64}
    customJacs::Vector{CustomJac}
end

function CJAD(val::Number, customJacs::Vector{CustomJac})
    CJAD([val], customJacs)
end

##### All operators are overladed to be elementwise operations #####
### Overload +:
function +(A::CJAD, B::CJAD)
    if length(A) == length(B)
        return CJAD(A.val .+ B.val, A.customJacs .+ B.customJacs)
    elseif length(A) == 1
        newA = expand_scalar_CJAD(A, length(B))
        return CJAD(newA.val .+ B.val, newA.customJacs .+ B.customJacs)
    elseif length(B.val) == 1
        newB = expand_scalar_CJAD(B, length(A))
        return CJAD(A.val .+ newB.val, A.customJacs .+ newB.customJacs)
    else
        error("CJAD vectors have uncompatible lengths")
    end
end
+(A::CJAD, B::Number) = CJAD(A.val .+ B, A.customJacs)
+(A::Number, B::CJAD) = B+A
function +(A::CJAD, B::Vector{<:Number})
    if length(A) != length(B) && length(A) != 1
        error("Uncompatible lengths on CJAD and vector.")
    elseif length(A) == 1
        A = expand_scalar_CJAD(A, length(B))
    end
    CJAD(A.val .+ B, A.customJacs)
end
+(A::Vector{<:Number}, B::CJAD) = B + A

### Overload -:
function -(A::CJAD)
    val = -A.val
    n = length(A.customJacs)
    jac = Vector{CustomJac}(undef,n)
    for i in 1:n
        jac[i] = -A.customJacs[i]
    end
    return CJAD(val, jac)
end
-(A::CJAD, B::CJAD) = A + (-B)
-(A::CJAD, B::Number) = CJAD(A.val .- B, A.customJacs)
-(A::Number, B::CJAD) = A + (-B)
function -(A::CJAD, B::Vector{<:Number})
    if length(A) != length(B) && length(A) != 1
        error("Uncompatible lengths on CJAD and vector.")
    elseif length(A) == 1
        A = expand_scalar_CJAD(A, length(B))
    end
    CJAD(A.val .- B, A.customJacs)
end
-(A::Vector{<:Number}, B::CJAD) = A + (-B)

### Overload *:
function *(A::CJAD, B::CJAD)
    if length(A) == 1
        A = expand_scalar_CJAD(A,length(B.val))
    elseif length(B.val) == 1
        B = expand_scalar_CJAD(B,length(A))
    end
    if length(A) == length(B)
        jac = product_rule(A, B)
        return CJAD(A.val .* B.val, jac)
    else
        error("CJAD vectors have uncompatible lengths")
    end
end
*(A::CJAD, B::Number) = CJAD(A.val .* B, A.customJacs .* B)
*(A::Number, B::CJAD) = B * A
function *(A::CJAD, B::Vector{<:Number})
    if length(A) != length(B) && length(A) != 1
        error("Uncompatible lengths on CJAD and vector.")
    elseif length(A) == 1
        A = expand_scalar_CJAD(A, length(B))
    end
    val = A.val .* B
    jac = Vector{CustomJac}(undef,length(A.customJacs))
    for i = 1:length(A.customJacs)
        jac[i] = B * A.customJacs[i]
    end
    return CJAD(val, jac)
end
*(A::Vector{<:Number}, B::CJAD) = B * A
*(A::BitArray{1}, B::CJAD) = convert(Vector{Float64}, A) * B
function *(A::AbstractArray{<:Number, 2}, B::CJAD)
    if size(A,2) != length(B)
        error("Array and CJAD dimensions does not support array multiplication.")
    end
    val = A * B.val
    jac = Vector{CustomJac}(undef,length(B.customJacs))
    for i = 1:length(B.customJacs)
        jac[i] = A * B.customJacs[i]
    end
    return CJAD(val,jac)
end


### Overload /:
function /(A::CJAD, B::CJAD)
    return A * B^(-1)
end
/(A::CJAD, B::Number) = A * (1 / B)
/(A::Number, B::CJAD) = A * B^(-1)
/(A::Vector{<:Number}, B::CJAD) = A * B^(-1)
/(A::CJAD, B::Vector{<:Number}) = A * (1 ./ B)

###Overload ^:
function ^(A::CJAD, k::Number)
    jac = chain_rule(k .* A.val.^(k-1), A.customJacs)
    CJAD(A.val.^k, jac)
end

function inv(A::CJAD)
    jac = chain_rule(-A.val.^(-2), A.customJacs)
    CJAD(A.val.^(-1), jac)
end
### Overload exp():
function exp(A::CJAD)
    jac = chain_rule(exp.(A.val), A.customJacs)
    CJAD(exp.(A.val), jac)
end

### Overload ==:
function (==)(A::CJAD, B::CJAD)
    if length(A) != length(B)
        return false
    end
    if A.val ≈ B.val && length(A.customJacs) == length(B.customJacs)
        for i = 1:length(A.customJacs)
            if !(A.customJacs[i] == B.customJacs[i])
                return false
            end
        end
        return true
    end
    return false
end

### Overload copy():
function copy(A::CJAD)
    val = copy(A.val)
    n = length(A.customJacs)
    jac = Vector{CustomJac}(undef,n)
    for i in 1:n
        jac[i] = copy(A.customJacs[i])
    end
    return CJAD(val, jac)
end

function getindex(A::CJAD, ix::Vector{Int})
    n = length(ix)
    if maximum(ix) > length(A)
        error("Index can not be higher than length of CJAD. \n
        Index = $ix, length CJAD = $(length(A))")
    elseif minimum(ix) <= 0
        error("Negative or zero indexes does not exist. \n
        Index = $ix.")
    end
    nJac = length(A.customJacs)
    jac = Vector{CustomJac}(undef,nJac)
    for i = 1:nJac
        jac[i] = A.customJacs[i][ix]
    end
    return CJAD(A.val[ix], jac)
end
getindex(A::CJAD, i::Int) = A[[i]]
getindex(A::CJAD, ix::UnitRange{Int}) = A[collect(ix)]

function setindex!(A::CJAD, B::CJAD, ix::Int)
    if ix > length(A)
        error("Index can not be higher than length of CJAD or less than 1. \n
        Index = $ix, length CJAD = $(length(A))")
    elseif length(B)>length(A)
        error("CJAD objects not compatible for setindex!")
    end
    A.val[ix] = B.val[1]
    for i = 1:length(A.customJacs)
        A.customJacs[i] = change_jacobian(A.customJacs[i], B.customJacs[i], ix)
    end
end

function setindex!(A::CJAD, B::CJAD, ix::AbstractVector{Int})
    if length(B) != length(ix)
        error("Length of indexes needs to be equal length of CJAD to insert.")
    end
    A.val[ix] = B.val
    for i = 1:length(A.customJacs)
        A.customJacs[i] = change_jacobian(A.customJacs[i], B.customJacs[i],ix)
    end
end

function sum(A::CJAD)
    val = sum(A.val)
    jac = Vector{CustomJac}(undef, length(A.customJacs))
    for i = 1:length(A.customJacs)
        jac[i] = colSum(A.customJacs[i])
    end
    return CJAD(val, jac)
end

function iterate(iter::CJAD, state = 1)
    if state > length(iter.val)
        return nothing
    end
    return (iter[state], state + 1)
end

length(A::CJAD) = length(A.val)

function cat(A::CJAD, B::CJAD...)
    varargin = [A, B...]
    nArg = length(varargin)
    nJac = length(varargin[1].customJacs)
    dimensions = Vector{Int64}(undef, nArg)
    for i = 1:nArg
        dimensions[i] = length(varargin[i])
    end
    allValues = Vector{Float64}(undef, sum(dimensions))
    ix = 0
    for i = 1:nArg
        allValues[ix+1:ix+dimensions[i]] = varargin[i].val
        ix += dimensions[i]
    end
    jacForEqualArg = Vector{SparseMatrixCSC{Float64,Int}}(undef,nArg)
    allJacs = Vector{SparseMatrixCSC{Float64,Int}}(undef,nJac)
    for jacNum = 1:nJac
        for argNum = 1:nArg
            jacForEqualArg[argNum] =
                convert(SparseJac, varargin[argNum].customJacs[jacNum]).jac
        end
        allJacs[jacNum] = vcat(jacForEqualArg...)
    end
    return CJAD(allValues, [SparseJac(hcat(allJacs...))])
end


## Help functions:

function expand_scalar_CJAD(A::CJAD, dimB)
    val = [A.val[1] for i = 1:dimB]
    n = length(A.customJacs)
    jac = Vector{CustomJac}(undef,n)
    for i = 1:n
        jac[i] = expand_scalar_CJAD_jac(A.customJacs[i], dimB)
    end
    return CJAD(val,jac)
end

function product_rule(A::CJAD, B::CJAD)
    nJac = length(A.customJacs)
    newJac = Vector{CustomJac}(undef,nJac)
    for i = 1:nJac
        newJac[i] = A.val * B.customJacs[i] + B.val * A.customJacs[i]
    end
    return newJac
end

function chain_rule(val::Vector{Float64}, customJacs::Vector{CustomJac})
    nJac = length(customJacs)
    newJac = Vector{CustomJac}(undef,nJac)
    for i = 1:nJac
        newJac[i] = val * customJacs[i]
    end
    return newJac
end

function print_dim(A::CJAD)
    println("CJAD.val = ",typeof(A.val), size(A.val))
    for i = 1:length(A.customJacs)
        println("CJAD.customJacs[$i] = ", typeof(A.customJacs[i]),
        size(A.customJacs[i]))
    end
end

function initialize_CJAD(x1,xn...)
    # Don't have a way to check number of output parameters yet.
    variables = [x1,xn...]
    m = length(variables)
    output = Vector{CJAD}(undef,m)
    for i in 1:m
        jac = Vector{CustomJac}(undef,m)
        nI = length(variables[i])
        for j in 1:m
            nJ = length(variables[j])
            if i == j
                jac[j] = IdentityJac(nI)
            else
                jac[j] = NullJac(nI, nJ)
            end
        end
        output[i] = CJAD(variables[i],jac)
    end
    if length(output) == 1
        return output[1]
    end
    return output
end

double(A::CJAD) = A.val
double(A::Number) = A

end #module CustomJacobianAD
