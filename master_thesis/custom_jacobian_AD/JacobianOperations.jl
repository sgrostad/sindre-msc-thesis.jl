using LinearAlgebra, SparseArrays
export change_jacobian

import Base: +
+(A::NullJac, B::NullJac) = B
+(A::NullJac, B::DiagJac) = B
+(A::NullJac, B::IdentityJac) = B
+(A::NullJac, B::SparseJac) = B

+(A::SparseJac, B::SparseJac) = SparseJac(A.jac + B.jac)
+(A::SparseJac, B::NullJac) = B+A
+(A::SparseJac, B::IdentityJac) = SparseJac(A.jac + I)
function +(A::SparseJac, B::DiagJac)
    n = length(B.jac)
    diagJac = sparse(1:n, 1:n, B.jac)
    return SparseJac((A.jac + diagJac))
end

+(A::DiagJac, B::DiagJac) = DiagJac(A.jac .+ B.jac)
+(A::DiagJac, B::NullJac) = B+A
+(A::DiagJac, B::SparseJac) = B+A
+(A::DiagJac, B::IdentityJac) = DiagJac(A.jac + ones(B.size))

+(A::IdentityJac, B::IdentityJac) = DiagJac(2*ones(A.size))
+(A::IdentityJac, B::NullJac) = B+A
+(A::IdentityJac, B::SparseJac) = B+A
+(A::IdentityJac, B::DiagJac) = B+A

import Base: *
*(A::NullJac, B::Number) = A
*(A::IdentityJac, B::Number) = DiagJac(ones(A.size) .* B)
*(A::DiagJac, B::Number) = DiagJac(A.jac .* B)
*(A::SparseJac, B::Number) = SparseJac(A.jac .* B)
*(A::Number, B::NullJac) = B * A
*(A::Number, B::IdentityJac) = B * A
*(A::Number, B::DiagJac) = B * A
*(A::Number, B::SparseJac) = B * A

*(A::NullJac, B::Vector{<:Number}) = A
*(A::IdentityJac, B::Vector{<:Number}) = DiagJac(B)
*(A::DiagJac, B::Vector{<:Number}) = DiagJac(A.jac .* B)
function *(A::SparseJac, B::Vector{<:Number})
    diagIndex = 1:length(B)
    valDiag = sparse(diagIndex, diagIndex, B)
    newJac = valDiag * A.jac
    return SparseJac(newJac)
end
*(A::Vector{<:Number}, B::NullJac) = B * A
*(A::Vector{<:Number}, B::IdentityJac) = B * A
*(A::Vector{<:Number}, B::DiagJac) = B * A
*(A::Vector{<:Number}, B::SparseJac) = B * A

*(A::AbstractArray{<:Number, 2}, B::NullJac) = NullJac(size(A,1), size(B,2))
*(A::AbstractArray{<:Number, 2}, B::IdentityJac) = SparseJac(A)
function *(A::AbstractArray{<:Number, 2}, B::DiagJac)
    return SparseJac(A .* transpose(B.jac))
end
*(A::AbstractArray{<:Number, 2}, B::SparseJac) = SparseJac(A * B.jac)

import Base: ==

==(A::NullJac, B::IdentityJac) = false
==(A::NullJac, B::DiagJac) = iszero(B.jac) && A.nCols == size(B,2) &&
    A.nRows == size(B,1)
==(A::NullJac, B::SparseJac) = iszero(B.jac) && A.nCols == size(B.jac, 2) &&
    A.nRows == size(B.jac,1)
==(A::IdentityJac, B::NullJac) = false
==(A::DiagJac, B::NullJac) = B == A
==(A::SparseJac, B::NullJac) = iszero(A.jac)

function ==(A::IdentityJac, B::DiagJac)
    for e in B.jac
        if e != 1
            return false
        end
    end
    if size(B) == size(A)
        return true
    end
    return false
end
function ==(A::IdentityJac, B::SparseJac)
    (row, col, val) = findnz(B.jac)
    if length(row) != length(col) || length(row) != size(A,1)
        return false
    end
    for i = 1:size(A,1)
        if row[i] != i || col[i] != i || val[i] != 1
            return false
        end
    end
    return true
end
==(A::DiagJac, B::IdentityJac) = B == A
==(A::SparseJac, B::IdentityJac) = B == A

function ==(A::DiagJac, B::SparseJac)
    (row, col, val) = findnz(B.jac)
    if length(row) != length(col) || length(row) != size(A,1)
        return false
    end
    for i = 1:size(A,1)
        if row[i] != i || col[i] != i || val[i] != A.jac[i]
            return false
        end
    end
    return true
end
==(A::SparseJac, B::DiagJac) = B == A


change_jacobian(A::NullJac, B::NullJac, ix::AbstractVector{Int}) = A
#TODO These operations would be much easier with a CSR matrix. Change to this?
## As of now, some of these change the A parameter, others are not. If A
## does not change CustomJac type, then it will be changed.
function change_jacobian(A::NullJac, B::SparseJac, ix::AbstractVector{Int})
    A = SparseJac(spzeros(A.nRows, A.nCols))
    A.jac[ix,:] = B.jac
    return A
end

function change_jacobian(A::IdentityJac, B::NullJac, ix::AbstractVector{Int})
    A = DiagJac(ones(A.size))
    for i in ix
        A.jac[i] = 0
    end
    return A
end
function change_jacobian(A::IdentityJac, B::SparseJac, ix::AbstractVector{Int})
    A = SparseJac(sparse(1.0I, A.size, A.size))
    A.jac[ix,:] = B.jac
    return A
end

function change_jacobian(A::DiagJac, B::NullJac, ix::AbstractVector{Int})
    for i in ix
        A.jac[i] = 0
    end
    return A
end
function change_jacobian(A::DiagJac, B::SparseJac, ix::AbstractVector{Int})
    dIx = collect(1:size(A,1))
    A = SparseJac(sparse(dIx, dIx, A.jac))
    A.jac[ix,:] = B.jac
    return A
end


function change_jacobian(A::SparseJac, B::NullJac, ix::AbstractVector{Int})
    A.jac[ix,:] = spzeros(1,size(A.jac,2))
    return A
end

function change_jacobian(A::SparseJac, B::SparseJac, ix::AbstractVector{Int})
    A.jac[ix,:] = B.jac
    return A
end

change_jacobian(A::CustomJac, B::CustomJac, ix::Int) = change_jacobian(A,B,[ix])
