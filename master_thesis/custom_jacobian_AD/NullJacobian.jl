export NullJac
import Base: size, -, ==, copy, getindex, setindex!

struct NullJac <: CustomJac
    nRows::Int
    nCols::Int
end

function size(A::NullJac)
    return (A.nRows, A.nCols)
end

function size(A::NullJac, dim::Int)
    if dim == 1
        return A.nRows
    elseif dim == 2
        return A.nCols
    else
        error("Dimension choice of NullJac can only be 1 or 2")
    end
end
expand_scalar_CJAD_jac(A::NullJac, dimB) = NullJac(dimB, A.nCols)

function -(A::NullJac)
    return A
end

==(A::NullJac, B::NullJac) = (A.nRows == B.nRows && A.nCols == B.nCols)

copy(A::NullJac) = NullJac(A.nRows, A.nCols)

colSum(A::NullJac) = NullJac(1, A.nCols)

##Not testing for inside range:
getindex(A::NullJac, i::Int) = NullJac(1, A.nCols)
getindex(A::NullJac, v::AbstractVector{Int}) = NullJac(length(v), A.nCols)
