export IdentityJac
import Base: size, -, ==, copy, getindex, setindex!

struct IdentityJac <: CustomJac
    size::Int
end

function size(A::IdentityJac)
    return (A.size, A.size)
end

function size(A::IdentityJac, dim::Int)
    if dim == 1 || dim == 2
        return A.size
    else
        error("Dimension choice of IdentityJac can only be 1 or 2")
    end
end

expand_scalar_CJAD_jac(A::IdentityJac, dimB) =
    SparseJac(ones(dimB))


function -(jac::IdentityJac)
    return DiagJac(-ones(jac.size))
end

==(A::IdentityJac, B::IdentityJac) = A.size == B.size

copy(A::IdentityJac) = IdentityJac(A.size)

colSum(A::IdentityJac) = SparseJac(sparse(ones(A.size), 1:A.size, ones(A.size)))

##Not testing for inside range:
getindex(A::IdentityJac, i::Int) = SparseJac(sparse([1], [i], [1], 1, size(A,2)))
getindex(A::IdentityJac, v::AbstractVector{Int}) = SparseJac(sparse(1:length(v), v, ones(length(v)), length(v), size(A,2)))
