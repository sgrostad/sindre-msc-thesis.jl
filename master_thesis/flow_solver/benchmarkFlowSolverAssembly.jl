using MATLAB, BenchmarkTools
using SetupFlowSolver,Statistics, Profile
using ForwardAutoDiff, CustomJacobianAD, FlowSystems, WellParameters

function assembleCJAD(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G, rep)
    (p_ad, bhp_ad, qS_ad) = initialize_CJAD(p_init, p_init[Int(wc[1])], 0)
    dt = 0.1;
    for i = 1:rep
        eq1 = presEq(p_ad, p_init, dt)
        eq1[wc] -= q_conn(p_ad, bhp_ad)
        eq = cat(eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))
    end
end

function assembleForwardAutoDiff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G, rep)
    (p_ad, bhp_ad, qS_ad) = initialize_AD(p_init, p_init[Int(wc[1])], 0)
    dt = 0.1;
    for i = 1:rep
        eq1 = presEq(p_ad, p_init, dt)
        eq1[wc] -= q_conn(p_ad, bhp_ad)
        eq = cat(eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))
    end
end
function assembleLocalAD(p_init, N, T, gradz, pv_r, WI, wc, dz, rep)
    well = WellPar(WI,wc,dz)
    fs = FlowSystem(p_init, p_init[wc[1]], 0.0, N, T, gradz, well, pv_r[1])
    dt = 0.1
    for i = 1:rep
        assembleFlowSystem!(fs, p_init, dt, well)
    end
end

function benchmarkAssemble()
    rep = 10
    k = 3
    time = zeros(5,k)
    for disc = k:k
        println("disc = $disc")
        ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
        presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
        hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G = setup_flow_solver(disc)
        nanoseconds = 10^(-9)
        println("run CJAD:")
        time[1,disc] = median(@benchmark assembleCJAD($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)).time * nanoseconds
        println("run ForwardAutoDiff:")
        time[2,disc] = median(@benchmark assembleForwardAutoDiff($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)).time * nanoseconds
        println("run LocalAD:")
        time[3,disc] = median(@benchmark assembleLocalAD($p_init, $N, $T, $gradz, $pv_r,
        $WI, $wc, $dz, $rep)).time * nanoseconds
        println("run MRST:")
        time[4:5,disc] = mxcall(:benchmarkFlowSolverAssemblyMRST, 1, disc, rep)
    end
    println("Results for $rep reps:")
    println("CJAD:              $(time[1,:])")
    println("ForwardAutoDiff:   $(time[2,:])")
    println("LocalAD:           $(time[3,:])")
    println("MRST:              $(time[4,:])")
    println("MRST_diagonal:     $(time[5,:])")
end

function benchmarkAssembleCJADandLocalAD()
    rep = 1
    disc = 5
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G = setup_flow_solver(disc)
    println("Number of cells = $(G["cells"]["num"])")
    println("run CJAD:")
    @btime assembleCJAD($ctrlEq, $rateEq, $q_conn,
    $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
    $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
    $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)
    println("run LocalAD:")
    @btime assembleLocalAD($p_init, $N, $T, $gradz, $pv_r, $WI, $wc, $dz, $rep)
    println("run matlab")
    matlabTime = mxcall(:benchmarkFlowSolverAssemblyMRST, 1, disc, rep)
    println("Time in matlab:\n $matlabTime")
end
mxcall(:runMatlabCode, 1, "initializeMRST");
benchmarkAssembleCJADandLocalAD()
#benchmarkAssemble()
#profileAssemble()
#=
rep = 5
disc = 5
ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G = setup_flow_solver(disc)
println("Number of cells = $(G["cells"]["num"])")
Profile.clear()
Profile.init(n = 10^10, delay = 0.0001)
Juno.@profiler assembleLocalAD(p_init, N, T, gradz, pv_r, WI, wc, dz, rep)
=#
