module WellParameters
export WellPar
struct WellPar
    WI::Vector{Float64}
    wc::Vector{Int}
    dz::Vector{Float64}
end
end  # module WellParameters
