module FlowSystems
using SparseArrays, StaticArrays, WellParameters, LocalAD
export FlowSystem, setAllFlowJacobianValues!, resetFlowSystem!, assembleFlowSystem!
export getNormalJac
import Base: size, length, ==, copy
struct FlowSystem
    eqVal::Vector{Float64}
    varVals::Vector{Float64}
    pIx::UnitRange{Int64}
    bhpIx::Int
    qSIx::Int
    globalJac::SparseMatrixCSC{MVector{NUM_DERIV,Float64}, Int}
    T::SparseMatrixCSC{Float64, Int}
    gradz::SparseMatrixCSC{Float64, Int}
    pv_r::Float64 #Needs to be a vector if cells have different porosity
end

function FlowSystem(pressure::Vector{Float64}, bhp::Float64, qS::Float64,
        N::Array{Int,2}, T::Vector{Float64}, gradz::Vector{Float64}, well::WellPar,
        pv_r::Float64)
    numNeighbours, col = size(N)
    numVals = length(pressure)+2
    if col != 2
        error("Neighbourlist must be on the form n×2")
    elseif size(N,1) != length(T)
        error("Must have equal number of transimissibilities as facets")
    end
    rows = [N[:,1];N[:,2]]
    cols = [N[:,2];N[:,1]]
    newT = sparse(rows, cols, [T; T])
    newGradz = sparse(rows, cols, [gradz; -gradz])
    diag = collect(1:numVals-1)
    rows = [rows; diag]
    cols = [cols; diag]
    #add Well eq:
    bhpNumber = numVals - 1
    numWells = length(well.wc)
    wellIx = bhpNumber*ones(numWells)
    rows = [rows; well.wc; wellIx]
    cols = [cols; wellIx; well.wc]
    #add production eq:
    qSNumber = numVals
    rows = [rows; qSNumber; bhpNumber]
    cols = [cols; bhpNumber; qSNumber]
    elementsInJac = length(cols)
    nzval = [@MVector zeros(NUM_DERIV) for i = 1:elementsInJac]
    eqVal = copy([pressure; bhp; qS])
    varVals = [pressure; bhp; qS]
    pIx = 1:length(pressure)
    bhpIx = length(pressure)+ 1
    qSIx = length(pressure) + 2
    return FlowSystem(eqVal, varVals, pIx, bhpIx, qSIx, sparse(rows, cols, nzval),
        newT, newGradz, pv_r)
end

function setAllFlowJacobianValues!(fs::FlowSystem, newVal::Float64)
    for i = 1:length(fs.globalJac.nzval)
        for j = 1:NUM_DERIV
            fs.globalJac.nzval[i][j] = newVal
        end
    end
end

function resetFlowSystem!(fs::FlowSystem)
    for i = 1:length(fs)
        fs.eqVal[i] = 0.0
    end
    setAllFlowJacobianValues!(fs, 0.0)
end

function size(fs::FlowSystem, dim::Int)
    if dim == 1
        return length(fs.eqVal)
    elseif dim == 2
        return size(fs.globalJac,2)
    else
        error("FlowSystem only has two dimensions. (\"dim\" = 1 or 2)")
    end
end
size(fs::FlowSystem) = (size(fs, 1), size(fs, 2))
length(fs::FlowSystem) = size(fs, 1)
function (==)(A::FlowSystem, B::FlowSystem)
    if A.eqVal == B.eqVal && A.globalJac == B.globalJac
        return true
    end
    return false
end
function copy(fs::FlowSystem)
    newVal = copy(fs.eqVal)
    newGlobalJac = copy(fs.globalJac)
    return FlowSystem(newVal, newGlobalJac)
end

function getNormalJac(fs::FlowSystem)
    numVals = length(fs.globalJac.nzval)
    newNzval = zeros(numVals)
    newRow = zeros(numVals)
    newCol = zeros(numVals)
    ix = 1
    for col = 1:size(fs,2)
        neighbourIndices = fs.globalJac.colptr[col]:(fs.globalJac.colptr[col + 1] - 1)
        for rowIx in neighbourIndices
            row = fs.globalJac.rowval[rowIx]
            newNzval[ix] = fs.globalJac[row,col][1]
            newRow[ix] = row
            newCol[ix] = col
            ix += 1
        end
    end
    return sparse(newRow, newCol, newNzval)
end

const g = 9.806649999999999
const mu = 0.005
const rhoS = 750
const p_r = 20000000
const rho_r = 850
const c = 1e-8
const cr = 9.999999999999999e-12

function assembleFlowSystem!(fs::FlowSystem, prevPressure::Vector{Float64},
        dt::Float64, well::WellPar)
    resetFlowSystem!(fs)
    bhpIx = length(fs.eqVal)-1
    qSIx = length(fs.eqVal)
    for from = 1:length(fs.eqVal)
        neighbourIndices = fs.globalJac.colptr[from]:(fs.globalJac.colptr[from + 1] - 1)
        for toIx in neighbourIndices
            to = fs.globalJac.rowval[toIx]
            if from == to
                if from <= length(fs.pIx)
                    p0 = prevPressure[from]
                    timeDerivLAD = timeDerivative(fs.varVals[from], p0, dt, fs.pv_r)
                    fs.eqVal[from] += timeDerivLAD.val
                    fs.globalJac[from,from] .+= timeDerivLAD.derivatives
                end
                continue
            end
            if from == bhpIx && to in well.wc
                rateEqLAD = rateEq(fs.varVals[to], fs.varVals[bhpIx], 0.0, [1,0,0], well)
                fs.eqVal[bhpIx] += rateEqLAD.val
                fs.globalJac[bhpIx,to] .+= rateEqLAD.derivatives
                rateEqLAD = rateEq(fs.varVals[to], fs.varVals[bhpIx], 0.0, [0,1,0], well)
                fs.globalJac[bhpIx,bhpIx] .+= rateEqLAD.derivatives
                continue
            elseif from in well.wc && to == bhpIx
                qConnLAD = q_conn(fs.varVals[from], fs.varVals[bhpIx], [1, 0, 0], well)
                fs.eqVal[from] -= qConnLAD.val
                fs.globalJac[from,from] .-= qConnLAD.derivatives
                qConnLAD = q_conn(fs.varVals[from], fs.varVals[bhpIx], [0, 1, 0], well)
                fs.globalJac[from,to] .-= qConnLAD.derivatives
                continue
            elseif from == qSIx && to == bhpIx
                ctrlEqLAD = ctrlEq(fs.varVals[bhpIx])
                fs.eqVal[qSIx] = ctrlEqLAD.val
                fs.globalJac[from, to] .= ctrlEqLAD.derivatives
                continue
            elseif from == bhpIx && to == qSIx
                qSLAD = createVariable(fs.varVals[qSIx], 1)
                fs.eqVal[bhpIx] += qSLAD.val
                fs.globalJac[from, to] .+= qSLAD.derivatives
                continue
            end
            T = fs.T[from, to]
            gradz = fs.gradz[from, to]
            fluxLAD = flux(from, to, fs.varVals, T, gradz)
            fs.eqVal[from] += fluxLAD.val
            fs.globalJac[to, from] .-= fluxLAD.derivatives
            fs.globalJac[from, from] .+= fluxLAD.derivatives
        end
    end
end

function p_conn(bhp, dz)
    return bhp + g*dz*rho(bhp)
end
function q_conn(p, bhp, derivative::Vector{Int}, well::WellPar)
    pAD = createVariable(p, derivative[1])
    bhpAD = createVariable(bhp, derivative[2])
    return well.WI[1] * (rho(pAD) / mu) * (p_conn(bhpAD, well.dz[1]) - pAD)
end

function rateEq(p::Float64, bhp::Float64, qS::Float64, derivative::Vector{Int},
        well::WellPar)
    qSAD = createVariable(qS, derivative[3])
    return qSAD - q_conn(p, bhp, derivative[1:2], well)/rhoS
end
function ctrlEq(bhp)
    bhpAD = createVariable(bhp,1)
    return bhpAD - 10000000
end


function rho(p)
    return rho_r * exp(c*(p-p_r))
end
function pv(p, pv_r)
    return pv_r * exp(cr * (p - p_r))
end
function timeDerivative(p::Float64, p0::Float64, dt::Float64, pv_r::Float64)
    pCell = createVariable(p,1)
    return (1/dt) * (pv(pCell, pv_r)*rho(pCell) - pv(p0, pv_r) * rho(p0))
end

## gradient from cell "from" to cell "to", derivatives wrt cell "from".
function grad(from::Int, to::Int, pressure::Vector{Float64})
    pFrom = createVariable(pressure[from], 1)
    pTo = createVariable(pressure[to], 0)
    return (pTo - pFrom)
end
grad(pFrom::LAD, pTo::LAD) = (pTo - pFrom)


## flux from cell "from" to cell "to", derivatives wrt cell "from".
function flux(from::Int, to::Int, pressure::Vector{Float64}, T::Float64,
        gradz::Float64)
    pFrom = createVariable(pressure[from], 1)
    pTo = createVariable(pressure[to], 0)
    rhoAverage = avg(rho(pFrom), rho(pTo))
    mu = 0.005
    g = 9.806649999999999
    viscousFlux = -T/mu * (grad(pFrom, pTo))
    gravityFlux = T/mu * g * rhoAverage * gradz
    return avg(rho(pFrom), rho(pTo)) * (viscousFlux + gravityFlux)
end
## faceAverage between cell "from" and "to", derivatives wrt cell "from"
function avg(from::Int, to::Int, pressure::Vector{Float64})
    pFrom = createVariable(pressure[from], 1)
    pTo = createVariable(pressure[to], 0)
    return 0.5 * (pTo + pFrom)
end
avg(pFrom::LAD, pTo::LAD) = 0.5 * (pFrom + pTo)
end  # module FlowSystems
