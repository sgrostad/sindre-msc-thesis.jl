function benchmarks = benchmarkFlowSolverMRST(d)
    mrstModule add ad-blackoil ad-core
    benchmarks = zeros(2,length(d));
    for j = 1:length(d)
        name = strcat('master_thesis/flow_solver/variables/flowSolverVariablesMATLAB','Disc',...
            num2str(d(j)));
        load(name)
        if d(j) == 1
            k = 10;
        else
            k = 1;
        end
        times = zeros(k,1);
        for method = 1:2
            for i  = 1:k
                tic
                if method == 1
                    [p_ad, bhp_ad, qS_ad] = initVariablesADI(p_init, p_init(wc(1)), 0);
                else
                    [p_ad, bhp_ad, qS_ad] = initVariablesAD_diagonal(p_init, p_init(wc(1)), 0);
                end
                t = 0; step = 0;
                while t < totTime
                   t = t + dt;
                   step = step + 1;
                   converged = false;
                   p0  = p_ad.val;
                   nit = 0;
                   while ~converged && (nit < maxits)
                      eq1     = presEq(p_ad, p0, dt);
                      eq1(wc) = eq1(wc) - q_conn(p_ad, bhp_ad);
                      eqs = {eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad)};
                      eq  = cat(eqs{:});
                      J   = eq.jac{1};  % Jacobian
                      res = eq.val;     % residual
                      upd = -(J \ res); % Newton update

                      % Update variables
                      p_ad.val   = p_ad.val   + upd(pIx);
                      bhp_ad.val = bhp_ad.val + upd(bhpIx);
                      qS_ad.val  = qS_ad.val  + upd(qSIx);

                      residual  = norm(res);
                      converged = ~(residual > tol);
                      nit       = nit + 1;
                   end
                end
                times(i) = toc;
            end
            benchmarks(method,j) = mean(times);
        end
    end
end
