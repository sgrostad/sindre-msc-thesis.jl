function res = benchmarkFlowSolverAssemblyMRST(disc, rep)
    mrstModule add ad-core
    name = strcat('master_thesis/flow_solver/variables/flowSolverVariablesMATLAB','Disc',...
            num2str(disc));
    load(name)
    res = zeros(2,1);
    numberOfTests = 10;
    p_init_start = p_init;
    for testNum = 1:numberOfTests
        tic
        [p_ad, bhp_ad, qS_ad] = initVariablesADI(p_init, p_init(wc(1)), 0);
        dt = 0.1;
        for j = 1:rep
            eq1     = presEq(p_ad, p_init, dt);
        end
        timeNormal(testNum) = toc;
        if sum(timeNormal)>20
            break;
        end
    end
    res(1,1) = median(timeNormal);

    p_init = p_init_start;
    for testNum = 1:numberOfTests
        tic
        [p_ad, bhp_ad, qS_ad] = initVariablesAD_diagonal(p_init, p_init(wc(1)), 0);
        dt = 0.1;
        for j = 1:rep
            eq1     = presEq(p_ad, p_init, dt);
            eq1(wc) = eq1(wc) - q_conn(p_ad, bhp_ad);
            eqs = {eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad)};
            eq  = cat(eqs{:});
        end
        timeDiagonal(testNum) = toc;
        p_init(1) = eq.val(1);
        if sum(timeDiagonal)>20
            break;
        end
    end
    res(2,1) = median(timeDiagonal);
end
