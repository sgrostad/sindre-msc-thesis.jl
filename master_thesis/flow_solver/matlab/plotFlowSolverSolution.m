% Plotting code copied from of flowSolverTutorialAD.m in MRST:
function plotFlowSolverSolution(in)
%% Plot production rate and pressure decay
% distribute input to correct variables:
sol = in{1};
G = in{2};
W = in{3};
dt = in{4};
nperf = in{5};
% Initialize show:
show = true([G.cells.num, 1]);
I = repmat(2, [nperf, 1]);
J = (1 : nperf).' + 1;
K = repmat(5, [nperf, 1]);
cellInx = sub2ind(G.cartDims, ...
   [I-1; I-1; I; I;   I(1:2) - 1], ...
   [J  ; J;   J; J;   nperf  + [2 ; 2]], ...
   [K-1; K;   K; K-1; K(1:2) - [0 ; 1]]);

show(cellInx) = false;
% Plot:
figure
clf
[ha, hr, hp] = plotyy(...
   convertTo([sol(2:end).time], day), ...
   convertTo(-[sol(2:end).qS], meter^3/day), ...
   ...
   convertTo([sol(2:end).time], day), ...
   convertTo(mean([sol(2:end).pressure], 1), barsa), 'stairs', 'plot');

set(ha, 'FontSize', 16);
set(hr, 'LineWidth', 2);
set(hp, 'LineStyle', 'none', 'Marker', 'o', 'LineWidth', 1);
set(ha(2), 'YLim', [100, 210], 'YTick', 100:50:200);

xlabel('time [days]');
ylabel(ha(1), 'rate [m^3/day]');
ylabel(ha(2), 'avg pressure [bar]');

%% Plot pressure evolution
clf
steps = [2, 5, 10, 20];
for i = 1 : numel(steps),
   subplot(2,2,i)
   plotCellData(G, convertTo(sol(steps(i)).pressure, barsa), ...
                show, 'EdgeColor', repmat(0.5, [1, 3]))

   plotWell(G, W)

   view(-125, 20), camproj perspective

   caxis([115, 205])
   axis tight off;

   text(200, 170, -8, ...
        sprintf('%.1f days', convertTo(steps(i)*dt, day)), 'FontSize', 14)
end

h = colorbar('South', 'Position', [0.1, 0.05, 0.8, .025]);
colormap(jet(55));

%% Copyright Notice
%
% <html>
% <p><font size="-1">
% Copyright 2009-2018 SINTEF ICT, Applied Mathematics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
end

