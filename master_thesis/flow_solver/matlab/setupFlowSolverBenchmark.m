function setupFlowSolverBenchmark(disc)
[nx, ny, nz] = deal( 10*disc,  10*disc, 10*disc);
[Dx, Dy, Dz] = deal(200, 200, 50);
G = cartGrid([nx, ny, nz], [Dx, Dy, Dz]);
G = computeGeometry(G);

rock = makeRock(G, 30*milli*darcy, 0.3);

cr   = 1e-6/barsa;
p_r  = 200*barsa;
pv_r = poreVolume(G, rock);
pv   = @(p) pv_r .* exp(cr * (p - p_r));
p    = linspace(100*barsa, 220*barsa, 50);

mu    = 5*centi*poise;
c     = 1e-3/barsa;
rho_r = 850*kilogram/meter^3;
rhoS  = 750*kilogram/meter^3;
rho   = @(p) rho_r .* exp(c * (p - p_r));

nperf = 8*disc;
I = repmat(2, [nperf, 1]);
J = (1 : nperf).' + 1;
K = repmat(5, [nperf, 1]);

cellInx = sub2ind(G.cartDims, I, J, K);
W = addWell([], G, rock, cellInx, 'Name', 'P1', 'Dir', 'y' );

gravity reset on, g = norm(gravity);
[z_0, z_max] = deal(0, max(G.cells.centroids(:,3)));
equil  = ode23(@(z,p) g .* rho(p), [z_0, z_max], p_r);
p_init = reshape(deval(equil, G.cells.centroids(:,3)), [], 1);  clear equil

show = true([G.cells.num, 1]);
cellInx = sub2ind(G.cartDims, ...
   [I-1; I-1; I; I;   I(1:2) - 1], ...
   [J  ; J;   J; J;   nperf  + [2 ; 2]], ...
   [K-1; K;   K; K-1; K(1:2) - [0 ; 1]]);

show(cellInx) = false;

N  = double(G.faces.neighbors);
intInx = all(N ~= 0, 2);
N  = N(intInx, :);                            % Interior neighbors
hT = computeTrans(G, rock);                   % Half-transmissibilities
cf = G.cells.faces(:,1);                      % Map: cell -> face number
nf = G.faces.num;                             % Number of faces
T  = 1 ./ accumarray(cf, 1 ./ hT, [nf, 1]);   % Harmonic average
T  = T(intInx);                               % Restricted to interior

n = size(N, 1);
C = sparse(repmat((1 : n).', [2, 1]), N, ...
           repmat([-1, 1], [n, 1]), n, G.cells.num);
grad = @(x) C * x;
div  = @(x)-C' * x;
avg  = @(x) 0.5 * (x(N(:,1)) + x(N(:,2)));


gradz  = grad(G.cells.centroids(:,3));
flux   = @(p)  -(T / mu).*(grad(p) - g*avg(rho(p)).*gradz);
presEq = @(p,p0,dt) (1 / dt) * (pv(p).*rho(p) - pv(p0).*rho(p0)) ...
                      + div(avg(rho(p)) .* flux(p));

wc = W(1).cells; % connection grid cells
WI = W(1).WI;    % well-indices
dz = W(1).dZ;    % connection depth relative to bottom-hole

p_conn  = @(bhp)  bhp + g*dz.*rho(bhp); %connection pressures
q_conn  = @(p,bhp) WI .* (rho(p(wc)) / mu) .* (p_conn(bhp) - p(wc));

rateEq = @(p,bhp,qS)  qS - sum(q_conn(p, bhp))/rhoS;
ctrlEq = @(bhp)       bhp - 100*barsa;

[p_ad, bhp_ad, qS_ad] = initVariablesADI(p_init, p_init(wc(1)), 0);
nc = G.cells.num;
[pIx, bhpIx, qSIx] = deal(1:nc, nc+1, nc+2);

% Parameters specifying the simulation
numSteps = 52;                  % number of time-steps
totTime  = 365*day;             % total simulation time
dt       = totTime / numSteps;  % constant time step
tol      = 1e-5;                % Newton tolerance
maxits   = 10;                  % max number of Newton its

% Empty structure for keeping the solution
sol = repmat(struct('time', [], 'pressure', [], 'bhp', [], 'qS', []), ...
             [numSteps + 1, 1]);

% Initial state
sol(1)  = struct('time', 0, 'pressure', double(p_ad), ...
                 'bhp', double(bhp_ad), 'qS', double(qS_ad));
name = strcat('master_thesis/flow_solver/variables/flowSolverVariablesMATLAB','Disc',num2str(disc)); 
save(name);
end


%% Copyright Notice
%
% <html>
% <p><font size="-1">
% Copyright 2009-2018 SINTEF ICT, Applied Mathematics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
