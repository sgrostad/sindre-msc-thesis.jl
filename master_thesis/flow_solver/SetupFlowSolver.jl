module SetupFlowSolver
using MATLAB
export setup_flow_solver, setupFlowSolverLocalAD, setup2DGrid
function setup_flow_solver(disc)

    barsa = 1e5
    ## Set up model geometry
    (nx, ny, nz) = (10.0 * disc, 10.0 * disc, 10.0 * disc)
    (Dx, Dy, Dz) = (200.0, 200.0, 50.0)
    geometryInput = mxarray([nx ny nz Dx Dy Dz])
    G = mxcall(:runMatlabCode, 1, "compute geometry", geometryInput)

    ## Define compressible rock model
    rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
    cr = 1e-11
    p_r = 200e5
    pv_r = rock["poro"].*G["cells"]["volumes"]
    pv_ad(p) = pv_r * exp(cr * (p-p_r))
    pv(p, pv_r) = pv_r * exp(cr * (p-p_r))
    p = collect(range(100e5,length = 50, stop = 220e5))

    ##Define model for compressible fluid
    mu = 0.0050
    c = 1e-8
    rho_r = 850.0
    rhoS = 750.0
    rho(p) = rho_r * exp(c * (p-p_r))


    ##Add well to geometry
    d = min(disc, 3)
    nperf = 8.0 * disc
    i = mxarray(ones(Int(nperf),1)*2.0)
    j = mxarray(collect(2:Int(nperf)+1)*1.0)
    k = mxarray(ones(Int(nperf),1)*5.0)

    W = mxcall(:runMatlabCode, 1, "add well",
                mxarray([mxarray(G) mxarray(rock) i j k ]))

    ##Derive initial pressure
    g = 9.806649999999999
    p_init = mxcall(:runMatlabCode, 1, "derive initial pressure",
                    mxarray([mxarray(G) p_r rho_r c]))

    ## compute transmissibilities on interior connections
    Ntemp = G["faces"]["neighbors"]
    intInx = [Int(n != 0) for n in Ntemp]
    intInx = convert(Array{Bool,1},intInx[:,1].* intInx[:,2])
    N1 = [Ntemp[i,1] for i in 1:size(Ntemp,1) if intInx[i]]
    N2 = [Ntemp[i,2] for i in 1:size(Ntemp,1) if intInx[i]]
    N = [N1 N2]
    hT = mxcall(:runMatlabCode, 1, "computeTrans",
                mxarray([mxarray(G) mxarray(rock)]))
    cf = G["cells"]["faces"][:,1]
    nf = G["faces"]["num"]
    T = mxcall(:runMatlabCode, 1, "Harmonic average in interior",
            mxarray([mxarray(cf) mxarray(hT) nf mxarray(intInx)]))

    ## Define discrete operators
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)]))
    N = convert(Array{Int64,2},N)
    gradient(x) = C * x
    divergence(x) = -C' * x
    average(x) = 0.5 * (x[N[:,1]] + x[N[:,2]])

    ## Define flow equations
    gradz = gradient(G["cells"]["centroids"][:,3])
    flux(p) = -(T / mu) * (gradient(p) - g*average(rho(p))*gradz)
    np = length(p_init)
    presEq(p, p0, dt) = (1/dt) * (pv_ad(p)*rho(p) - pv.(p0, pv_r).*rho.(p0)) +
                        divergence(average(rho(p)) * flux(p))
    ## Define well equations
    wc = convert(Array{Int64,1},W["cells"])
    WI = W["WI"]
    dz = W["dZ"]

    p_conn(bhp) = bhp + g*dz*rho(bhp)
    q_conn(p,bhp) = WI * (rho(p[wc])/mu) * (p_conn(bhp) - p[wc])

    rateEq(p,bhp,qS) = qS - sum(q_conn(p,bhp))/rhoS
    ctrlEq(bhp) = bhp - 100*barsa

    return (ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)
end

function setupFlowSolverLocalAD(disc)
    (ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G) =
    setup_flow_solver(disc);
    return (dz, WI, wc, np, gradz, N, C, T, nf, cf, hT, p_init, pv_r, G)
end

function setup2DGrid()
    arr = mxcall(:runMatlabCode, 1, "2D 2phase grid")
    G = arr[1]
    N = arr[2]
    T = arr[3]
    gradz = arr[4]
    pv_r = arr[5]
    N = convert(Array{Int64,2},N)
    return (G, N, T, gradz, pv_r)
end
end #module
