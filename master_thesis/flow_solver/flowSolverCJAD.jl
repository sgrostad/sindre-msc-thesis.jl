using CustomJacobianAD, MATLAB, Plots, LinearAlgebra, Printf
mxcall(:runMatlabCode, 1, "initializeMRST");

struct solution
    time
    pressure
    bhp
    qS
end

function flowSolver(makePlots)
    barsa = 1e5
    ## Set up model geometry
    (nx, ny, nz) = (10.0, 10.0, 10.0)
    (Dx, Dy, Dz) = (200.0, 200.0, 50.0)
    geometryInput = mxarray([nx ny nz Dx Dy Dz])
    G = mxcall(:runMatlabCode, 1, "compute geometry", geometryInput)

    ## Define compressible rock model
    rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
    cr = 1e-11
    p_r = 200e5
    pv_r = rock["poro"].*G["cells"]["volumes"]
    pv_ad(p) = pv_r * exp(cr * (p-p_r))
    pv(p, pv_r) = pv_r * exp(cr * (p-p_r))
    p = collect(range(100e5,length = 50, stop = 220e5))

    ##Define model for compressible fluid
    mu = 0.0050
    c = 1e-8
    rho_r = 850.0
    rhoS = 750.0
    rho(p) = rho_r * exp(c * (p-p_r))

    ##Add well to geometry
    nperf = 8.0
    i = mxarray(ones(8,1)*2.0)
    j = mxarray(collect(2:9)*1.0)
    k = mxarray(ones(8,1)*5.0)

    W = mxcall(:runMatlabCode, 1, "add well",
                mxarray([mxarray(G) mxarray(rock) i j k ]))

    ##Derive initial pressure
    g = 9.806649999999999
    p_init = mxcall(:runMatlabCode, 1, "derive initial pressure",
                    mxarray([mxarray(G) p_r rho_r c]))

    ## Plot well and initial pressure
    if makePlots
        mxcall(:runMatlabCode, 1, "plot well and initial pressure",
                mxarray([mxarray(G) mxarray(W) nperf I_J K mxarray(p_init)]))
    end

    ## compute transmissibilities on interior connections
    Ntemp = G["faces"]["neighbors"]
    intInx = [Int(n != 0) for n in Ntemp]
    intInx = convert(Array{Bool,1},intInx[:,1].* intInx[:,2])

    N = zeros(2700,2)
    N[:,1] = [Ntemp[i,1] for i in 1:size(Ntemp,1) if intInx[i]]
    N[:,2] = [Ntemp[i,2] for i in 1:size(Ntemp,1) if intInx[i]]
    hT = mxcall(:runMatlabCode, 1, "computeTrans",
                mxarray([mxarray(G) mxarray(rock)]))
    cf = G["cells"]["faces"][:,1]
    nf = G["faces"]["num"]
    T = mxcall(:runMatlabCode, 1, "Harmonic average in interior",
            mxarray([mxarray(cf) mxarray(hT) nf mxarray(intInx)]))

    ## Define discrete operators
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)]))
    N = convert(Array{Int64,2},N)
    gradient(x) = C * x
    divergence(x) = -C' * x

    average(x) = 0.5 * (x[N[:,1]] + x[N[:,2]])
    if makePlots
        spy(C) #Very slow and can have problems with collision with function in
        #MATLAB library
    end

    ## Define flow equations
    gradz = gradient(G["cells"]["centroids"][:,3])
    flux(p) = -(T / mu) * (gradient(p) - g*average(rho(p))*gradz)
    np = length(p_init)
    presEq(p, p0, dt) = (1/dt) * (pv_ad(p)*rho(p) - pv.(p0, pv_r).*rho.(p0)) +
                        divergence(average(rho(p)) * flux(p))
    ## Define well equations
    wc = convert(Array{Int64,1},W["cells"])
    WI = W["WI"]
    dz = W["dZ"]

    p_conn(bhp) = bhp + g*dz*rho(bhp)
    q_conn(p,bhp) = WI * (rho(p[wc])/mu) * (p_conn(bhp) - p[wc])


    rateEq(p,bhp,qS) = qS - sum(q_conn(p,bhp))/rhoS

    ctrlEq(bhp) = bhp - 100*barsa

    ## Initialize loop
    #AD-Variables...
    (p_ad, bhp_ad, qS_ad)= initialize_CJAD(p_init, p_init[Int(wc[1])], 0)

    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    sol = Vector{solution}(undef, numSteps + 1)
    sol[1] = solution(0, p_ad, bhp_ad, qS_ad)

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        @printf("\nTime step %d: Time %.2f -> %.2f days\n",
        stepNumber, (t-dt)/secondsInDay, t/secondsInDay)
        #Newton loop
        converged = false
        p0 = p_ad.val
        nit = 0
        while !converged && (nit < maxIts)
            eq1 = presEq(p_ad, p0, dt)
            eq1[wc] -= q_conn(p_ad, bhp_ad)

            eq = cat(eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))

            upd = -(eq.customJacs[1].jac\eq.val)
            #Update variables:
            p_ad += upd[pIx]
            bhp_ad += upd[bhpIx]
            qS_ad += upd[qSIx]

            residual = norm(eq.val)
            converged = (residual<tol)
            nit       = nit + 1;
            @printf("\tIteration %3d:  Res = %.4e\n", nit, residual)
        end
        if !converged
            error("Newton solver did not converge")
        else
            sol[stepNumber + 1] = solution(t, p_ad.val, bhp_ad.val, qS_ad.val)
        end
    end
    return sol, G, W, nperf, dt
end

sol, G, W, nperf, dt = flowSolver(false)
mxcall(:runMatlabCode, 1, "plot solution",
            mxarray([mxstructarray(sol) mxarray(G) mxarray(W) dt nperf]));
