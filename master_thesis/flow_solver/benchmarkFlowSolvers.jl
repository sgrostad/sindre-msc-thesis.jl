using CustomJacobianAD, ForwardAutoDiff, MATLAB, LinearAlgebra, Printf
using LocalAD, FlowSystems, WellParameters
using BenchmarkTools, Statistics, SetupFlowSolver, Profile
# Line below needs to run each time you begin running matlab scripts/functions
mxcall(:runMatlabCode, 1, "initializeMRST");

function flow_solver_LocalAD(p_init, N, T, gradz, pv_r, WI, wc, dz, G)
    well = Well(WI,wc,dz)
    fs = FlowSystem(p_init, p_init[wc[1]], 0.0, N, T, gradz, well, pv_r[1])
    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        #Newton loop
        converged = false
        p0 = fs.varVals[pIx]
        nit = 0
        while !converged && (nit < maxIts)
            assembleFlowSystem!(fs, p0, dt, well)
            J = getNormalJac(fs)
            upd = -(J\fs.eqVal)
            #Update variables:
            fs.varVals .+= upd

            residual = norm(fs.eqVal)
            converged = (residual<tol)
            nit       = nit + 1;
        end
    end
end

function flow_solver_CJAD(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)

    (p_ad, bhp_ad, qS_ad)= initialize_CJAD(p_init, p_init[Int(wc[1])], 0)
    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        #Newton loop
        converged = false
        p0 = p_ad.val
        nit = 0
        while !converged && (nit < maxIts)
            eq1 = presEq(p_ad, p0, dt)
            eq1[wc] -= q_conn(p_ad, bhp_ad)

            eq = cat(eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))

            upd = -(eq.customJacs[1].jac\eq.val)
            #Update variables:
            p_ad += upd[pIx]
            bhp_ad += upd[bhpIx]
            qS_ad += upd[qSIx]

            residual = norm(eq.val)
            converged = (residual<tol)
            nit       = nit + 1;
        end
    end
end

function flow_solver_forward_auto_diff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)

    (p_ad, bhp_ad, qS_ad)= initialize_AD(p_init, p_init[Int(wc[1])], 0)
    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        #Newton loop
        converged = false
        p0 = p_ad.val
        nit = 0
        while !converged && (nit < maxIts)
            eq1 = presEq(p_ad, p0, dt)
            eq1[wc] = eq1[wc] - q_conn(p_ad, bhp_ad)

            eq = cat(eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))

            upd = -(eq.jac\eq.val)
            #Update variables:
            p_ad += upd[pIx]
            bhp_ad += upd[bhpIx]
            qS_ad += upd[qSIx]

            residual = norm(eq.val)
            converged = (residual<tol)
            nit       = nit + 1;
        end
    end
end

function benchmark_larger_flow_solvers()
    nanoseconds = 10^(-9)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 1

    println("Benchmark ForwardAutoDiff on flow solver:")
    maxDisc = 3
    forwardAutoDiffTime = zeros(maxDisc)
    for i = 1:maxDisc
        ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
        presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
        hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
        setup_flow_solver(i)

        forwardAutoDiffTime[i] = median(
        @benchmark flow_solver_forward_auto_diff($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G)).time * nanoseconds
    end
    println("Benchmark CustomJacobianAD on flow solver:")
    CJADTime = zeros(maxDisc)
    for i = 1:maxDisc
        ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
        presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
        hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
        setup_flow_solver(i)

        CJADTime[i] = median(
        @benchmark flow_solver_CJAD($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G)).time * nanoseconds
    end
    println("Benchmark LocalAD on flow solver:")
    LocalADTime = zeros(maxDisc)
    for i = 1:maxDisc
        dz, WI, wc, np, gradz, N, C, T, nf, cf, hT, p_init, pv_r, G =
        setupFlowSolverLocalAD(i)

        LocalADTime[i] = median(
        @benchmark flow_solver_LocalAD($p_init, $N, $T, $gradz, $pv_r, $WI, $wc,
                $dz, $G)).time * nanoseconds
    end

    println("Benchmark MRST on flow solver:")
    mrstTime = mxcall(:benchmarkFlowSolverMRST, 1, mxarray(collect(1:maxDisc)))
    println("Result:")
    println("ForwardAutoDiff:")
    println(forwardAutoDiffTime)
    println("CustomJacobianAD:")
    println(CJADTime)
    println("LocalAD:")
    println(LocalADTime)
    println("MRST:")
    println(mrstTime[1,:])
    println("MRST diagonal:")
    println(mrstTime[2,:])
end
function profile_flow_solver()
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
    setup_flow_solver(1)

    flow_solver_forward_auto_diff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)

    flow_solver_CJAD(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)

    Profile.init(n = 10^10, delay = 0.001)
    Profile.clear()

    @profiler flow_solver_CJAD(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)

    @profiler flow_solver_forward_auto_diff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)
end
function quick_benchmark_flow_solvers()
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
    setup_flow_solver(1)
    println("CJAD:")
    @btime flow_solver_CJAD($ctrlEq, $rateEq, $q_conn,
    $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
    $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
    $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G)
    println("ForwardAutoDiff:")
    @btime flow_solver_forward_auto_diff($ctrlEq, $rateEq, $q_conn,
    $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
    $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
    $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G)
    println("LocalAD:")
    @btime flow_solver_LocalAD($p_init, $N, $T, $gradz, $pv_r, $WI, $wc,
            $dz, $G)
end

benchmark_larger_flow_solvers()
#profile_flow_solver()
#quick_benchmark_flow_solvers()
