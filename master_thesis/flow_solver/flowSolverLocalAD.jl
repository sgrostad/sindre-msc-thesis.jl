using FlowSystems, MATLAB, Plots, LinearAlgebra, Printf, SetupFlowSolver
using WellParameters
mxcall(:runMatlabCode, 1, "initializeMRST");

struct solution
    time
    pressure
    bhp
    qS
end

function flowSolver()
    disc = 1
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G = setup_flow_solver(disc)

    well = Well(WI,wc,dz)
    fs = FlowSystem(p_init, p_init[wc[1]], 0.0, N, T, gradz, well, pv_r[1])

    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    sol = Vector{solution}(undef, numSteps + 1)
    sol[1] = solution(0, fs.varVals[pIx], fs.varVals[bhpIx], fs.varVals[bhpIx])

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        @printf("\nTime step %d: Time %.2f -> %.2f days\n",
        stepNumber, (t-dt)/secondsInDay, t/secondsInDay)
        #Newton loop
        converged = false
        p0 = fs.varVals[pIx]
        nit = 0
        while !converged && (nit < maxIts)

            assembleFlowSystem!(fs, p0, dt, well)
            J = getNormalJac(fs)
            upd = -(J\fs.eqVal)
            #Update variables:
            fs.varVals .+= upd

            residual = norm(fs.eqVal)
            converged = (residual<tol)
            nit       = nit + 1;
            @printf("\tIteration %3d:  Res = %.4e\n", nit, residual)
        end
        if !converged
            error("Newton solver did not converge")
        else
            sol[stepNumber + 1] = solution(t, fs.varVals[pIx], fs.varVals[bhpIx],
                fs.varVals[bhpIx])
        end
    end
    return sol, G, dt
end

sol, G, dt = flowSolver()
rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
i = mxarray(ones(8,1)*2.0)
j = mxarray(collect(2:9)*1.0)
k = mxarray(ones(8,1)*5.0)
nperf = 8.0
W = mxcall(:runMatlabCode, 1, "add well",
            mxarray([mxarray(G) mxarray(rock) i j k ]))
mxcall(:runMatlabCode, 1, "plot solution",
            mxarray([mxstructarray(sol) mxarray(G) mxarray(W) dt nperf]));
