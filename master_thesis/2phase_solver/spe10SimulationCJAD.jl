using TwoPhaseSystems, MATLAB, Printf, SetupFlowSolver, LinearAlgebra
using CustomJacobianAD, SparseArrays, InitSPE10Sim
using CustomJacobianAD
mxcall(:runMatlabCode, 1, "initializeMRST");

function SPE10CJAD()
    (G, grid, fluid, N, T, dt, totTime, well, p0, s0) = initializeSpe10Simulation()
    ## Setup Equations:
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)]))
    dGrad(x) =   C*x;
    dDiv(x) = -C'*x;
    upw(flag, x::Vector{Float64}) = flag.*x[grid.N[:, 1]] + .!flag.*x[grid.N[:, 2]];
    upw(flag, x::CJAD) = flag*x[grid.N[:, 1]] + .!flag*x[grid.N[:, 2]];
    bhp = well.outPres;
    q(lambda::Vector{Float64}, p::CJAD) =  -well.WI .* lambda * (p - bhp);
    q(lambda::Vector{Float64}, p::Vector{Float64}) =  -well.WI .* lambda .* (p - bhp);

    p = initialize_CJAD(p0)
    s = initialize_CJAD(s0)

    secondsInDay = 86400.0
    smallTol = 1e-4
    tol = 1e-8
    maxIts = 40
    t = 0.0
    stepNumber = 0
    i = 0
    saturations = Vector{Vector{Float64}}(undef,length(dt))
    while t < totTime
        i += 1
        t += dt[i]
        stepNumber += 1
        @printf("\nTime step %d: Time %.2f -> %.2f days\n",
            stepNumber, (t-dt[i])/secondsInDay, t/secondsInDay)
        println("Finished with $(i/length(dt)*100)% of the simulation.")
        #Newton loop
        converged = false
        nit = 0
        ## Pressure equation:
        lambda0 = TwoPhaseSystems.λwo.(fluid, s0)
        while !converged && (nit < maxIts)
            dp = dGrad(p)
            v = -upw(double(dp) .<= 0, lambda0).*T*dp
            pressureEq = dDiv(v)
            pressureEq[well.inIx] -= well.inRate
            pressureEq[well.outIx] -= q(lambda0[well.outIx], p[well.outIx])
            res = pressureEq.val
            J = pressureEq.customJacs[1].jac
            upd = -(J\res)
            p += upd
            residual = norm(res)
            if nit > 10
                converged = (residual < smallTol)
            else
                converged = (residual<tol)
            end
            nit       = nit + 1;
            println("Pressure iteration $nit:  Res = $residual")
        end
        if !converged
            error("Newton solver did not converge for pressure equation")
        end
        ## Transport equation:
        nit = 0
        converged = false
        s0 = copy(double(s))
        p0 = copy(double(p))
        while !converged && (nit < maxIts)
            dp0 = dGrad(p0)
            v = -upw(dp0 .<= 0, lambda0) .* T .* dp0
            fw = TwoPhaseSystems.fractionalWaterFlow(fluid, s)
            transportEq = (s - s0) + (dt[i]./grid.pv) * dDiv(upw(dp0 .<= 0, fw) * v);
            transportEq[well.inIx] -= (dt[i]/grid.pv[well.inIx]) * well.inRate
            transportEq[well.outIx] -= (dt[i]./grid.pv[well.outIx]) * fw[well.outIx] * q(lambda0[well.outIx], p0[well.outIx])
            res = transportEq.val
            J = transportEq.customJacs[1].jac
            upd = -(J\res)
            s += upd
            residual = norm(res)
            if nit > 10
                converged = (residual < smallTol)
            else
                converged = (residual<tol)
            end
            nit       = nit + 1;
            println("Transport iteration $nit:  Res = $residual")
            if maximum(double(s)) > 1
                println("waterSaturation above 1! \n Max is $(maximum(double(s)))")
                s.val = max.(min.(double(s),1),0)
            end
        end
        if !converged
            println("Newton solver did not converge for transport equation")
            saturations[i] = copy(double(s))
            return saturations[1:i-1], dt
        end
        saturations[i] = copy(double(s))
    end
    return saturations, dt

end
saturations, dt = SPE10CJAD()
mxcall(:spe10Simulation, 1, "plot solution",
    mxarray([mxarray(saturations) mxarray(dt) mxarray(collect(1:length(saturations)))]))
#mxcall(:spe10Simulation, 1, "plot solution", mxarray([mxarray(saturations[end]) mxarray(sum(dt)) mxarray(1)]))
