grad(pFrom, pTo) = (pTo - pFrom)

function flux(from, to, fluid::Fluid, tps::TwoPhaseSystem, grid::Grid)
    pFrom = createVariable(tps.pressure[from], 1)
    pTo = createVariable(tps.pressure[to], 0)
    saturationIx = findUpStream(from, to, tps)
    λ = λwo(fluid, tps.waterSaturation[saturationIx])
    T = grid.T[from, to]
    viscousFlux = -T*λ*grad(pFrom, pTo)
    gravityFlux = 0
    return viscousFlux + gravityFlux
end
# TODO når korrekt kan flux-funksjonene gjøres om til én funksjon.
function totalFlux(from, to, fluid::Fluid, tps::TwoPhaseSystem, grid::Grid, prevWaterSaturation)
    pFrom = tps.pressure[from]
    pTo = tps.pressure[to]
    saturationIx = findUpStream(from, to, tps)
    waterSaturation = prevWaterSaturation[saturationIx]
    λ = λwo(fluid, waterSaturation)
    T = grid.T[from, to]
    viscousFlux = -T*λ*grad(pFrom, pTo)
    gravityFlux = 0
    return viscousFlux + gravityFlux
end

function findUpStream(from, to, tps)
    if tps.pressure[from] > tps.pressure[to]
        return from
    end
    return to
end

function timeDerivative(cellIx, tps, prevWaterSat)
    waterSat = createVariable(tps.waterSaturation[cellIx], 1)
    return (waterSat - prevWaterSat)
end

function waterFlux(from, to, fluid, tps, grid, prevWaterSaturation)
    saturationIx = findUpStream(from, to, tps)
    if saturationIx == from 
        waterSat = createVariable(tps.waterSaturation[saturationIx], 1)
    else
        waterSat = createVariable(tps.waterSaturation[saturationIx], 0)
    end
    fw = fractionalWaterFlow(fluid, waterSat)
    return fw * totalFlux(from, to, fluid, tps, grid, prevWaterSaturation)
end

function totalProduction(pressure, waterSat, fluid, well, wellIx)
    bhp = well.outPres[wellIx]
    return well.WI[wellIx] * λwo(fluid, waterSat) * (pressure - bhp)
end
