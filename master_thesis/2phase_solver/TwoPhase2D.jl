using TwoPhaseSystems, MATLAB, Printf, SetupFlowSolver, LinearAlgebra

mxcall(:runMatlabCode, 1, "initializeMRST");
function initializeTwoPhase2DSimulation()
    cellArray = mxcall(:runMatlabCode, 1, "setup 2D model")
    (G, N, T, gradz, pv, wells, dt, totTime, p0, s0) = cellArray
    fluid = Fluid()
    numCells = convert(Int64, G["cells"]["num"])
    grid = Grid(T, pv, N, numCells)
    well = Well(wells[1], wells[2], [wells[3]], [wells[4]], [wells[5]])
    return (G, grid, gradz, fluid, N, T, dt, totTime, well, p0, s0)
end
function twoPhase2D()
    (G, grid, gradz, fluid, N, T, dt, totTime, well, p0, s0) = initializeTwoPhase2DSimulation()
    rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
    tps = TwoPhaseSystem(grid)
    tps.pressure .= p0
    tps.waterSaturation .= s0
    secondsInDay = 86400.0
    tol = 1e-8
    maxIts = 40
    t = 0.0
    stepNumber = 0
    i = 0
    while t < totTime
        i += 1
        t += dt[i]
        stepNumber += 1
        @printf("\nTime step %d: Time %.2f -> %.2f days\n",
            stepNumber, (t-dt[i])/secondsInDay, t/secondsInDay)
        #Newton loop
        converged = false
        nit = 0
        ## Pressure equation:
        while !converged && (nit < maxIts)
            assemblePressureEquations!(tps, fluid, grid, well)
            upd = -(tps.pressureEqJac\tps.pressureEq)
            tps.pressure .+= upd
            residual = norm(tps.pressureEq)
            converged = (residual<tol)
            nit       = nit + 1;
            println("Pressure iteration $nit:  Res = $residual")
        end
        if !converged
            error("Newton solver did not converge for pressure equation")
        end
        ## Transport equation:
        nit = 0
        converged = false
        prevWaterSaturation = copy(tps.waterSaturation)
        while !converged && (nit < maxIts)
            assembleTransportEquations!(tps, fluid, grid, prevWaterSaturation, dt[i], well)

            upd = -(tps.transportEqJac\tps.transportEq)
            tps.waterSaturation .+= upd
            residual = norm(tps.transportEq)
            converged = (residual<tol)
            nit       = nit + 1;
            println("Transport iteration $nit:  Res = $residual")
            if maximum(tps.waterSaturation) > 1
                println("waterSaturation above 1! \n Max is $(maximum(tps.waterSaturation))")
            end
            tps.waterSaturation .= max.(min.(tps.waterSaturation,1),0)
        end
        if !converged
            return tps
            error("Newton solver did not converge for transport equation")
        end
        mxcall(:runMatlabCode, 1, "plot 2D grid", mxarray([mxarray(G) mxarray(tps.waterSaturation)]))
    end
    return tps

end
tps = twoPhase2D()
