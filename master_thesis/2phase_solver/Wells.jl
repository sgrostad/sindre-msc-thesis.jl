export Well
struct Well
    inRate::Float64
    inIx::Int64

    outPres::Vector{Float64}
    outIx::Vector{Int64}
    WI::Vector{Float64}
end
