using Statistics, MATLAB, BenchmarkTools, TwoPhaseSystems, InitSPE10Sim, Profile
using SparseArrays, CustomJacobianAD
mxcall(:runMatlabCode, 1, "initializeMRST");

function testTwoPhaseSystem(tps::TwoPhaseSystem, fluid::Fluid, grid::Grid,
        well::Well, repTot::Int64, repPressure::Int64, repTransport::Int64,
        dt::Float64, p::Vector{Float64}, s::Vector{Float64}, s0::Vector{Float64})
    tps.pressure .= p
    tps.waterSaturation .= s
    for i = 1:repTot
        for j = 1:repPressure
            assemblePressureEquations!(tps, fluid, grid, well)
        end
        for k = 1:repTransport
            assembleTransportEquations!(tps, fluid, grid, s0, dt, well)
        end
    end
end

function testTwoPhaseCJAD(dGrad, dDiv, upw, q, C, T, fluid::Fluid, grid::Grid,
        well::Well, repTot::Int64, repPressure::Int64, repTransport::Int64,
        dt::Float64, p0::Vector{Float64}, s::Vector{Float64}, s0::Vector{Float64})
    p = initialize_CJAD(p0)
    s = initialize_CJAD(s0)
    for i = 1:repTot
        lambda0 = TwoPhaseSystems.λwo.(fluid, s0)
        for j = 1:repPressure
            dp = dGrad(p)
            v = -upw(double(dp) .<= 0, lambda0).*T*dp
            pressureEq = dDiv(v)
            pressureEq[well.inIx] -= well.inRate
            pressureEq[well.outIx] -= q(lambda0[well.outIx], p[well.outIx])
        end
        dp0 = dGrad(p0)
        v = -upw(dp0 .<= 0, lambda0) .* T .* dp0
        for k = 1:repTransport
            fw = TwoPhaseSystems.fractionalWaterFlow(fluid, s)
            transportEq = (s - s0) + (dt./grid.pv) * dDiv(upw(dp0 .<= 0, fw) * v);
            transportEq[well.inIx] -= (dt/grid.pv[well.inIx]) * well.inRate
            transportEq[well.outIx] -= (dt./grid.pv[well.outIx]) * fw[well.outIx] * q(lambda0[well.outIx], p0[well.outIx])
        end
    end
end

function benchmarkAssemblySPE10()
    (G, grid, fluid, N, T, dt, totTime, well, p0, s0) = initializeSpe10Simulation()
    (dGrad, dDiv, upw, q, C) = initSPE10CJADFunctions()
    tps = TwoPhaseSystem(grid)
    matlabDict = read_matfile("master_thesis/mat_files/benchmarkSPE10Variables.mat")
    p = jvector(matlabDict["testP"])
    p = rand(length(p)).*maximum(p)
    s0 = jvector(matlabDict["testS0"])
    s = jvector(matlabDict["testS"])
    s = rand(length(s))
    dt = jvalue(matlabDict["testDt"])
    # benchmark variables
    nanoseconds = 10^(-9)
    repTotList = 150:50:160 # only 1 iteration as it stands now
    times = zeros(3,length(repTotList))
    i = 0
    for repTot in repTotList
        i += 1
        println("Benchmarking loop $i of $(length(repTotList)).")
        println("Assembling equations for $repTot time steps:")
        repPressure = 2
        repTransport = 6
        println("Julia LocalAD benchmark running")
        times[1,i] = median(@benchmark testTwoPhaseSystem($tps, $fluid, $grid,
                $well, $repTot, $repPressure, $repTransport, $dt, $p, $s, $s0)).time * nanoseconds
        println("Julia CJAD benchmark running")
        times[2,i] = median(@benchmark testTwoPhaseCJAD($dGrad, $dDiv, $upw,
            $q, $C, $T, $fluid, $grid, $well, $repTot, $repPressure,
            $repTransport, $dt, $p, $s, $s0)).time * nanoseconds
        println("Matlab benchmark running")
        times[3,i] = mxcall(:benchmarkSPE10Assembly,1, repTot, repPressure, repTransport)
    end
    println("Time LocalAD from Julia: $(times[1,:])")
    println("Time CJAD from Julia:    $(times[2,:])")
    println("Time from Matlab:        $(times[3,:])")
    println("Matlab / LocalAD:        $(times[3,:]./times[1,:])")
    return times
end

function btimeJuliaAssemblySPE10()
    (G, grid, fluid, N, T, dt, totTime, well, p0, s0) = initializeSpe10Simulation()
    (dGrad, dDiv, upw, q, C) = initSPE10CJADFunctions()
    tps = TwoPhaseSystem(grid)
    matlabDict = read_matfile("master_thesis/mat_files/benchmarkSPE10Variables.mat")
    p = jvector(matlabDict["testP"])
    p = rand(length(p)).*maximum(p)
    s0 = jvector(matlabDict["testS0"])
    s = jvector(matlabDict["testS"])
    s = rand(length(s))
    dt = jvalue(matlabDict["testDt"])
    # benchmark variables
    for repTot = 5:5:5
        repPressure = 2
        repTransport = 6
        println("Assembling equation for $repTot time steps:")
        println("Julia LocalAD benchmark running")
        @btime testTwoPhaseSystem($tps, $fluid, $grid,
                $well, $repTot, $repPressure, $repTransport, $dt, $p, $s, $s0)
        println("Julia CJAD benchmark running")
        @btime testTwoPhaseCJAD($dGrad, $dDiv, $upw,
            $q, $C, $T, $fluid, $grid, $well, $repTot, $repPressure,
            $repTransport, $dt, $p, $s, $s0)
    end
end

# remove comment to profile:
#=
(G, grid, fluid, N, T, dt, totTime, well, p0, s0) = initializeSpe10Simulation()
tps = TwoPhaseSystem(grid)
matlabDict = read_matfile("master_thesis/mat_files/benchmarkSPE10Variables.mat")
p = jvector(matlabDict["testP"])
s0 = jvector(matlabDict["testS0"])
s = jvector(matlabDict["testS"])
dt = jvalue(matlabDict["testDt"])
# benchmark variables
nanoseconds = 10^(-9)
repTot = 100
repPressure = 1
repTransport = 0
Profile.clear()
Profile.init(n = 10^10, delay = 0.0001)
Juno.@profiler testTwoPhaseSystem(tps, fluid, grid, well, repTot, repPressure, repTransport, dt, p, s, s0)
=#


times = benchmarkAssemblySPE10()
#btimeJuliaAssemblySPE10()
