export Grid
struct Grid
    T::SparseMatrixCSC{Float64, Int64}
    pv::Vector{Float64}
    N::Array{Int64,2}
    numCells::Int64



    function Grid(T, pv, N, numCells)
        numNeighbours, col = size(N)
        if col != 2
            error("Neighbourlist must be on the form n×2")
        elseif size(N,1) != length(T)
            error("Must have equal number of transimissibilities as facets")
        end
        rows = [N[:,1];N[:,2]]
        cols = [N[:,2];N[:,1]]
        T = sparse(rows, cols, [T; T])
        return new(T, pv, N, numCells)
    end
end



function Grid(type=nothing)# Needs to be changed
    if type == "2D"
        (G, N, T, gradz, pv_r) = setup2DGrid()
    else
        (dz, WI, wc, np, gradz, N, C, T, nf, cf, hT, p_init, pv_r, G) =
        setupFlowSolverLocalAD(1)
    end
    numCells = convert(Int64, G["cells"]["num"])
    return Grid(T, gradz, pv_r, N, numCells)
end
