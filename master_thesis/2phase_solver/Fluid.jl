export Fluid
struct Fluid
    μw::Float64
    μo::Float64
    ρw::Float64
    ρo::Float64
end

function Fluid()
    μw = 0.001
    μo = 0.01
    ρw = 1000
    ρo = 700
    return Fluid(μw, μo, ρw, ρo)
end

function fluidProperties(fluid::Fluid, type)
    if type == "μ"
        return (fluid.μw, fluid.μo)
    elseif type == "ρ"
        return (fluid.ρw, fluid.ρo)
    else
        return (fluid.μw, fluid.μo, fluid.ρw, fluid.ρo)
    end
end

function fluidRelativePermeability(saturation)
    return saturation^2
end
function λwater(fluid, waterSaturation)
    krw = fluidRelativePermeability(waterSaturation)
    return krw/fluid.μw
end
function λoil(fluid, waterSaturation)
    oilSat = 1 - waterSaturation
    kro = fluidRelativePermeability(oilSat)
    return kro/fluid.μo
end
λwo(fluid, waterSat) = λwater(fluid, waterSat) + λoil(fluid, waterSat)
function fractionalWaterFlow(fluid, waterSaturation)
    λw = λwater(fluid, waterSaturation)
    λo = λoil(fluid, waterSaturation)
    return λw / (λw + λo)
end

function capillarPressure(waterSaturation)
    return 0.0
end

import Base: length, iterate
length(fluid::Fluid) = 1
function iterate(iter::Fluid, state = 1)
    if state > 1
        return nothing
    end
    return (iter, state + 1)
end
