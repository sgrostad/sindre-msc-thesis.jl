using Test, LocalAD, MATLAB, LinearAlgebra, SparseArrays
using TwoPhaseSystems
mxcall(:runMatlabCode, 1, "initializeMRST");

function tests()
    @testset "testing TwoPhaseSolver:" begin
        @testset "test first pressure and sat val" begin
            #setup:
            cellArray = mxcall(:runMatlabCode, 1, "setup 2D model");
            (G, N, T, gradz, pv, wells, dt, totTime, p0, s0) = cellArray;
            fluid = Fluid();
            numCells = convert(Int64, G["cells"]["num"]);
            grid = Grid(T, pv, N, numCells);
            well = Well(wells[1], wells[2], [wells[3]], [wells[4]], [wells[5]])

            tps = TwoPhaseSystem(grid);
            tps.pressure .= p0;
            tps.waterSaturation .= s0;
            #pressure:
            TwoPhaseSystems.setAllJacobianValues!(tps, 1.0)
            assemblePressureEquations!(tps, fluid, grid, well);
            upd = -(tps.pressureEqJac\tps.pressureEq);
            tps.pressure .+= upd;
            residual = norm(tps.pressureEq);

            assemblePressureEquations!(tps, fluid, grid, well);
            upd = -(tps.pressureEqJac\tps.pressureEq);
            tps.pressure .+= upd;
            residual = norm(tps.pressureEq);
            # test
            matlabDict = read_matfile("master_thesis/mat_files/firstPressureValue.mat");
            corrPressure = jvector(matlabDict["p"]);
            @test tps.pressure ≈ corrPressure
            # transport:
            prevWaterSaturation = copy(tps.waterSaturation);

            assembleTransportEquations!(tps, fluid, grid, prevWaterSaturation, dt[1], well);
            upd = -(tps.transportEqJac\tps.transportEq);
            tps.waterSaturation .+= upd;
            residual = norm(tps.transportEq)

            matlabDict = read_matfile("master_thesis/mat_files/firstTransportEqValues.mat");
            corrEq = jvector(matlabDict["res"]);
            @test tps.transportEq ≈ corrEq
            matlabDict = read_matfile("master_thesis/mat_files/firstTransportEqJac.mat");
            corrEqJac = jsparse(matlabDict["J"]);
            @test tps.transportEqJac ≈ corrEqJac
            matlabDict = read_matfile("master_thesis/mat_files/firstSaturationUpdate.mat");
            corrS = jvector(matlabDict["s"]);
            @test tps.waterSaturation ≈ corrS
        end
    end
end
tests()
