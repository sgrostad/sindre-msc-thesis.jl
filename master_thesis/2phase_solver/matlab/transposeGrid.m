function GT = transposeGrid(G)
    GT = G;
    GT.cartDims = [G.cartDims(2) G.cartDims(1), G.cartDims(3)];
    GT.nodes.coords = [G.nodes.coords(:,2) G.nodes.coords(:,1) G.nodes.coords(:,3)];
    GT.cells.centroids = [G.cells.centroids(:,2) G.cells.centroids(:,1) G.cells.centroids(:,3)];
    GT.faces.centroids = [G.faces.centroids(:,2) G.faces.centroids(:,1) G.faces.centroids(:,3)];
    GT.faces.normals = [G.faces.normals(:,2) G.faces.normals(:,1) G.faces.normals(:,3)];
end