gravity off
layers = 5;
[G, W, rock] = getSPE10setup(layers);
[state, model, schedule]  = setupSPE10_AD('layers',layers);
G = model.G;
rock = model.rock;
low = 1e-4;
rock.poro(rock.poro < low) = low;
var = spe10Simulation("setup model");
G = var{1};
N = var{2};
T = var{3};
pv_r = var{4};
well = var{5};
dt = var{6};
Tf = var{7};
p0 = var{8};
s0 = var{9};

nstep = length(dt);
inRate = well{1};
inIx = well{2};
outPres = well{3};
outIx = well{4};
WI = well{5};
for i = 1:5
    W(i).compi = [1,0];
end
%% Fluid model
muW    = 1*centi*poise;
rhoW  = 1000*kilogram/meter^3;
krW    = @(S) S.^2;

% Oil phase is lighter and has a cubic relative permeability
muO    = 10*centi*poise;
rhoO  = 700*kilogram/meter^3;
krO    = @(S) S.^2;

%% Extract grid information
nf = G.faces.num;                                 % Number of faces
nc = G.cells.num;                                 % Number of cells
N  = double(G.faces.neighbors);                   % Map: face -> cell
F  = G.cells.faces(:,1);                          % Map: cell -> face
intInx = all(N ~= 0, 2);                          % Interior faces
N  = N(intInx, :);                                % Interior neighbors

%% Compute transmissibilities
hT = computeTrans(G, rock);                       % Half-transmissibilities
T  = 1 ./ accumarray(F, 1 ./ hT, [nf, 1]);        % Harmonic average
T  = T(intInx);                                   % Restricted to interior
nf = size(N,1);

%% Define discrete operators
D     = sparse([(1:nf)'; (1:nf)'], N, ones(nf,1)*[-1 1], nf, nc);
grad  = @(x)  D*x;
div   = @(x)-D'*x;
avg   = @(x) 0.5 * (x(N(:,1)) + x(N(:,2)));
upw   = @(flag, x) flag.*x(N(:, 1)) + ~flag.*x(N(:, 2));
gradz = grad(G.cells.centroids(:,3));

%% Injection/production
bhp = outPres;
q = @(lambda, p) -WI .* lambda .* (p - bhp);

%% Initialize for solution loop
%[p, sW]    = initVariablesADI(p0, s0);
p    = initVariablesADI(p0);
sW   = initVariablesADI(s0);
[pIx, sIx] = deal(1:nc, (nc+1):(2*nc));
[tol, maxits] = deal(1e-9,25);
pargs = {};% {'EdgeColor','k','EdgeAlpha',.1};

sol = repmat(struct('time',[],'pressure',[], 's', []),[nstep+1,1]);
sol(1) = struct('time', 0, 'pressure', double(p),'s', double(sW));

%% Main loop
t = 0;
%hwb = waitbar(t,'Simulation ..');
nits = nan(nstep,1);
i = 1; clear cnd;
n = 0;
while t < Tf
    n = n+1;
    t = t + dt(n);
    fprintf('\nTime step %d: Time %.2f -> %.2f days\n', ...
        n, convertTo(t - dt(n), day), convertTo(t, day));

    % Newton loop for pressure. Constant saturation.
    resNorm   = 1e99;
    [p0, s0] = deal(double(p), double(sW));
    nit = 0;
    
    mobW0 = krW(s0)./muW;
    mobO0 = krO(1-s0)./muO;
    lambda0 = mobW0 + mobO0;
    while (resNorm > tol) && (nit <= maxits)

        % Pressure differences across each interface
        dp  = grad(p);

        % flux:
        v  = -upw(double(dp) <= 0, lambda0).*T.*dp;

        % Conservation of water and oil
        pressureEq = div(v);

        % Injector: volumetric source term
        pressureEq(inIx) = pressureEq(inIx) - inRate;

        % Producer: 
        pressureEq(outIx) = pressureEq(outIx) - q(lambda0(outIx), p(outIx));


        % Measure condition number
        cnd(i) = condest(pressureEq.jac{1}); i = i+1;                              %#ok<SAGROW>

        % Compute Newton update and update variable
        res = pressureEq.val;
        upd = -(pressureEq.jac{1} \ res);
        p.val  = p.val   + upd;

        resNorm = norm(res);
        nit     = nit + 1;
        fprintf('  Iteration %3d:  Res = %.4e\n', nit, resNorm);
    end
    if nit > maxits,
        error('Newton solver for pressure did not converge')
    end
    % Newton loop for saturation. Constant pressure.
    resNorm   = 1e99;
    p0 = double(p);
    nit = 0;
    while (resNorm > tol) && (nit <= maxits)

        % Mobility: Relative permeability over constant viscosity
        mobW = krW(sW)./muW;
        mobO = krO(1-sW)./muO;
        lambda = mobW + mobO;
        fw = mobW./(mobW + mobO);
        

        % Pressure differences across each interface
        dp  = grad(p0);

        % flux:
        v  = -upw(double(dp) <= 0, lambda0).*T.*dp;

        % Conservation of water and oil
        transportEq = (sW - s0) + (dt(n)./pv_r) .* div(upw(double(dp) <= 0, fw) .* v);
        %transportEq = (sW - s0) + (dt(n)./pv_r) .* fw .* div(v);
        % Think last one is wrong

        % Injector: volumetric source term
        transportEq(inIx) = transportEq(inIx) - dt(n)/pv_r(inIx) * inRate;

        % Producer: 
        transportEq(outIx) = transportEq(outIx) - ...
            dt(n)/pv_r(outIx) * fw(outIx) * q(lambda0(outIx), p0(outIx));

        % Measure condition number
        cnd(i) = condest(transportEq.jac{1}); i = i+1;                              %#ok<SAGROW>

        % Compute Newton update and update variable
        res = transportEq.val;
        upd = -(transportEq.jac{1} \ res);
        sW.val  = sW.val   + upd;
        sW.val = max( min(sW.val, 1), 0);

        resNorm = norm(res);
        nit     = nit + 1;
        fprintf('  Iteration %3d:  Res = %.4e\n', nit, resNorm);
    end
    % Add line with NaN in cnd variable to signify end of time step
    cnd(i) = NaN; i=i+1;                                                   %#ok<SAGROW>

    if nit > maxits,
        error('Newton solver for transport did not converge')
    else % plot
        sol(n+1) = struct('time', t, ...
                          'pressure', double(p), ...
                          's', double(sW));
    end
end
%close(hwb);

%%
intervals = floor(length(sol)/20);
for i = 1:intervals:length(sol)
    clf
    show = true([G.cells.num, 1]);
    plotCellData(G, sol(i).s, show, 'EdgeColor', 'k')
    axis tight off
    title(['Water front after ' formatTimeRange(sum(dt(1:i)))])
    colorbar
    drawnow
    pause(0.5)
end


%{
Copyright 2009-2018 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

