mrstModule add incomp
mrstModule add mimetic incomp deckformat
mrstVerbose false
gravity off
nx = 10; ny = 10; nz = 1;
G         = cartGrid([nx ny nz]);
% [nx, ny, nz] = deal( 100, 100, 1);
% [Dx, Dy, Dz] = deal(100, 100, 1);
% G = cartGrid([nx, ny, nz], [Dx, Dy, Dz]);

G         = computeGeometry(G);
rock.perm = repmat(100*milli*darcy, [G.cells.num, 1]);
rock.poro = repmat(0.3            , [G.cells.num, 1]);

%%
[G, W, rock] = getSPE10setup(layers);
low = 1e-4;
rock.poro(rock.poro < low) = low;
for i = 1:length(W)
    if W(i).sign < 0
        W(i).compi = [1,0];
    else
        W(i).compi = [0,1];
    end
end

%%

x = linspace(0, 1, 101) .';
y = linspace(1, 0, 101) .';
%%
pc_form = 'nonwetting';
cap_scale = 10;
[kr, pc]  = tabulatedSatFunc([x, x.^2, y.^2, y.*cap_scale*barsa]);
props = constantProperties([   1,  10] .* centi*poise, ...
                           [1000, 700] .* kilogram/meter^3);
fluid = struct('properties', props                  , ...
               'saturation', @(x, varargin)    x.s  , ...
               'relperm'   , kr);
%%
%{
rate = 0.5*meter^3/day;
bhp  = 1*barsa;

W = verticalWell(W, G, rock, 1, 1, 1:nz,          ...
                 'Type', 'rate', 'Val', rate, ...
                 'Radius', .1, 'Name', 'I', 'Comp_i', [1 0]);
W = verticalWell(W, G, rock, nx, ny, 1:nz,     ...
                 'Type','bhp', 'Val', bhp, ...
                 'Radius', .1, 'Dir', 'x', 'Name', 'P', 'Comp_i', [0 1]);
%}
rSol    = initState(G, W, 0, [0.0, 1.0]);
%%

gravity off
verbose = false;

S  = computeMimeticIP(G, rock, 'Verbose', verbose,'InnerProduct','ip_tpf');
%%
psolve  = @(state, fluid) incompMimetic(state, G, S, fluid, 'wells', W);
tsolve  = @(state, dT, fluid) implicitTransport(state, G, dT, rock, ...
                                                fluid, 'wells', W, ...
                                                'verbose', verbose);
%%
rSol    = psolve(rSol, fluid);
%%
dt = [.001*day; .1*day*ones(5,1); 1*day*ones(10,1); 10*day*ones(100,1)];
T = sum(dt);
pv     = poreVolume(G,rock);

%%
t  = 0; plotNo = 1;
h1 = 'No pc - '; H2 = 'Linear pc - ';
e = []; p_org = []; p_pc = [];
figure;
i = 0;
while t < T,
    i = i + 1
    dT = dt(i);
    % TRANSPORT SOLVE
    rSol    = tsolve(rSol, dT, fluid);

    % Check for inconsistent saturations
    s = [rSol.s(:,1)];
    assert(max(s) < 1+eps && min(s) > -eps);

    % Update solution of pressure equation.
    rSol    = psolve(rSol,    fluid);

    % Measure water saturation in production cells in saturation

    p_org = [p_org; rSol.s(W(2).cells,1)' ];                 %#ok
    % Increase time and continue if we do not want to plot saturations
    %plotGrid(G, s > 0, 'facea', .3, 'facec', 'red', 'edgea', 0);
    plotGridVolumes(G, s);
    plotWell(G, W);
    plotGrid(G, 'facea', 0, 'edgea', .05);
    view(-60,  70); 
    axis tight off
    title(['Water front after ' formatTimeRange(sum(dt(1:i(1))))])
    drawnow
    t = t + dT;
end

    plotGrid(G, s{i} > 0, 'facea', .3, 'facec', 'red', 'edgea', 0);
    %plotGridVolumes(G, s);
    plotWell(G, W);
    plotGrid(G, 'facea', 0, 'edgea', .05);
    view(-60,  70); 
    axis tight off
    title(['Water front after ' formatTimeRange(sum(dt(1:i(1))))])
    drawnow
    colorbar
