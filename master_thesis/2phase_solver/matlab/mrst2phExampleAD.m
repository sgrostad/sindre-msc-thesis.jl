%% Set up model geometry
[nx,ny,nz] = deal(  10,  10,  1);
[Dx,Dy,Dz] = deal(10, 10, 1);
G = cartGrid([nx, ny, nz], [Dx, Dy, Dz]);
G = computeGeometry(G);

%% Define rock model
rock = makeRock(G, 100*milli*darcy, 0.3);
pv_r  = poreVolume(G, rock);

%% Fluid model
muW    = 1*centi*poise;
rhoW  = 1000*kilogram/meter^3;
krW    = @(S) S.^2;

% Oil phase is lighter and has a cubic relative permeability
muO    = 10*centi*poise;
rhoO  = 700*kilogram/meter^3;
krO    = @(S) S.^2;

%% Extract grid information
nf = G.faces.num;                                 % Number of faces
nc = G.cells.num;                                 % Number of cells
N  = double(G.faces.neighbors);                   % Map: face -> cell
F  = G.cells.faces(:,1);                          % Map: cell -> face
intInx = all(N ~= 0, 2);                          % Interior faces
N  = N(intInx, :);                                % Interior neighbors

%% Compute transmissibilities
hT = computeTrans(G, rock);                       % Half-transmissibilities
T  = 1 ./ accumarray(F, 1 ./ hT, [nf, 1]);        % Harmonic average
T  = T(intInx);                                   % Restricted to interior
nf = size(N,1);

%% Define discrete operators
D     = sparse([(1:nf)'; (1:nf)'], N, ones(nf,1)*[-1 1], nf, nc);
grad  = @(x)  D*x;
div   = @(x)-D'*x;
avg   = @(x) 0.5 * (x(N(:,1)) + x(N(:,2)));
upw   = @(flag, x) flag.*x(N(:, 1)) + ~flag.*x(N(:, 2));
gradz = grad(G.cells.centroids(:,3));

%% Schedule and injection/production
rate = 0.5*meter^3/day;
bhp = 1*barsa;
nstep = 36;
Tf = 360*day;
dt = Tf/nstep*ones(1,nstep);
dt = [dt(1).*sort(repmat(2.^-[1:5 5],1,1)) dt(2:end)];
nstep = numel(dt);
[inRate,  inIx ] = deal(.1*sum(pv_r)/Tf, 1);
inRate = rate; 
[outPres, outIx] = deal(bhp, G.cells.num);
W = verticalWell([], G, rock, nx, ny, 1:nz,     ...
                 'Type','bhp', 'Val', bhp, ...
                 'Radius', .1, 'Dir', 'x', 'Name', 'P', 'Comp_i', [0 1]);
WI = W.WI;
W = verticalWell([], G, rock, 1, 1, 1:nz,          ...
                 'Type', 'rate', 'Val', rate, ...
                 'Radius', .1, 'Name', 'I', 'Comp_i', [1 0]);
q = @(bhp, lambda, p) -WI * lambda * (p - bhp);

%% Impose vertical equilibrium
gravity off,
rSol    = initState(G, W, 0, [0.2, 0.8]);
p0 = rSol.pressure;
s0 = rSol.s(:,1);
%% Initialize for solution loop
%[p, sW]    = initVariablesADI(p0, s0);
p    = initVariablesADI(p0);
sW   = initVariablesADI(s0);
[pIx, sIx] = deal(1:nc, (nc+1):(2*nc));
[tol, maxits] = deal(1e-9,25);
pargs = {};% {'EdgeColor','k','EdgeAlpha',.1};

sol = repmat(struct('time',[],'pressure',[], 's', []),[nstep+1,1]);
sol(1) = struct('time', 0, 'pressure', double(p),'s', double(sW));

%% Main loop
t = 0;
%hwb = waitbar(t,'Simulation ..');
nits = nan(nstep,1);
i = 1; clear cnd;
for n=1:nstep

    t = t + dt(n);
    fprintf('\nTime step %d: Time %.2f -> %.2f days\n', ...
        n, convertTo(t - dt(n), day), convertTo(t, day));

    % Newton loop for pressure. Constant saturation.
    resNorm   = 1e99;
    [p0, s0] = deal(double(p), double(sW));
    nit = 0;
    
    mobW0 = krW(s0)./muW;
    mobO0 = krO(1-s0)./muO;
    lambda0 = mobW0 + mobO0;
    while (resNorm > tol) && (nit <= maxits)

        % Pressure differences across each interface
        dp  = grad(p);

        % flux:
        v  = -upw(double(dp) <= 0, lambda0).*T.*dp;

        % Conservation of water and oil
        pressureEq = div(v);

        % Injector: volumetric source term
        pressureEq(inIx) = pressureEq(inIx) - inRate;

        % Producer: 
        pressureEq(outIx) = pressureEq(outIx) - q(bhp, lambda0(outIx), p(outIx));


        % Measure condition number
        cnd(i) = condest(pressureEq.jac{1}); i = i+1;                              %#ok<SAGROW>

        % Compute Newton update and update variable
        res = pressureEq.val;
        upd = -(pressureEq.jac{1} \ res);
        p.val  = p.val   + upd;

        resNorm = norm(res);
        nit     = nit + 1;
        fprintf('  Iteration %3d:  Res = %.4e\n', nit, resNorm);
    end
    if nit > maxits,
        error('Newton solver for pressure did not converge')
    end
    % Newton loop for saturation. Constant pressure.
    resNorm   = 1e99;
    p0 = double(p);
    nit = 0;
    while (resNorm > tol) && (nit <= maxits)

        % Mobility: Relative permeability over constant viscosity
        mobW = krW(sW)./muW;
        mobO = krO(1-sW)./muO;
        lambda = mobW + mobO;
        fw = mobW./(mobW + mobO);
        

        % Pressure differences across each interface
        dp  = grad(p0);

        % flux:
        v  = -upw(double(dp) <= 0, lambda0).*T.*dp;

        % Conservation of water and oil
        transportEq = (sW - s0) + (dt(n)./pv_r) .* div(upw(double(dp) <= 0, fw) .* v);
        %transportEq = (sW - s0) + (dt(n)./pv_r) .* fw .* div(v);
        % Think last one is wrong

        % Injector: volumetric source term
        transportEq(inIx) = transportEq(inIx) - dt(n)/pv_r(inIx) * inRate;

        % Producer: 
        transportEq(outIx) = transportEq(outIx) - ...
            dt(n)/pv_r(outIx) * fw(outIx) * q(bhp, lambda0(outIx), p0(outIx));

        % Measure condition number
        cnd(i) = condest(transportEq.jac{1}); i = i+1;                              %#ok<SAGROW>

        % Compute Newton update and update variable
        res = transportEq.val;
        upd = -(transportEq.jac{1} \ res);
        sW.val  = sW.val   + upd;
        sW.val = max( min(sW.val, 1), 0);

        resNorm = norm(res);
        nit     = nit + 1;
        fprintf('  Iteration %3d:  Res = %.4e\n', nit, resNorm);
    end
    % Add line with NaN in cnd variable to signify end of time step
    cnd(i) = NaN; i=i+1;                                                   %#ok<SAGROW>

    if nit > maxits,
        error('Newton solver for transport did not converge')
    else % plot
        nits(n) = nit;

        figure(1)
        clf
        plotCellData(G, double(sW), pargs{:});
        caxis([0, 1]); title('Saturation')
        colorbar
        drawnow

        sol(n+1) = struct('time', t, ...
                          'pressure', double(p), ...
                          's', double(sW));
        %waitbar(t/Tf,hwb);
        pause(.1);
    end
end
%close(hwb);

%%
%{
figure(2),
subplot(2,1,2);
bar(nits,1,'EdgeColor','r','FaceColor',[.6 .6 1]);
axis tight, set(gca,'YLim',[0 10]);
hold on,
plotyy(nan,nan,1:numel(dt),dt/day,@plot, ...
    @(x,y) plot(x,y,'-o','MarkerFaceColor',[.6 .6 .6]));

%%
figure(3); hold all
ipr = arrayfun(@(x) x.pressure(inIx), sol)/barsa;
t   = arrayfun(@(x) x.time, sol)/day;
fpr = arrayfun(@(x) sum(x.pressure)/G.cells.num, sol)/barsa;
plot(t,ipr,'-', t,fpr, '--');
% chl = get(gca,'Children');
% col=flipud(lines(2));
% for i=1:numel(chl), set(chl(i),'Color',col(ceil(i/2),:),'LineWidth',2); end

%%
figure(4); hold all
oip = arrayfun(@(x) sum(pv(x.pressure).*(1-x.s).*rhoO(x.pressure)), sol);
plot(t,(ioip - oip)./rhoOS,'-o','MarkerFaceColor',[.6 .6 .6])

%%
figure(5); hold all
plot(cumsum(isnan(cnd))+1, 1./cnd,'-o');
%}
%%
%{
g = cartGrid([1 1 1],[Dx Dy Dz]);
figure; colormap(winter);
for i=1:nstep+1
    s = sol(i).s;
    clf
    plotCellData(G,1-s,s>1e-3,'EdgeColor','k','EdgeAlpha',.1);
    plotGrid(g,'EdgeColor','k','FaceColor','none');
    view(3); plotGrid(G,[inIx; outIx],'FaceColor',[.6 .6 .6]);
    view(30,40); caxis([0 1]); axis tight off
    axis([-.5 Dx+.5 -.5 Dy+.5 -.5 Dz+.5]);
    h=colorbar('horiz');
    pos = get(gca,'Position');
    set(h,'Position',[.13 .15 .775 .03]); set(gca,'Position',pos);
    title(['Time: ' num2str(t(i)) ' days']);
    drawnow, pause(.5)
    print('-dpng','-r0',sprintf('twoPhaseAD-%02d.png',i-1));
end
%}


%{
Copyright 2009-2018 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

