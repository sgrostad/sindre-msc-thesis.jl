function res = benchmarkSPE10Assembly(repTot, repPressure, repTransport)
%% Setup simulation:
layers = 5;
[G, W, rock] = getSPE10setup(layers);
[state, model, schedule]  = setupSPE10_AD('layers',layers);
G = model.G;
rock = model.rock;
low = 1e-4;
rock.poro(rock.poro < low) = low;
var = spe10Simulation("setup model");
G = var{1};
N = var{2};
T = var{3};
pv_r = var{4};
well = var{5};
dt = var{6};
Tf = var{7};
p0 = var{8};
s0 = var{9};
nstep = length(dt);
inRate = well{1};
inIx = well{2};
outPres = well{3};
outIx = well{4};
WI = well{5};
for i = 1:5
    W(i).compi = [1,0];
end
muW    = 1*centi*poise;
rhoW  = 1000*kilogram/meter^3;
krW    = @(S) S.^2;
muO    = 10*centi*poise;
rhoO  = 700*kilogram/meter^3;
krO    = @(S) S.^2;
nf = G.faces.num;                                 % Number of faces
nc = G.cells.num;                                 % Number of cells
N  = double(G.faces.neighbors);                   % Map: face -> cell
F  = G.cells.faces(:,1);                          % Map: cell -> face
intInx = all(N ~= 0, 2);                          % Interior faces
N  = N(intInx, :);                                % Interior neighbors
hT = computeTrans(G, rock);                       % Half-transmissibilities
T  = 1 ./ accumarray(F, 1 ./ hT, [nf, 1]);        % Harmonic average
T  = T(intInx);                                   % Restricted to interior
nf = size(N,1);
D     = sparse([(1:nf)'; (1:nf)'], N, ones(nf,1)*[-1 1], nf, nc);
grad  = @(x)  D*x;
div   = @(x)-D'*x;
avg   = @(x) 0.5 * (x(N(:,1)) + x(N(:,2)));
upw   = @(flag, x) flag.*x(N(:, 1)) + ~flag.*x(N(:, 2));
gradz = grad(G.cells.centroids(:,3));
bhp = outPres;
q = @(lambda, p) -WI .* lambda .* (p - bhp);


%% benchmark begins:
totTests = 5;
load('/Users/sindregrostad/git/sindre-msc-thesis.jl/master_thesis/mat_files/benchmarkSPE10Variables.mat','testP', 'testP0', 'testS', 'testS0', 'testDt')
testP = rand(size(testP))*max(testP);
testS = rand(size(testS));

benchmarkTimes = zeros(totTests,1);
for test = 1:totTests
    tic
    p    = initVariablesADI(testP);
    p0 = testP0;
    sW   = initVariablesADI(testS);
    res = zeros(length(p0),1);
    s0 = testS0;
    dt = testDt;
    for i = 1:repTot
        mobW0 = krW(s0)./muW;
        mobO0 = krO(1-s0)./muO;
        lambda0 = mobW0 + mobO0;
        for j = 1:repPressure
            dp  = grad(p);
            v  = -upw(double(dp) <= 0, lambda0).*T.*dp;
            pressureEq = div(v);
            pressureEq(inIx) = pressureEq(inIx) - inRate;
            pressureEq(outIx) = pressureEq(outIx) - q(lambda0(outIx), p(outIx));
        end
        for k = 1:repTransport
            mobW = krW(sW)./muW;
            mobO = krO(1-sW)./muO;
            fw = mobW./(mobW + mobO);
            dp  = grad(p0);
            v  = -upw(double(dp) <= 0, lambda0).*T.*dp;
            transportEq = (sW - s0) + (dt./pv_r) .* div(upw(double(dp) <= 0, fw) .* v);
            transportEq(inIx) = transportEq(inIx) - dt/pv_r(inIx) * inRate;
            transportEq(outIx) = transportEq(outIx) - ...
                dt/pv_r(outIx) * fw(outIx) * q(lambda0(outIx), p0(outIx));
        end
    end
    benchmarkTimes(test) = toc;
end
res = median(benchmarkTimes);
end