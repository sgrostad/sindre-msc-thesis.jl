%% setup MRST
runMatlabCode("initializeMRST");
mrstModule add ad-fi deckformat spe10
%% Create plots for SPE 10
layers = 5;
[G, W, rock] = getSPE10setup(layers);
[state, model, schedule]  = setupSPE10_AD('layers',layers);
G = model.G;
rock = model.rock;
low = 1e-4;
rock.poro(rock.poro < low) = low;
GT = transposeGrid(G);
%% Permeability
f1 = figure;
plotCellData(GT, log10(rock.perm(:,1)), 'edgeColor', 'none');
plotWell(GT, W)
axis tight
c = colorbar;
c.Location = 'southoutside';
c.Label.String = 'log_{10}[m^2]';
c.Label.FontSize = 14;
pbaspect(GT.cartDims)
title('Permeability')

%% Porosity
f2 = figure;
plotCellData(GT, rock.poro, 'edgeColor', 'none');
plotWell(GT, W)
axis tight
c = colorbar;
c.Location = 'southoutside';
pbaspect(GT.cartDims)
title('Porosity')

%% WaterSaturation
s = load('/Users/sindregrostad/git/sindre-msc-thesis.jl/master_thesis/mat_files/allResultsTwoPhaseSimulation.mat', "sol");
sol = s.sol;
plotIx = [10,62, 100, 150];
figure
for i = 1:length(plotIx)
    t = plotIx(i);
    ax(i) = subplot(4,1,i);
    plotCellData(GT, sol(t).s, 'edgeColor', 'none')
    axis tight off
    title(['Water saturation after ' formatTimeRange(sol(t).time)])
    pbaspect(GT.cartDims)
    ax(i).LineWidth = 1;
end
h=colorbar;
set(h, 'Position', [.68314 .11 .0581 .8150])
set(h,'Limits',[0 1])
for i=1:4
      pos=get(ax(i), 'Position');
      set(ax(i), 'Position', [pos(1) pos(2) 0.7*pos(3) pos(4)]);
end

