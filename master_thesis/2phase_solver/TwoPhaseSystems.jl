module TwoPhaseSystems
using LocalAD, MATLAB
using SetupFlowSolver
using StaticArrays, SparseArrays
export TwoPhaseSystem, assemblePressureEquations!, assembleTransportEquations!
export initializePressureAndSat
include("Fluid.jl")
include("GridProperties.jl")
include("Wells.jl")
import Base: size, length
struct TwoPhaseSystem
    pressure::Vector{Float64}
    waterSaturation::Vector{Float64}

    pressureEq::Vector{Float64}
    pressureEqJac::SparseMatrixCSC{Float64,Int}

    transportEq::Vector{Float64}
    transportEqJac::SparseMatrixCSC{Float64,Int}
end
include("Equations.jl")
function TwoPhaseSystem(grid::Grid)
    diag = collect(1:grid.numCells)
    rows = [grid.N[:,1]; grid.N[:,2]; diag]
    cols = [grid.N[:,2]; grid.N[:,1]; diag]
    elementsInJac = length(cols)
    nzval = zeros(elementsInJac)
    matrixInit = sparse(rows, cols, nzval)
    vectorInit = zeros(grid.numCells)
    return TwoPhaseSystem(copy(vectorInit), copy(vectorInit), copy(vectorInit),
        copy(matrixInit), copy(vectorInit), copy(matrixInit))
end
function initializePressureAndSat(tps::TwoPhaseSystem, p0::Vector{Float64},
    waterSaturation::Vector{Float64})
    tps.pressure .= p0
    tps.waterSaturation .= waterSaturation
end
#Saturation is fixed
function assemblePressureEquations!(tps::TwoPhaseSystem, fluid::Fluid,
        grid::Grid, well::Well)
    resetPressureEquations!(tps)
    for variable = 1:size(tps,2)
        neighbourIndices =
            tps.pressureEqJac.colptr[variable]:(tps.pressureEqJac.colptr[variable + 1] - 1)
        for eqIx in neighbourIndices
            eq = tps.pressureEqJac.rowval[eqIx]
            if variable == eq
                continue
            end
            fluxLAD = flux(variable, eq, fluid, tps, grid)
            tps.pressureEq[eq] -= fluxLAD.val
            tps.pressureEqJac[eq, variable] -= fluxLAD.derivatives[1]
            tps.pressureEqJac[variable, variable] += fluxLAD.derivatives[1]
        end
    end
    # Handle Injector:
    eq = well.inIx
    tps.pressureEq[eq] -= well.inRate
    #Handle Wells:
    wellIx = 0
    for eq in well.outIx
        wellIx += 1
        pressure = createVariable(tps.pressure[eq], 1)
        q = -totalProduction(pressure,
            tps.waterSaturation[eq], fluid, well, wellIx)
        tps.pressureEq[eq] -= q.val
        tps.pressureEqJac[eq, eq] -= q.derivatives[1]
    end
end
#pressure is fixed
function assembleTransportEquations!(tps::TwoPhaseSystem, fluid::Fluid,
        grid::Grid, prevWaterSaturation::Vector{Float64}, dt::Float64, well::Well)
    resetTransportEquations!(tps)
    for variable = 1:size(tps,2)
        neighbourIndices =
        tps.transportEqJac.colptr[variable]:(tps.transportEqJac.colptr[variable + 1]- 1)
        for eqIx in neighbourIndices
            eq = tps.transportEqJac.rowval[eqIx]
            pv = grid.pv[eq]
            if variable == eq
                timeDerivLAD = timeDerivative(eq, tps,
                    prevWaterSaturation[eq])
                tps.transportEq[eq] += timeDerivLAD.val
                tps.transportEqJac[eq,eq] += timeDerivLAD.derivatives[1]
                continue
            end
            waterFluxLAD = dt/pv * waterFlux(variable, eq, fluid, tps, grid, prevWaterSaturation)
            tps.transportEq[eq] -= waterFluxLAD.val
            tps.transportEqJac[eq, variable] -= waterFluxLAD.derivatives[1]
            tps.transportEqJac[variable, variable] += waterFluxLAD.derivatives[1]
        end
    end
    #Handle Injector:
    eq = well.inIx
    tps.transportEq[eq] -= dt/grid.pv[eq] * well.inRate
    #Handle Wells:
    wellIx = 0
    for eq in well.outIx
        wellIx += 1
        waterSat = createVariable(tps.waterSaturation[eq], 1)
        fw = fractionalWaterFlow(fluid, waterSat)
        q = totalProduction(tps.pressure[eq],
            prevWaterSaturation[eq], fluid, well, wellIx)
        waterWellFlux = -dt/grid.pv[eq] * q * fw
        tps.transportEq[eq] -=  waterWellFlux.val
        tps.transportEqJac[eq,eq] -= waterWellFlux.derivatives[1]
    end
end
function setAllJacobianValues!(tps::TwoPhaseSystem, newVal::Float64)
    for i = 1:length(tps.pressureEqJac.nzval)
        tps.pressureEqJac.nzval[i] = newVal
    end
    for i = 1:length(tps.transportEqJac.nzval)
        tps.transportEqJac.nzval[i] = newVal
    end
end
function setPressureJacobianValues!(tps::TwoPhaseSystem, newVal::Float64)
    for i = 1:length(tps.pressureEqJac.nzval)
        tps.pressureEqJac.nzval[i] = newVal
    end
end
function setTransportJacobianValues!(tps::TwoPhaseSystem, newVal::Float64)
    for i = 1:length(tps.transportEqJac.nzval)
        tps.transportEqJac.nzval[i] = newVal
    end
end
function resetAllEquations!(tps::TwoPhaseSystem)
    for i = 1:length(tps)
        tps.pressureEq[i] = 0.0
        tps.transportEq[i] = 0.0
    end
    setAllJacobianValues!(tps, 0.0)
end
function resetPressureEquations!(tps::TwoPhaseSystem)
    for i = 1:length(tps)
        tps.pressureEq[i] = 0.0
    end
    setPressureJacobianValues!(tps, 0.0)
end
function resetTransportEquations!(tps::TwoPhaseSystem)
    for i = 1:length(tps)
        tps.transportEq[i] = 0.0
    end
    setTransportJacobianValues!(tps, 0.0)
end


function size(tps::TwoPhaseSystem, dim::Int)
    if dim == 1
        return length(tps.pressureEq)
    elseif dim == 2
        return size(tps.pressureEqJac,2)
    else
        error("TwoPhaseSystem only has two dimensions. (\"dim\" = 1 or 2)")
    end
end
size(tps::TwoPhaseSystem) = (size(tps, 1), size(tps, 2))
length(tps::TwoPhaseSystem) = size(tps, 1)
end
