module InitSPE10Sim
using MATLAB, TwoPhaseSystems, CustomJacobianAD
export initializeSpe10Simulation, initSPE10CJADFunctions

function initializeSpe10Simulation()
    cellArray = mxcall(:spe10Simulation, 1, "setup model")
    (G, N, T, pv, wells, dt, totTime, p0, s0) = cellArray
    fluid = Fluid()
    numCells = convert(Int64, G["cells"]["num"])
    grid = Grid(T, pv, N, numCells)
    well = Well(wells[1], wells[2], wells[3], wells[4], wells[5])
    return (G, grid, fluid, N, T, dt, totTime, well, p0, s0)
end

function initSPE10CJADFunctions()
    (G, grid, fluid, N, T, dt, totTime, well, p0, s0) = initializeSpe10Simulation()
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)]))
    dGrad(x) =   C*x;
    dDiv(x) = -C'*x;
    upw(flag, x::Vector{Float64}) = flag.*x[grid.N[:, 1]] + .!flag.*x[grid.N[:, 2]];
    upw(flag, x::CJAD) = flag*x[grid.N[:, 1]] + .!flag*x[grid.N[:, 2]];
    q(lambda::Vector{Float64}, p::CJAD) =  -well.WI .* lambda * (p - well.outPres);
    q(lambda::Vector{Float64}, p::Vector{Float64}) =  -well.WI .* lambda .* (p - well.outPres);
    return (dGrad, dDiv, upw, q, C)
end
end  # module InitSPE10Sim
