using TwoPhaseSystems, MATLAB, Printf, SetupFlowSolver, LinearAlgebra
using CustomJacobianAD, SparseArrays, InitSPE10Sim
mxcall(:runMatlabCode, 1, "initializeMRST");

function SPE10LocalAD()
    (G, grid, fluid, N, T, dt, totTime, well, p0, s0) = initializeSpe10Simulation()

    tps = TwoPhaseSystem(grid)
    tps.pressure .= p0
    tps.waterSaturation .= s0
    secondsInDay = 86400.0
    smallTol = 1e-4
    tol = 1e-8
    maxIts = 40
    t = 0.0
    stepNumber = 0
    i = 0
    saturations = Vector{Vector{Float64}}(undef,length(dt))
    while t < totTime
        i += 1
        t += dt[i]
        stepNumber += 1
        @printf("\nTime step %d: Time %.2f -> %.2f days\n",
            stepNumber, (t-dt[i])/secondsInDay, t/secondsInDay)
        println("Finished with $(i/length(dt)*100)% of the simulation.")
        #Newton loop
        converged = false
        nit = 0
        ## Pressure equation:
        while !converged && (nit < maxIts)
            assemblePressureEquations!(tps, fluid, grid, well)
            upd = -(tps.pressureEqJac\tps.pressureEq)
            tps.pressure .+= upd
            residual = norm(tps.pressureEq)
            if nit > 10
                converged = (residual < smallTol)
            else
                converged = (residual<tol)
            end
            nit       = nit + 1;
            println("Pressure iteration $nit:  Res = $residual")
        end
        if !converged
            error("Newton solver did not converge for pressure equation")
        end
        ## Transport equation:
        nit = 0
        converged = false
        prevWaterSaturation = copy(tps.waterSaturation)
        while !converged && (nit < maxIts)
            assembleTransportEquations!(tps, fluid, grid, prevWaterSaturation, dt[i], well)

            upd = -(tps.transportEqJac\tps.transportEq)
            tps.waterSaturation .+= upd
            residual = norm(tps.transportEq)
            if nit > 10
                converged = (residual < smallTol)
            else
                converged = (residual<tol)
            end
            nit       = nit + 1;
            println("Transport iteration $nit:  Res = $residual")
            if maximum(tps.waterSaturation) > 1
                println("waterSaturation above 1! \n Max is $(maximum(tps.waterSaturation))")
                tps.waterSaturation .= max.(min.(tps.waterSaturation,1),0)
            end
        end
        if !converged
            println("Newton solver did not converge for transport equation")
            saturations[i] = tps.waterSaturation
            return saturations[1:i-1], dt
        end
        saturations[i] = copy(tps.waterSaturation)
    end
    return saturations, dt

end
saturations, dt = SPE10LocalAD()
mxcall(:spe10Simulation, 1, "plot solution",
    mxarray([mxarray(saturations) mxarray(dt) mxarray(collect(1:length(saturations)))]))
#mxcall(:spe10Simulation, 1, "plot solution", mxarray([mxarray(saturations[end]) mxarray(sum(dt)) mxarray(1)]))
