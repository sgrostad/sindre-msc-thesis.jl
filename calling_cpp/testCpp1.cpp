//
//  testCpp1.cpp
//  callingCppFromJulia
//
//  Created by Sindre Grøstad on 15/01/2019.
//  Copyright © 2019 Sindre Grøstad. All rights reserved.
//

#include "testCpp1.hpp"

double cppFunction1(double a, double b){
    return a*b*getCppNumber();
}

double getCppNumber(){
    return 34.2;
}
