//
//  testCpp1.hpp
//  callingCppFromJulia
//
//  Created by Sindre Grøstad on 15/01/2019.
//  Copyright © 2019 Sindre Grøstad. All rights reserved.
//

#ifndef testCpp1_hpp
#define testCpp1_hpp

#include <stdio.h>
double cppFunction1(double a, double b);
double getCppNumber();
#endif /* testCpp1_hpp */
