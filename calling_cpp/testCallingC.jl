# simple_built_in is a function that takes calls a standard library in c. Mean
# is a function that uses the shared library libmean that was created by
# makefile with the command "make lib". This makefile must be changed if the
# shared library has more dependencies.

# In Juno you also need to restart Julia every time you recompile the source
# files to the library, or the old one will be used.
using Libdl
function simple_built_in()
    t = ccall((:clock, "libc"), Int32, ())
    println(t)
end

function testC()
    pathToDir = convert(String, @__DIR__)
    pathToLib = pathToDir * "/callingCppFromJulia/callingCppFromJulia/lib.so"
    mylib = dlopen(pathToLib)
    x=ccall(dlsym(mylib, :function1), Float64,(Float64,Float64),2,5)
    println("function1():")
    println(x)
    ## Alternatively:
    println("mean(2,5):")
    y = ccall((:mean, "/Users/sindregrostad/git/sindre-msc-thesis.jl/calling_cpp/callingCppFromJulia/callingCppFromJulia/lib.so"),
    Float64,(Float64,Float64),2,5)
    println(y)
end

# Not working. The library Cxx that is supposed to make calling cpp libraries
# possible looks abandoned for the moment and it is not possible to install for
# Julia v.1.0.
function testCpp()
    pathToDir = convert(String, @__DIR__)
    pathToLib = pathToDir * "/callingCppFromJulia/callingCppFromJulia/lib.so"
    mylib = dlopen(pathToLib)
    x=ccall(dlsym(mylib, :cppFunction1), Float64,(Float64,Float64),2,5)
    println("function1():")
    println(x)
end

function main()
    simple_built_in()
    testC()
    #testCpp()
end

main()

#=
Summary
Managed to call c functions, but it has the following limitations:
- It does not support pure-header libraries
- The restrictions of ccall apply here; for example, there is no support for
    struct.
The Cxx library that is supposed to give support for C++ does not work, even
tough the latest commit says "1.0 Support."
=#
