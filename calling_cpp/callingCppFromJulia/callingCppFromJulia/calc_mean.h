//
//  calc_mean.h
//  callingCppFromJulia
//
//  Created by Sindre Grøstad on 14/01/2019.
//  Copyright © 2019 Sindre Grøstad. All rights reserved.
//

#ifndef calc_mean_h
#define calc_mean_h

#include <stdio.h>
double function1(double a, double b);
double mean(double a, double b);
double getNumber(void);
#endif /* calc_mean_h */
