//
//  helloWorld.hpp
//  callingCppFromJulia
//
//  Created by Sindre Grøstad on 14/01/2019.
//  Copyright © 2019 Sindre Grøstad. All rights reserved.
//

#ifndef helloWorld_h
#define helloWorld_h
#include <stdio.h>
char* helloWorld(void);

#endif /* helloWorld_hpp */
