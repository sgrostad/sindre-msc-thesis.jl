//
//  helloWorld.cpp
//  callingCppFromJulia
//
//  Created by Sindre Grøstad on 14/01/2019.
//  Copyright © 2019 Sindre Grøstad. All rights reserved.
//

#include "helloWorld.h"

char* helloWorld(){
    return "Hello, World!\n";
}
