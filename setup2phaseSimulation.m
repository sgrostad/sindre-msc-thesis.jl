%This function is a workaround such that all matlab code can be put in
%another folder and called via this function. Have not found a way to call
%matlab function in anotherfolder directly in mxcall().
function ret = setup2phaseSimulation(functionName,in)
    addpath("mrst-2018a/")
    ret = 0;
    switch (functionName)
        case "initializeMRST"
            startup();
            mrstModule add ad-core
        case "setup model geometry"
            ret = setupModel();
        case "plot solution"
            plotSolution(in)
    end
end

function ret = setupModel()
    %% Set up model geometry    
    isCompr = false;
    [nx,ny,nz] = deal(  11,  11,  3);
    [Dx,Dy,Dz] = deal(200, 200, 50);
    G = cartGrid([nx, ny, nz], [Dx, Dy, Dz]);
    G = computeGeometry(G);
    %% Define rock model
    rock = makeRock(G, 30*milli*darcy, 0.2);
    cr   = 1e-6/psia*double(isCompr);
    pR   = 200*barsa;
    pv_r = poreVolume(G, rock);
    pv   = @(p) pv_r .* exp(cr * (p - pR) );
    %% Fluid model
    % Water is slightly compressible with quadratic relative permeability
    muW    = 1*centi*poise;
    cw     = 2e-6/psia*double(isCompr);
    rhoWR  = 1014*kilogram/meter^3;
    rhoWS  = 1000*kilogram/meter^3;
    rhoW   = @(p) rhoWR .* exp( cw * (p - pR) );
    krW    = @(S) S.^2;

    % Oil phase is lighter and has a cubic relative permeability
    muO    = 5*centi*poise;
    co     = 1e-5/psia*double(isCompr);
    rhoOR  = 850*kilogram/meter^3;
    rhoOS  = 750*kilogram/meter^3;
    rhoO   = @(p) rhoOR .* exp( co * (p - pR) );
    krO    = @(S) S.^3;
    fluid = [muW, muO, rhoWR, rhoOR];
    %% Extract grid information
    nf = G.faces.num;                                 % Number of faces
    nc = G.cells.num;                                 % Number of cells
    N  = double(G.faces.neighbors);                   % Map: face -> cell
    F  = G.cells.faces(:,1);                          % Map: cell -> face
    intInx = all(N ~= 0, 2);                          % Interior faces
    N  = N(intInx, :);                                % Interior neighbors
    %% Compute transmissibilities
    hT = computeTrans(G, rock);                       % Half-transmissibilities
    T  = 1 ./ accumarray(F, 1 ./ hT, [nf, 1]);        % Harmonic average
    T  = T(intInx);                                   % Restricted to interior
    nf = size(N,1);
    %% Compute gradz
    D     = sparse([(1:nf)'; (1:nf)'], N, ones(nf,1)*[-1 1], nf, nc);
    grad  = @(x)  D*x;
    gradz = grad(G.cells.centroids(:,3));
    %% Impose vertical equilibrium
    gravity reset on,
    g     = norm(gravity);
    equil = ode23(@(z,p) g.* rhoO(p), [0, max(G.cells.centroids(:,3))], pR);
    p0    = reshape(deval(equil, G.cells.centroids(:,3)), [], 1);  clear equil
    sW0   = zeros(G.cells.num, 1);
    sW0   = reshape(sW0,G.cartDims); sW0(:,:,nz)=1; sW0 = sW0(:);
    %% Schedule and injection/production
    nstep = 36;
    Tf = 360*day;
    dt = Tf/nstep*ones(1,nstep);
    dt = [dt(1).*sort(repmat(2.^-[1:5 5],1,1)) dt(2:end)];
    nstep = numel(dt);

    [inRate,  inIx ] = deal(.1*sum(pv(p0))/Tf, nx*ny*nz-nx*ny+1);
    [outPres, outIx] = deal(100*barsa, nx*ny);
    well = [inRate,  inIx, outPres, outIx];
    %% set return variable
    ret = {G, gradz, rock, pv_r, fluid, N, T, p0, sW0, dt, Tf, well};
end

function plotSolution(in)
    sol = in{1};
    G = in{2};
    pargs = {};% {'EdgeColor','k','EdgeAlpha',.1};
    figure(1); clf
    subplot(2,1,1)
    plotCellData(G, sol.pressure/barsa, pargs{:});
    title('Pressure'), view(30, 40);
    colorbar
    
    subplot(2,1,2)
    plotCellData(G, sol.s, pargs{:});
    caxis([0, 1]), view(30, 40); title('Water saturation')
    colorbar
    drawnow
    pause(.1);
end