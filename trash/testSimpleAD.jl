using Pkg
#Pkg.add("Test")
push!(LOAD_PATH, ".")
using simpleAD, Test

@testset "testing simpleAD.jl" begin
    a = 14
    (x,y,z) = initVariablesAD(1,2,3)
    (x2,y2,z2) = initVariablesAD(1,2,3)
    @testset "equality" begin
        @test x == x2
        x3 = AD(1,[1 0 0])
        @test x == x3
    end
    @testset "multiplication" begin
        @test x*y == AD(2,[2 1 0])
        @test x*4 == AD(4,[4 0 0])
        @test 4*x == AD(4,[4 0 0])
    end
    @testset "addition" begin
        @test x+y == AD(3,[1 1 0])
        @test 2+y == AD(4,[0 1 0])
        @test y+2 == AD(4,[0 1 0])
    end
    @testset "subtraction" begin
        @test x-y == AD(-1,[1 -1 0])
        @test 2-y == AD(0,[0 1 0])
        @test y-2 == AD(0,[0 1 0])
    end
    @testset "division" begin
        @test x/z == AD(1/3,[1/3 0 -1/9])
    end
    @testset "power" begin
    end
    @testset "exp" begin
    end
    @testset "log" begin
    end
end
