using Profile
function func1(x)
    for i = 1:length(x)
        x[i] += 1
    end
end
function func2(x,i)
    x[i] = 0.0
end
function sumArray(x)
    sum = 0
    for i = 1:length(x)
        sum += x[i]
        func2(x,i)
    end
    return sum
end
function main()
    z = 100000
    x = rand(z)
    func1(x)
    sumArray(x)
end
Profile.init(n = 10^10, delay = 0.000000001)
@profiler main()
