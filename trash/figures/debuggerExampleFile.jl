function testDebugging(x)
    a = 3.0
    b = 4.0
    c = 5.0
    vector = [a, b, c]
    total = 0
    for i = 1:length(vector)
        total += vector[i] * x
    end
    return total
end
Juno.@enter testDebugging(1)
