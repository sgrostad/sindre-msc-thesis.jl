function res = benchmark_linear_solver()
    res = zeros(3,1);
    n = [1002, 8002, 27002];
    for i = 1:3
        if i == 1
            repetitions = 20;
        else 
            repetitions = 8;
        end
        tempres = zeros(repetitions,1);
        for j = 1:repetitions
            J = getJacobianflowSolver(i);
            b = rand(n(i),1);
            tic
            x = J\b;
            tempres(j) = toc;
        end
        res(i,1) = median(tempres);
    end
end

    