function ret = runMRSTBranchCode(functionName, in)
    ret = 0;
    addpath("/Users/sindregrostad/git/mrst/mrst-core")
    switch (functionName)
        case "initializeMRSTBranch"
            startup();
            mrstModule add ad-blackoil ad-core
        case "getTwoPhaseOilWaterModel"
            gridDisc = in*10;
            G = cartGrid([gridDisc, gridDisc, gridDisc], [1, 1, 1]);
            %G = cartGrid([2, 2, 2], [1, 1, 1]);
            G = computeGeometry(G);
            rock = makeRock(G, 1, 1);
            model = struct(TwoPhaseOilWaterModel(G, rock, struct()));
            rmOp = {'Grad', 'Div', 'AccDiv', 'faceAvg', 'faceUpstr', ...
                'splitFaceCellValue'};
            model.operators = rmfield(model.operators, rmOp);
            model = rmfield(model, 'AutoDiffBackend');
            ret = model;
        case "runTestMexDiagonalOperators"
            disc = 10*in(1);
            G = cartGrid([disc, disc, disc], [1, 1, 1]);
            G = computeGeometry(G);
            rock = makeRock(G, 1, 1);
            model = TwoPhaseOilWaterModel(G, rock, struct());
            block_size = in(2);
            ret = Copy_of_testMexDiagonalOperators(model, 'block_size', ...
                block_size);
        otherwise
            error("Function name, %s, is not defined!",functionName)
    end
end

