push!(LOAD_PATH, ".")
using simpleAD, Printf, LinearAlgebra

function newtonSolver(f,x0::AD)
    tol = 1e-8
    maxIts = 1000
    converged = false
    it = 0
    x = x0
    while (!converged && it < maxIts)
        it += 1
        upd = -f(x).val./f(x).jac
        x.val += upd[1]
        if norm(upd)<tol
            converged = true
        end
    end
    if !converged
        error("Newton solver did not converge in $it iterations.")
    end
    return x,it,converged
end

function main()
    f(x) = exp(2*x)-4x^2+13
    x0 = initVariablesAD(23)[1]
    x,it,converged = newtonSolver(f,x0)
    if converged
        @printf("Newton solver converged in %d iterations.\n",it)
    else
        @printf("Newton solver DID NOT converge in %d iterations.\n",it)
    end
    @printf("x = %.10f\n", x.val)
    @printf("f(x) = %.10f\n",f(x.val))
end

main()
