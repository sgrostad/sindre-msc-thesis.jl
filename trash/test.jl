#=using Distributed
println(Threads.nthreads())
println(Threads.threadid())
a = zeros(10)
Threads.@threads for i = 1:10
    a[i] = Threads.threadid()
end
println(a)
=#

using Test, LocalAD, MATLAB, LinearAlgebra, SparseArrays
using TwoPhaseSystems, BenchmarkTools, Distributed
#=
mxcall(:runMatlabCode, 1, "initializeMRST");
            ## Setup
cellArray = mxcall(:runMatlabCode, 1, "setup 2D model");
(G, N, T, gradz, pv, wells, dt, totTime, p0, s0) = cellArray;
fluid = Fluid();
numCells = convert(Int64, G["cells"]["num"]);
grid = Grid(T, pv, N, numCells);
well = Well(wells[1], wells[2], [wells[3]], [wells[4]], [wells[5]])

tps = TwoPhaseSystem(grid);
tps.pressure .= p0;
tps.waterSaturation .= s0;
#pressure:
p1 = copy(tps.pressureEqJac)
p1.nzval .= 1.0
TwoPhaseSystems.setAllJacobianValues!(tps, 1.0)
assemblePressureEquations!(tps, fluid, grid, well);
=#
n = 300
m1 = sprand(n,n,0.3)
for row = 1:n
    for col = 1:n
        m1[row,col] = 1
    end
end
m2 = copy(m1).*2
println("*************")
println("Should be false = $(m1 == m2)")
Threads.@threads for col = 1:n
    for row = 1:n
        m2[row,col] -= 1
    end
end
println("Should be true = $(m1 == m2)")
m2 *= 2
println("Should be false = $(m1 == m2)")
Threads.@threads for col = 1:n
    neighbourIndices =
        m2.colptr[col]:(m2.colptr[col + 1] - 1)
    for rowIx in neighbourIndices
        row = m2.rowval[rowIx]
        m2[row,col] -= 1
    end
end
println("Should be true = $(m1 == m2)")
@time begin
    for col = 1:n
        neighbourIndices =
            m2.colptr[col]:(m2.colptr[col + 1] - 1)
        for rowIx in neighbourIndices
            row = m2.rowval[rowIx]
            m2[row,col] -= 1
        end
    end
end
@time begin
    Threads.@threads for col = 1:n
        neighbourIndices =
            m2.colptr[col]:(m2.colptr[col + 1] - 1)
        for rowIx in neighbourIndices
            row = m2.rowval[rowIx]
            m2[row,col] -= 1
        end
    end
end
@time begin
    @distributed for col = 1:n
        neighbourIndices =
            m2.colptr[col]:(m2.colptr[col + 1] - 1)
        for rowIx in neighbourIndices
            row = m2.rowval[rowIx]
            m2[row,col] -= 1
        end
    end
end
@time begin
    @sync @distributed for col = 1:n
        neighbourIndices =
            m2.colptr[col]:(m2.colptr[col + 1] - 1)
        for rowIx in neighbourIndices
            row = m2.rowval[rowIx]
            m2[row,col] -= 1
        end
    end
end
