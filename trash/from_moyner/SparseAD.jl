module SparseAD
import Base: +, getindex, exp, .*, *, -
export AD
   type AD
      value::Vector{Float64}
      jacobian::SparseMatrixCSC{Float64, Int64}
   end
   function +(x::AD, y::AD)
     x.value += y.value;
     x.jacobian += y.jacobian;
     return x
   end
   function +(x::AD, y)
     x.value += y;
     return x
   end

   function -(x::AD, y::AD)
     x.value -= y.value;
     x.jacobian -= y.jacobian;
     return x
   end
   function -(x::AD, y)
     x.value -= y;
     return x
   end
   function .*(x::AD, y::AD)
     x.value = x.value.*y.value;
     x.jacobian = x.jacobian.*y.jacobian;
     return x
   end

   function *(x::AD, y)
     x.value = x.value*y;
     x.jacobian = x.jacobian*y;
     return x
   end

   function *(x, y::AD)
     y.value = x*y.value;
     y.jacobian = x*y.jacobian;
     return y
   end

   function exp(x::AD)
      eu = exp(x.value);
      J = spdiagm(eu)*x.jacobian;

     return AD(eu, J)
   end
   function getindex(x::AD, subs)
      if(size(subs, 1) == 1)
         subs = [subs];
      end
      return AD(x.value[subs], x.jacobian[subs, :])
   end

end #module
