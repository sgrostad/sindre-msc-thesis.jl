tic()
n_c = 100000;
G = cartGrid([n_c, 1]);
N = getNeighbourship(G);
nf = size(N,1);
nc = G.cells.num;
C  = sparse( [(1:nf)'; (1:nf)'], N, ones(nf,1)*[1 -1], nf, nc);
Grad = @(x) -C*x;
Div  = @(x) C'*x;

pD = ones(G.cells.num, 1);
p0 = pD;
dt = 1;
for i = 1:100
    p = initVariablesADI(pD);
    rho = exp(p*1e-10);
    rho0 = exp(p0*1e-10);

    v = - C*p;
    eq = (1/dt).*(rho - rho0) - C'*v;
    src = zeros(nc,1);
    src(1) = 1;
    src(end) = -1;
    
    eq = eq + src;
    dp = eq.jac{1}\eq.val;
    pD = pD + dp;
end
eq = eq + src;
toc()