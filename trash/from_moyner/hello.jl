include("./SparseAD.jl")
using SparseAD
#using ForwardDiff
type Grid
  numCells::Int
  left::Vector{Int}
  right::Vector{Int}
end


function initAD(values)
  n = size(values, 1)
  return AD(values, speye(n))
end

function cartGrid(dims)
  # N = zeros(prod(dims), 2)
  n = prod(dims)
  return Grid(n, 1:n-1, 2:n)
end

function numFaces(G::Grid)
  return size(G.left, 1)
end

function faceAverage(G::Grid, v)
  return 0.5*(v[G.left] + v[G.right])
end

function getMatrixC(G::Grid)
  n = numFaces(G);
  I = [1:n; 1:n];
  J = [G.left; G.right];
  V = [ones(n); -ones(n)];
  return sparse(I, J, V, n, G.numCells)
end

function residual(G, C, p, p0, dt)
  p = initAD(p);
  return residualDouble(G, C, p, p0, dt);
end

function residualDouble(G, C, p, p0, dt)
  nc = G.numCells;
  rho = exp(p*1e-10);
  rho0 = exp(p0*1e-10)

  v = - C*p;
  eq = (1/dt).*(rho - rho0) - C'*v;
  src = zeros(nc)
  src[1] = 1
  src[end] = -1
  #eq[1] = p[1];
  #eq[nc] = p[nc] - 1;
  eq = eq + src;
  return eq
end


function solvePressure(G)
  C = getMatrixC(G);
  p0 = ones(n_c);
  p = p0;
  dt = 1

  for i = 1:100
    r = residual(G, C, p, p0, dt)
    #display(maximum(abs(r.value)))
    #dp = -r.jacobian\r.value
    luA = lufact(-r.jacobian)
    dp = luA\r.value;
    p = p + dp
  end
  return p
end

n_c = 100000;
#n_c = 10;

G = cartGrid([n_c])
solvePressure(G)
#numFaces(g)
#d = ones(n_c);
#p = initAD(ones(n_c))
#p2 = initAD(rand(n_c))

#c = p + p2
#c[2]
#r = residual(G, C, p, p0, dt)
#faceAverage(g, d)
#faceAverage(g, p)
#C = getMatrixC(G);
#p0 = ones(n_c);
#dt = 1

#p0 = ones(G.numCells)
#f(x::Vector) = residualDouble(G, C, x, p0, dt);
