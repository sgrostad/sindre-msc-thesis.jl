using ForwardAutoDiff, CustomJacobianAD
using LinearAlgebra, Plots, Statistics, MATLAB
using BenchmarkTools
# mxcall(:runMRSTBranchCode, 1, "initializeMRSTBranch");

faceAverage1(cjad::CJAD, N) = 0.5 * (cjad[N[:,1]] + cjad[N[:,2]])


function benchmarkAssemblyAD()
    numPrimaryVariables = 5
    disc = 9
    juliaTime = zeros(2,disc)
    matlabTime = zeros(2,disc)
    for i = 1:disc
        println("i = $i")
        model = mxcall(:runMRSTBranchCode, 1, "getTwoPhaseOilWaterModel", i)
        N = convert(Array{Int64,2}, model["operators"]["N"])
        nFaces = size(N,1)
        nCells = convert(Int, model["G"]["cells"]["num"])
        diagonalJacs = rand(nCells, numPrimaryVariables)
        ##Create CJAD:
        customJacs = Vector{CustomJac}(undef, numPrimaryVariables)
        for j = 1:numPrimaryVariables
            customJacs[j] = DiagJac(diagonalJacs[:,j])
        end
        cjad = CJAD(rand(nCells), customJacs)
        #### Benchmark: ###########
        nanoseconds = 10^(-9)
        if faceAverage1(cjad, N) != faceAverage2(cjad, N)
            error("Different results!")
        end
        juliaTime[1,i] = median(@benchmark faceAverage1($cjad, $N)).time * nanoseconds
        juliaTime[2,i] = median(@benchmark faceAverage2($cjad, $N)).time * nanoseconds
        matlabTime[:,i] = mxcall(:runMRSTBranchCode, 1, "runTestMexDiagonalOperators",
            mxarray([i, convert(Float64, numPrimaryVariables)]))
    end
    println("Times from Julia method 1:")
    println(juliaTime[1,:])
    println("Times from Julia method 2:")
    println(juliaTime[2,:])
    println("Times from Matlab:")
    println(matlabTime)
end

benchmarkAssemblyAD()
