export faceAverage2
function faceAverage2(A, N::AbstractArray)
    #newVal = vec(0.5 .* sum(A.val[N],dims = 2))
    newVal = Vector{Float64}(undef,size(N,1))
    for i = 1:size(N,1)
        newVal[i] = 0.5 * (A.val[N[i,1]] + A.val[N[i,2]])
    end
    numJacs = length(A.customJacs)
    newJacs = Vector{CustomJac}(undef, numJacs)
    for i = 1:length(A.customJacs)
        newJacs[i] = faceAverage(A.customJacs[i], N)
    end
    return CJAD(newVal, newJacs)
end
