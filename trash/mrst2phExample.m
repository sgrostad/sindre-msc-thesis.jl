mrstModule add incomp
mrstModule add mimetic incomp deckformat
mrstVerbose false
gravity off
[nx, ny, nz] = deal( 10, 10, 1);
[Dx, Dy, Dz] = deal(10, 10, 1);
G = cartGrid([nx, ny, nz], [Dx, Dy, Dz]);
G         = computeGeometry(G);
rock.perm = repmat(100*milli*darcy, [G.cells.num, 1]);
rock.poro = repmat(0.3            , [G.cells.num, 1]);

%x = linspace(0, 1, 100000001) .';
%y = linspace(1, 0, 100000001) .';
x = linspace(0, 1, 1001) .';
y = linspace(1, 0, 1001) .';
%%
pc_form = 'nonwetting';
cap_scale = 10;
kr = tabulatedSatFunc([x, x.^2, y.^2]);

props = constantProperties([   1,  10] .* centi*poise, ...
                           [1000, 700] .* kilogram/meter^3);
fluid = struct('properties', props                  , ...
               'saturation', @(x, varargin)    x.s  , ...
               'relperm'   , kr);
%%
rate = 0.5*meter^3/day;
bhp  = 1*barsa;

W = verticalWell([], G, rock, 1, 1, 1:nz,          ...
                 'Type', 'rate', 'Val', rate, ...
                 'Radius', .1, 'Name', 'I', 'Comp_i', [1 0]);
W = verticalWell(W, G, rock, nx, ny, 1:nz,     ...
                 'Type','bhp', 'Val', bhp, ...
                 'Radius', .1, 'Dir', 'x', 'Name', 'P', 'Comp_i', [0 1]);
             rSol    = initState(G, W, 0, [0.2, 0.8]);
%%

gravity off
verbose = false;

S  = computeMimeticIP(G, rock, 'Verbose', verbose,'InnerProduct','ip_tpf');
%%
psolve  = @(state, fluid) incompMimetic(state, G, S, fluid, 'wells', W);
tsolve  = @(state, dT, fluid) implicitTransport(state, G, dT, rock, ...
                                                fluid, 'wells', W, ...
                                                'verbose', verbose);
%%
rSol    = psolve(rSol, fluid);
%%
T      = 360*day();
dT     = T/15;
dTplot = 100*day();  % plot only every 100th day
N      = fix(T/dTplot);
pv     = poreVolume(G,rock);
%%
t  = 0; plotNo = 1;
h1 = 'No pc - '; H2 = 'Linear pc - ';
e = []; p_org = []; p_pc = [];
%figure;
%dT = 27000.0;
while t < T,
   % TRANSPORT SOLVE
   rSol    = tsolve(rSol, dT, fluid);

   % Check for inconsistent saturations
   s = [rSol.s(:,1)];
   assert(max(s) < 1+eps && min(s) > -eps);

   % Update solution of pressure equation.
   rSol    = psolve(rSol,    fluid);

   % Measure water saturation in production cells in saturation

   p_org = [p_org; rSol.s(W(2).cells,1)' ];                 %#ok
   % Increase time and continue if we do not want to plot saturations
   t = t + dT;
   heading = [num2str(convertTo(t,day)),  ' days'];
   plotCellData(G, rSol.s(:,1));
   caxis([0 1]), view(60,50), axis equal off, title(heading)
   drawnow
   colorbar
    %{
   if ( t < plotNo*dTplot && t<T) continue, end

   % Plot saturation
   heading = [num2str(convertTo(t,day)),  ' days'];
   r = 0.01;
   subplot('position',[(plotNo-1)/N+r, 0.50, 1/N-2*r, 0.48]), cla
   plotCellData(G, rSol.s(:,1));
   caxis([0 1]), view(60,50), axis equal off, title([h1 heading])

   plotNo = plotNo+1;
    %}
end