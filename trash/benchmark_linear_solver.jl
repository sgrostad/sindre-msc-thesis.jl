using SparseArrays, BenchmarkTools, Statistics, MATLAB

function testLinearSolver(J,b)
    x = J\b
end

function benchmark_linear_solver()
    nanoseconds = 10^(-9)
    k = 3
    res = zeros(k,2)
    n = [1002, 8002, 27002]
    res[:,2] = mxcall(:benchmark_linear_solver, 1)
    for i = 1:k
        J = mxcall(:getJacobianflowSolver, 1, i)
        b = rand(Int(n[i]),1)
        res[i,1] = median(@benchmark testLinearSolver($J,$b)).time*nanoseconds
    end
    p = plot(n, res, label=["Julia","MATLAB"], legend=:topleft,
    shape = [:circle])
    xaxis!("n",:log10)
    yaxis!("Time [s]",:log10)
    return p
end

benchmark_linear_solver()
