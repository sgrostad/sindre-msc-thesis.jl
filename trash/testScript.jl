push!(LOAD_PATH, pwd() * "/master_thesis/")
using CustomJacAD

x_init = collect(1:5)
y_init = 6.0
z_init = collect(7:11)
(x, y, z) = initialize_CJAD(x_init, y_init, z_init);
#println(x+y)
function test(vec::Vector{<:Number})
    println(typeof(vec))
end
vector = Vector{SparseJac}(undef,3)
vector[1] = NullJac(2,2)
vector[2] = IdentityJac(5)
vector[3] = DiagJac([1,3,4])
println(vector)
#test([1,2,3,4])
