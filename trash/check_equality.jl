using CustomJacobianAD, ForwardAutoDiff, MATLAB, LinearAlgebra, Printf
using BenchmarkTools, Statistics, SetupFlowSolver, FlowSystems, WellParameters
# Line below needs to run each time you begin running matlab scripts/functions
mxcall(:runMatlabCode, 1, "initializeMRST");

function checkEqualityFADandCJAD()
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
    setup_flow_solver(1)
    (p_cjad, bhp_cjad, qS_cjad)= initialize_CJAD(p_init, p_init[Int(wc[1])], 0)
    (p_ad, bhp_ad, qS_ad)= initialize_AD(p_init, p_init[Int(wc[1])], 0)
    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        #Newton loop
        converged = false
        p0 = p_cjad.val
        nit = 0
        while !converged && (nit < maxIts)
            eq1 = presEq(p_ad, p0, dt)
            eq1[wc] = eq1[wc] - q_conn(p_ad, bhp_ad)

            eq = cat(eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))

            eq2 = presEq(p_cjad, p0, dt)
            eq2[wc] -= q_conn(p_cjad, bhp_cjad)

            eq3 = cat(eq2, rateEq(p_cjad, bhp_cjad, qS_cjad), ctrlEq(bhp_cjad))

            if !(eq3.customJacs[1].jac ≈ eq.jac)
                error("Jacobians are different!")
            elseif !(isapprox(eq3.val, eq.val, atol = 1e-13))
                error("Values are different!")
            elseif nnz(eq3.customJacs[1].jac) != nnz(eq.jac)
                error("Jacobians have different structure")
            end

            upd = -(eq.jac\eq.val)
            #Update variables:
            p_ad += upd[pIx]
            bhp_ad += upd[bhpIx]
            qS_ad += upd[qSIx]

            upd = -(eq3.customJacs[1].jac\eq3.val)

            p_cjad += upd[pIx]
            bhp_cjad += upd[bhpIx]
            qS_cjad += upd[qSIx]

            residual = norm(eq.val)
            converged = (residual<tol)
            nit       = nit + 1;

        end
    end
end

function checkEqualityCJADandLocalAD()
    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
    setup_flow_solver(1)
    (p_cjad, bhp_cjad, qS_cjad)= initialize_CJAD(p_init, p_init[Int(wc[1])], 0)
    well = Well(WI,wc,dz)
    fs = FlowSystem(p_init, p_init[wc[1]], 0.0, N, T, gradz, well, pv_r[1])
    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        #println("stepNumber: $stepNumber")
        #Newton loop
        converged = false
        p = p_cjad.val
        p0 = p
        nit = 0
        newtonLoopNumber = 0
        while !converged && (nit < maxIts)
            newtonLoopNumber += 1
            #println("newtonLoopNumber: $newtonLoopNumber")
            eq1 = presEq(p_cjad, p0, dt)
            eq1[wc] -= q_conn(p_cjad, bhp_cjad)
            eq = cat(eq1, rateEq(p_cjad, bhp_cjad, qS_cjad), ctrlEq(bhp_cjad))
            assembleFlowSystem!(fs, p0, dt, well)
            LADjac = getNormalJac(fs)
            if !(eq.customJacs[1].jac ≈ LADjac)
                error("Jacobians are different!")
            elseif !(isapprox(eq.val, fs.eqVal, atol = 1e-13))
                error("Values are different!")
            #elseif nnz(eq.customJacs[1].jac) != nnz(LADjac)
            #    error("Jacobians have different structure")
            end

            upd = -(eq.customJacs[1].jac\eq.val)
            #Update variables:
            p_cjad += upd[pIx]
            bhp_cjad += upd[bhpIx]
            qS_cjad += upd[qSIx]

            upd2 = -(LADjac\fs.eqVal)
            #Update variables:
            fs.varVals .+= upd2

            residual = norm(fs.eqVal)
            println(residual)
            converged = (residual<tol)
            nit       = nit + 1;

        end
    end
    println("CJAD and LocalAD produce the same result!")
end
checkEqualityCJADandLocalAD()
#checkEqualityFADandCJAD()
