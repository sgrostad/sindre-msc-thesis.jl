function res = benchmark_matlab_multiplication(lengthVectors)
    res = zeros(length(lengthVectors),2);
    res(:,1) = do_benchmark(lengthVectors,true);
    res(:,2) = do_benchmark(lengthVectors,false);
end

function res = do_benchmark(lengthVectors, vector)
    
    res = zeros(length(lengthVectors),1);
    for i = 1:length(lengthVectors)
        x = rand(lengthVectors(i),1);
        y = rand(lengthVectors(i),1);
        z = rand(lengthVectors(i),1);
        if i<12
            repetitions = 400;
        else
            repetitions = 5;
        end
        tempRes = zeros(repetitions,1);
        for j = 1:repetitions
            x = rand(lengthVectors(i),1);
            y = rand(lengthVectors(i),1);
            z = rand(lengthVectors(i),1);
            tic
            compute_function(x,y,z, vector)
            tempRes(j) = toc;
        end
        res(i) = median(tempRes);
    end
end

function compute_function(x, y, z, vector)
    f = @(x,y,z) exp(2*x.*y) - 4*x.*z.^2 + 13*x - 7;
    fx = @(x,y,z) 2*y.*exp(2*x.*y) - 4*z.^2 + 13;
    fy = @(x,y,z) 2*x.*exp(2*x.*y);
    fz = @(x,y,z) -8*x.*z;
    if vector
        fans = f(x,y,z);
        fxans = fx(x,y,z);
        fyans = fy(x,y,z);
        fzans = fz(x,y,z);
    else
        n = length(x);
        fans = zeros(n,1);
        fxans = zeros(n,1);
        fyans = zeros(n,1);
        fzans = zeros(n,1);
        for i = 1:n
            fans(i) = f(x(i),y(i),z(i));
            fxans(i) = fx(x(i),y(i),z(i));
            fyans(i) = fy(x(i),y(i),z(i));
            fzans(i) = fz(x(i),y(i),z(i));
        end
    end
end
