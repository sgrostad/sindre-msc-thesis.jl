using TwoPhaseSystems, MATLAB, Printf, SetupFlowSolver, LinearAlgebra
mxcall(:runMatlabCode, 1, "initializeMRST");

struct Solution
    time
    pressure
    s
end
function initializeTwoPhase3DSimulation()
    cellArray = mxcall(:setup2phaseSimulation, 1, "setup model geometry")
    (G, gradz, rock, pv, fluidVar, N, T, p0, waterSat0, dt, totTime, wells) = cellArray
    fluid = Fluid(fluidVar[1], fluidVar[2], fluidVar[3], fluidVar[4])
    numCells = convert(Int64, G["cells"]["num"])
    grid = Grid(T, gradz, pv, N, numCells)
    well = Well(wells[1], wells[2], wells[3], wells[4])
    return (G, grid, gradz, rock, pv, fluid, N, T, p0, waterSat0, dt, totTime, well)
end
function twoPhase3D()
    (G, grid, gradz, rock, pv, fluid, N, T, p0, waterSat0, dt, totTime, well) =
        initializeTwoPhase3DSimulation()
    tps = TwoPhaseSystem(grid)
    initializePressureAndSat(tps, p0, waterSat0)

    #initialize solution loop
    nSteps = length(dt)
    tol = 1e-5
    maxIts = 15
    secondsInDay = 86400.0
    sol = Vector{Solution}(undef, nSteps + 1)
    sol[1] = Solution(0, tps.pressure, tps.waterSaturation)
    t = 0;
    for stepNumber = 1:nSteps
        t += dt[stepNumber]
        @printf("\nTime step %d: Time %.2f -> %.2f days\n", stepNumber,
        (t-dt[stepNumber])/secondsInDay, t/secondsInDay)
        converged = false
        nit = 0
        println("Pressure equation:")
        while !converged && (nit < maxIts)
            assemblePressureEquations!(tps, fluid, grid)
            upd = -(tps.pressureEqJac\tps.pressureEq)
            tps.pressure .+= upd
            residual = norm(tps.pressureEq)
            converged = (residual<tol)
            nit       = nit + 1;
            @printf("\tIteration %3d:  Res = %.4e\n", nit, residual)
        end
        if !converged
            error("Newton solver did not converge for pressure equation")
        end
        nit = 0
        converged = false
        prevWaterSaturation = copy(tps.waterSaturation)
        println("Transport equation:")
        while !converged && (nit < maxIts)
            assembleTransportEquations!(tps, fluid, grid,
            prevWaterSaturation, dt[stepNumber])
            if maximum(tps.waterSaturation) > 1
                error("waterSaturation above 1!")
            end
            upd = -(tps.transportEqJac\tps.transportEq)
            tps.waterSaturation .+= upd
            #TODO begrense saturation til å maks 1: (Nok?)
            tps.waterSaturation .= max.(min.(tps.waterSaturation,1),0)
            residual = norm(tps.transportEq)
            converged = (residual<tol)
            nit       = nit + 1;
            @printf("\tIteration %3d:  Res = %.4e\n", nit, residual)
        end
        if !converged
            error("Newton solver did not converge for transport equation")
        else
            sol[stepNumber + 1] = Solution(t, tps.pressure, tps.waterSaturation)
            mxcall(:setup2phaseSimulation, 1, "plot solution",
                        mxarray([mxstruct(sol[stepNumber + 1]) mxarray(G)]));
        end
    end
    return sol
end

sol = twoPhase3D()
#=(G, gradz, rock, pv, fluidVar, N, T, p0, waterSat0, dt, totTime, well) =
initializeTwoPhase3DSimulation()
=#
