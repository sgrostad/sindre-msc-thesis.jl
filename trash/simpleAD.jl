module simpleAD
export AD, initVariablesAD
mutable struct AD
    val # Specify type?
    jac # Specify type?
end

### Overload +:
import Base: +
 +(A::AD, B::AD) = AD(A.val + B.val, A.jac + B.jac)
+(A::AD, B::Number) = AD(A.val + B, A.jac)
+(A::Number, B::AD) = B+A

### Overload -:
import Base: -
-(A::AD, B::AD) = AD(A.val - B.val, A.jac - B.jac)
-(A::AD, B::Number) = AD(A.val - B, A.jac)
-(A::Number, B::AD) = B-A

### Overload *:
import Base: *
*(A::AD, B::AD) = AD(A.val * B.val, A.jac * B.val + A.val * B.jac)
*(A::AD, B::Number) = AD(A.val * B, A.jac * B)
*(A::Number, B::AD) = B * A

### Overload /:
import Base: /
/(A::AD, B::AD) = AD(A.val / B.val, (A.jac * B.val - A.val * B.jac) / B.val^2)
/(A::AD, B::Number) = AD(A.val / B, A.jac / B)
/(A::Number, B::AD) = B / A

###Overload ^:
function Base.:^(A::AD, k::Number)
    if A.val <= 0 && k<=1
        error("Have not implemented for complex numbers yet")
    end
    jac = zeros(1,length(A.jac))
    for j in 1:length(A.jac)
        jac[j] = k * A.val^(k-1)*A.jac[j]
    end
    AD(A.val^k, jac)
end

### Overload exp():
function Base.:exp(A::AD)
    jac = zeros(1,length(A.jac))
    for j in 1:length(A.jac)
        if A.jac[j] != 0
            jac[j] = A.jac[j] * exp(A.val)
        end
    end
    AD(exp(A.val), jac)
end


### Overload log():
function Base.:log(A::AD)
    if A.val == 0
        error("Can not take log of 0")
    end
    jac = zeros(1,length(A.jac))
    for j in 1:length(A.jac)
        if A.jac[j] != 0
            jac[j] = A.jac[j] / A.val
        end
    end
    AD(log(A.val), jac)
end

### Overload ==:
function Base.:(==)(A::AD, B::AD)
    if A.val ≈ B.val && A.jac ≈ B.jac
        return true
    end
    return false
end

function initVariablesAD(x1,xn...)
    # Don't have a way to check number of output parameters yet.
    variables = [x1,xn...]
    n = length(variables)
    output = Array{AD,2}(undef,1,n)
    for i in 1:n
        jac = zeros(1,n)
        jac[i] = 1
        output[i] = AD(variables[i],jac)
    end
    return output
end

end
