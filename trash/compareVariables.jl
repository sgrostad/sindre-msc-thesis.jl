push!(LOAD_PATH, ".")
using ForwardAutoDiff, MATLAB
#mxcall(:runMatlabCode, 1, "initializeMRST");

function getOneADVariable()
    barsa = 1e5
    ## Set up model geometry
    (nx, ny, nz) = (10.0, 10.0, 10.0)
    (Dx, Dy, Dz) = (200.0, 200.0, 50.0)
    geometryInput = mxarray([nx ny nz Dx Dy Dz])
    G = mxcall(:runMatlabCode, 1, "compute geometry", geometryInput)

    ## Define compressible rock model
    rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
    cr = 1e-11
    p_r = 200e5
    pv_r = rock["poro"].*G["cells"]["volumes"]
    #pv(p,i) = pv_r[i]* exp(cr * (p-p_r)) ## This was difficult in Julia given I have understood correctly
    #pv(p) = [pv(p[i],i) for i in range(1,length(p))]
    pv(p) = pv_r .* exp.(cr * (p.-p_r))
    pv_ad(p) = pv_r * exp(cr * (p-p_r))
    #pvPlot(p) = pv_r[1] .* exp(cr * (p-p_r)) # Not sure why plot uses different
    p = collect(range(100e5,length = 50, stop = 220e5))
    #plot(p, map(pvPlot,p))

    ##Define model for compressible fluid
    mu = 0.0050
    c = 1e-8
    rho_r = 850.0
    rhoS = 750.0
    rho(p) = rho_r * exp(c * (p-p_r))

    ##Add well to geometry
    nperf = 8.0
    i = mxarray(ones(8,1)*2.0)
    j = mxarray(collect(2:9)*1.0)
    k = mxarray(ones(8,1)*5.0)

    W = mxcall(:runMatlabCode, 1, "add well",
                mxarray([mxarray(G) mxarray(rock) i j k ]))

    ##Derive initial pressure
    g = 9.806649999999999
    p_init = mxcall(:runMatlabCode, 1, "derive initial pressure",
                    mxarray([mxarray(G) p_r rho_r c]))


    ## compute transmissibilities on interior connections
    Ntemp = G["faces"]["neighbors"]
    intInx = [Int(n != 0) for n in Ntemp]
    intInx = convert(Array{Bool,1},intInx[:,1].* intInx[:,2])
    # TODO need to be a more elegant way to create N...
    N = zeros(2700,2)
    N[:,1] = [Ntemp[i,1] for i in 1:size(Ntemp,1) if intInx[i]]
    N[:,2] = [Ntemp[i,2] for i in 1:size(Ntemp,1) if intInx[i]]
    hT = mxcall(:runMatlabCode, 1, "computeTrans",
                mxarray([mxarray(G) mxarray(rock)]))
    cf = G["cells"]["faces"][:,1]
    nf = G["faces"]["num"]
    T = mxcall(:runMatlabCode, 1, "Harmonic average in interior",
            mxarray([mxarray(cf) mxarray(hT) nf mxarray(intInx)]))

    ## Define discrete operators
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)])) #TODO make this matrix in Julia
    N = convert(Array{Int64,2},N)
    gradient(x) = C * x
    divergence(x) = -C' * x
    #average(x) = 0.5 * ([x[n] for n in N[:,1]] + [x[n] for n in N[:,2]])
    average(x) = 0.5 * (x[N[:,1]] + x[N[:,2]])

    ## Define flow equations
    gradz = gradient(G["cells"]["centroids"][:,3])
    flux(p) = -(T / mu) * (gradient(p) - g*average(rho(p))*gradz)
    np = length(p_init)
    presEq(p, p0, dt) = (1/dt) * (pv_ad(p)*rho(p) - pv(p0).*rho.(p0)) +
                        divergence(average(rho(p)) * flux(p))
                        #not 100% equal answer as matlab.(Machine error?)
    ## Define well equations
    wc = convert(Array{Int64,1},W["cells"])
    WI = W["WI"]
    dz = W["dZ"]

    p_conn(bhp) = bhp + g*dz*rho(bhp)
    q_conn(p,bhp) = WI * (rho(p[wc])/mu) * (p_conn(bhp) - p[wc])
    #q_conn(in) =  WI.* (rho.(in[wc])./mu).* (p_conn(in[end]) - in[wc])

    rateEQ(p,bhp,qS) = qS - sum(q_conn(p,bhp))/rhoS
    #rateEQ(in) = in[end] - sum(q_conn(in[1:end-1]))/rhoS
    ctrlEq(bhp) = bhp - 100*barsa

    ## Initialize loop
    #AD-Variables...
    (p_ad, bhp_ad, qS_ad)= initialize_AD(p_init, p_init[Int(wc[1])], 0)

    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)
    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10
    p0 = p_ad.val
    eq1 = presEq(p_ad, p0, dt)
    eq1[wc] = eq1[wc] - q_conn(p_ad, bhp_ad)
    eq = cat(eq1, rateEQ(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))
    return eq
end

ad = getOneADVariable()
mrstArray = mxcall(:runMatlabCode,1,"compareVariables")
mrstAD = AD(mrstArray[1], mrstArray[2])
println(ad == mrstAD)
