using MATLAB, BenchmarkTools
using SetupFlowSolver, ForwardAutoDiff, CustomJacobianAD, Statistics, LocalAD
using PresEqSystems
mxcall(:runMatlabCode, 1, "initializeMRST");

function presEqForwardAutoDiff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G, rep)
    p_ad = initialize_AD(p_init)
    dt = 0.5
    for i = 1:rep
        eq1 = presEq(p_ad, p_init, dt)
    end
end

function presEqCustomJacobianAD(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G, rep)
    p_ad = initialize_CJAD(p_init)
    dt = 0.5;
    for i = 1:rep
        eq1 = presEq(p_ad, p_init, dt)
    end
end

function presEqLocalAD(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G, rep)
    pes = PresEqSystem(p_init, N, T, gradz, pv_r[1])
    dt = 0.5;
    for i = 1:rep
        assemblePresEqSystem!(pes, p_init, p_init, dt)
    end
end


function benchmarkPresEqAssembly()
    rep = 100
    k = 3
    nanoseconds = 10^(-9)
    recordedTimes = zeros(5,k)
    for disc = 1:k
        println("disc = $disc")
        ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
        presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
        hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G = setup_flow_solver(disc)

        println("run CJAD:")
        recordedTimes[1,disc] = median(@benchmark presEqCustomJacobianAD($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)).time * nanoseconds
        println("run ForwardAutoDiff:")
        recordedTimes[2,disc] = median(@benchmark presEqForwardAutoDiff($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)).time * nanoseconds
        println("run LocalAD")
        recordedTimes[3,disc] = median(@benchmark presEqLocalAD($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)).time * nanoseconds
        println("run MRST:")
        recordedTimes[4:5,disc] = mxcall(:benchmarkPresEqAssemblyMRST, 1, disc, rep)
    end
    println("CJAD:\n$(recordedTimes[1,:])")
    println("ForwardAutoDiff:\n$(recordedTimes[2,:])")
    println("LocalAD:\n$(recordedTimes[3,:])")
    println("MRST: \n$(recordedTimes[4,:])")
    println("MRST_diagonal:\n$(recordedTimes[5,:])")
end
function testLocalAD()
    rep = 100
    k = 1
    nanoseconds = 10^(-9)
    recordedTimes = zeros(2,k)
    if k != 1
        for disc = 1:k
            println("disc = $disc")
            ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
            presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
            hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G = setup_flow_solver(disc)
            println("run CJAD:")
            recordedTimes[1,disc] = median(@benchmark presEqCustomJacobianAD($ctrlEq, $rateEq, $q_conn,
            $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
            $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
            $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)).time * nanoseconds
            println("run LocalAD")
            recordedTimes[2,disc] = median(@benchmark presEqLocalAD($ctrlEq, $rateEq, $q_conn,
            $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
            $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
            $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)).time * nanoseconds
        end
        println("CJAD:\n$(recordedTimes[1,:])")
        println("LocalAD:\n$(recordedTimes[2,:])")
    else
        disc = k
        println("disc = $disc")
        ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
        presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
        hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G = setup_flow_solver(disc)
        println("run CJAD:")
        @btime presEqCustomJacobianAD($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)
        println("run LocalAD")
        @btime presEqLocalAD($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G, $rep)
        #=presEqLocalAD(ctrlEq, rateEq, q_conn,
        p_conn, dz, WI, wc, presEq, np, flux, gradz, average, divergence,
        gradient, N, C, T, nf, cf, hT, p_init, g, rho, rhoS, rho_r, c,
        mu, pv_ad, pv, pv_r, p_r, cr, G, rep)=#

    end
end

testLocalAD()
#benchmarkPresEqAssembly()
