module PresEqSystems
using StaticArrays, SparseArrays, LocalAD
export PresEqSystem, assemblePresEqSystem!
export setAllPresEqJacobianValues!, assemblePresEqSystem!
import Base: size, length, ==, copy
struct PresEqSystem
    val::Vector{Float64}
    globalJac::SparseMatrixCSC{MVector{NUM_DERIV,Float64}, Int}
    T::SparseMatrixCSC{Float64, Int} #TODO check if this is the optimal solution
    gradz::SparseMatrixCSC{Float64, Int}#TODO check if this is the optimal solution
    pv_r::Float64 #Needs to be a vector if for example cells have different sizes
end

PresEqSystem(val::Vector{Float64}, N::Array{Int,2}) =
    PresEqSystem(val, N, ones(size(N,1)), ones(size(N,1)), 1.0)

function PresEqSystem(val::Vector{Float64}, N::Array{Int,2}, T::Vector{Float64},
        gradz::Vector{Float64}, pv_r::Float64)
    numNeighbours, col = size(N)
    if col != 2
        error("Neighbourlist must be on the form n×2")
    elseif size(N,1) != length(T)
        error("Must have equal number of transimissibilities as facets")
    end
    rows = [N[:,1];N[:,2]]
    cols = [N[:,2];N[:,1]]
    newT = sparse(rows, cols, [T; T])
    newGradz = sparse(rows, cols, [gradz; -gradz])
    diag = collect(1:length(val))
    rows = [rows; diag]
    cols = [cols; diag]
    elementsInJac = length(cols)
    nzval = [@MVector zeros(NUM_DERIV) for i = 1:elementsInJac]
    return PresEqSystem(copy(val), sparse(rows, cols, nzval), newT, newGradz, pv_r)
end

function setAllPresEqJacobianValues!(pes::PresEqSystem, newVal::Float64)
    for i = 1:length(pes.globalJac.nzval)
        for j = 1:NUM_DERIV
            pes.globalJac.nzval[i][j] = newVal
        end
    end
end

function resetPresEqSystem!(pes::PresEqSystem)
    for i = 1:length(pes)
        pes.val[i] = 0.0
    end
    setAllPresEqJacobianValues!(pes, 0.0)
end

function size(pes::PresEqSystem, dim::Int)
    if dim == 1
        return length(pes.val)
    elseif dim == 2
        return size(pes.globalJac,2)
    else
        error("PresEqSystem only has two dimensions. (\"dim\" = 1 or 2)")
    end
end
size(pes::PresEqSystem) = (size(pes, 1), size(pes, 2))
length(pes::PresEqSystem) = size(pes, 1)
function (==)(A::PresEqSystem, B::PresEqSystem)
    if A.val == B.val && A.globalJac == B.globalJac
        return true
    end
    return false
end
function copy(pes::PresEqSystem)
    newVal = copy(pes.val)
    newGlobalJac = copy(pes.globalJac)
    return PresEqSystem(newVal, newGlobalJac)
end

function assemblePresEqSystem!(pes::PresEqSystem, pressure::Vector{Float64},
        prevPressure::Vector{Float64}, dt::Float64)
    resetPresEqSystem!(pes)
    for from = 1:length(pes.val)
        neighbourIndices = pes.globalJac.colptr[from]:(pes.globalJac.colptr[from + 1] - 1)
        for toIx in neighbourIndices
            to = pes.globalJac.rowval[toIx]
            if from == to
                p = pressure[from]
                p0 = prevPressure[from]
                timeDerivLAD = timeDerivative(p, p0, dt, pes.pv_r)
                pes.val[from] += timeDerivLAD.val
                pes.globalJac[from,from] .+= timeDerivLAD.derivatives
                continue
            end
            T = pes.T[from, to]
            gradz = pes.gradz[from, to]
            fluxLAD = flux(from, to, pressure, T, gradz)
            pes.val[from] += fluxLAD.val
            pes.globalJac[to, from] .-= fluxLAD.derivatives
            pes.globalJac[from, from] .+= fluxLAD.derivatives
        end
    end
end

function rho(p)
    rho_r = 850
    c = 1e-8
    p_r = 20000000
    return rho_r * exp(c*(p-p_r))
end
function pv(p, pv_r)
    cr = 9.999999999999999e-12
    p_r = 20000000
    return pv_r * exp(cr * (p - p_r))
end
function timeDerivative(p::Float64, p0::Float64, dt::Float64, pv_r::Float64)
    pCell = createVariable(p,1)
    return (1/dt) * (pv(pCell, pv_r)*rho(pCell) - pv(p0, pv_r) * rho(p0))
end

## gradient from cell "from" to cell "to", derivatives wrt cell "from".
function grad(from::Int, to::Int, pressure::Vector{Float64})
    pFrom = createVariable(pressure[from], 1)
    pTo = createVariable(pressure[to], 0)
    return (pTo - pFrom)
end
grad(pFrom::LAD, pTo::LAD) = (pTo - pFrom)


## flux from cell "from" to cell "to", derivatives wrt cell "from".
function flux(from::Int, to::Int, pressure::Vector{Float64}, T::Float64,
        gradz::Float64)
    pFrom = createVariable(pressure[from], 1)
    pTo = createVariable(pressure[to], 0)
    rhoAverage = avg(rho(pFrom), rho(pTo))
    mu = 0.005
    g = 9.806649999999999
    viscousFlux = -T/mu * (grad(pFrom, pTo))
    gravityFlux = T/mu * g * rhoAverage * gradz
    return avg(rho(pFrom), rho(pTo)) * (viscousFlux + gravityFlux)
end
## faceAverage between cell "from" and "to", derivatives wrt cell "from"
function avg(from::Int, to::Int, pressure::Vector{Float64})
    pFrom = createVariable(pressure[from], 1)
    pTo = createVariable(pressure[to], 0)
    return 0.5 * (pTo + pFrom)
end
avg(pFrom::LAD, pTo::LAD) = 0.5 * (pFrom + pTo)
end  # module PresEqSystems
