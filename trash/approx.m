function ret = approx(A,B,tol)
ret = true;
for row = 1:size(A,1)
    for col = 1:size(A,2)
        if abs(A(row,col) - B(row,col)) > tol
            ret = false;
            fprintf('(%d,%d)= %.9f\n',...
                row,col,full(A(row,col) - B(row,col)));
        elseif abs(A(row,col)) + abs(B(row,col)) > tol
            fprintf('(%d,%d) is correct!!\n',...
                row,col);
        end
    end
end
end