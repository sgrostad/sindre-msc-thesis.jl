%This function is a workaround such that all matlab code can be put in
%another folder and called via this function. Have not found a way to call
%matlab function in anotherfolder directly in mxcall().
function ret = runMatlabCode(functionName,in)
    addpath("mrst-2018a/")
    addpath("master_thesis/flow_solver/matlab")
    addpath("master_thesis/benchmarks/matlab")
    addpath("master_thesis/2phase_solver/matlab")
    ret = 0;
    switch (functionName)
        case "initializeMRST"
            startup();
            mrstModule add ad-core
            mrstModule add incomp
        case "testMatlabFromJulia"
            ret = testMatlabFromJulia(in);
        case "compute geometry"
            G = cartGrid(in(1:3), in(4:6));
            ret = computeGeometry(G);
        case "create rock"
            ret = makeRock(in, 30*milli*darcy, 0.3);
        case "add well"
            G = in{1};
            rock = in{2};
            I = in{3};
            J = in{4};
            K = in{5};
            cellInx = sub2ind(G.cartDims, I, J, K);
            ret = addWell([], G, rock, cellInx, 'Name', 'P1', 'Dir', 'y' );
            % Matlab in julia package does not support char array
            ret = char2intInStruct(ret);
        case "derive initial pressure"
            G = in{1};
            p_r = in{2};
            rho_r = in{3};
            c = in{4};
            rho = @(p) rho_r .* exp(c * (p - p_r));
            gravity reset on, g = norm(gravity);
            [z_0, z_max] = deal(0, max(G.cells.centroids(:,3)));
            equil  = ode23(@(z,p) g .* rho(p), [z_0, z_max], p_r);
            p_init = reshape(deval(equil, G.cells.centroids(:,3)), [], 1);
            ret = p_init;
        case "plot well and initial pressure"
            G = in{1};
            W = in{2};
            nperf = in{3};
            I = in{4};
            J = in{5};
            K = in{6};
            p_init = in{7};
            clf
            show = true([G.cells.num, 1]);
            cellInx = sub2ind(G.cartDims, ...
               [I-1; I-1; I; I;   I(1:2) - 1], ...
               [J  ; J;   J; J;   nperf  + [2 ; 2]], ...
               [K-1; K;   K; K-1; K(1:2) - [0 ; 1]]);

            show(cellInx) = false;

            plotCellData(G, convertTo(p_init, barsa), show, 'EdgeColor', 'k')
            plotWell(G, W, 'height', 10)
            view(-125, 20), camproj perspective
            colorbar
        case "computeTrans"
            G = in{1};
            rock = in{2};
            ret = computeTrans(G, rock);
        case "Harmonic average in interior"
            cf = in{1};
            hT = in{2};
            nf = in{3};
            intInx = in{4};
            T  = 1 ./ accumarray(cf, 1 ./ hT, [nf, 1]);
            T  = T(logical(intInx));
            ret = T;
        case "get matrix C"
            G = in{1};
            n = in{2};
            N = in{3};
            C = sparse(repmat((1 : n).', [2, 1]), N, ...
           repmat([-1, 1], [n, 1]), n, G.cells.num);
            ret = C;
        case "plot solution"
            plotFlowSolverSolution(in);
        case "compareVariables"
            load('variables/flowSolverVariablesMATLAB')
            p0  = double(p_ad);
            eq1 = presEq(p_ad, p0, dt);
            eq1(wc) = eq1(wc) - q_conn(p_ad, bhp_ad);
            eqs = {eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad)};
            eq  = cat(eqs{:});
            ret = [eq.val, eq.jac];
        case "setup 2D model"
            [nx, ny, nz] = deal( 100, 100, 1);
            [Dx, Dy, Dz] = deal(100, 100, 1);
            [nx, ny, nz] = deal( 10, 10, 1);
            [Dx, Dy, Dz] = deal(10, 10, 1);
            G = cartGrid([nx, ny, nz], [Dx, Dy, Dz]);
            G = computeGeometry(G);
            rock.perm = repmat(100*milli*darcy, [G.cells.num, 1]);
            rock.poro = repmat(0.3            , [G.cells.num, 1]);
%             lowperm = [41:45 51:55]';
%             low = 1*milli*darcy;
%             rock.perm(lowperm) = low;
%             rock.poro(68) = 0.2;
            N  = double(G.faces.neighbors);
            intInx = all(N ~= 0, 2);
            N  = N(intInx, :);                            % Interior neighbors
            hT = computeTrans(G, rock);                   % Half-transmissibilities
            cf = G.cells.faces(:,1);                      % Map: cell -> face number
            nf = G.faces.num;                             % Number of faces
            T  = 1 ./ accumarray(cf, 1 ./ hT, [nf, 1]);   % Harmonic average
            T  = T(intInx);
            n = size(N, 1);
            C = sparse(repmat((1 : n).', [2, 1]), N, ...
            repmat([-1, 1], [n, 1]), n, G.cells.num);
            grad = @(x) C * x;
            gradz  = grad(G.cells.centroids(:,3));
            pv_r = poreVolume(G, rock);
            %%Well:
            rate = 0.5*meter^3/day;
            bhp = 1*barsa;
            W = verticalWell([], G, rock, nx, ny, 1:nz,     ...
                 'Type','bhp', 'Val', bhp, ...
                 'Radius', .1, 'Dir', 'x', 'Name', 'P', 'Comp_i', [0 1]);
            WI = W.WI;
            W = verticalWell([], G, rock, 1, 1, 1:nz,          ...
                 'Type', 'rate', 'Val', rate, ...
                 'Radius', .1, 'Name', 'I', 'Comp_i', [1 0]);
            rSol    = initState(G, W, 0, [0.2, 0.8]);
            p0 = rSol.pressure;
            s0 = rSol.s(:,1);
            %% Schedule and injection/production
            nstep = 36;
            Tf = 360*day;
            dt = Tf/nstep*ones(1,nstep);
            dt = [dt(1).*sort(repmat(2.^-[1:5 5],1,1)) dt(2:end)];
            nstep = numel(dt);
            [inRate,  inIx ] = deal(.1*sum(pv_r)/Tf, 1);
            inRate = rate; 
            [outPres, outIx] = deal(bhp, G.cells.num);
            well = [inRate,  inIx, outPres, outIx, WI];
            ret = {G, N, T, gradz, pv_r, well, dt, Tf, p0, s0};
        case "plot 2D grid"
            clf
            show = true([in{1}.cells.num, 1]);
            plotCellData(in{1}, in{2}, show, 'EdgeColor', 'k')
            
            colorbar
    end
end

function s = char2intInStruct(s)
    fields = fieldnames(s);
    for i = 1:numel(fields)
        if ischar(s.(fields{i}))
            s.(fields{i}) = double(s.(fields{i}));
        end
    end
end
