function ret = spe10Simulation(functionName,in)
    addpath("mrst-2018a/")
    mrstModule add ad-fi deckformat spe10
    ret = 0;
    switch (functionName)
        case "initializeMRST"
            startup();
            mrstModule add ad-core
        case "setup model"
            ret = setupModel();
        case "run matlab model"
            runMatlabModel();
        case "get operator matrix"
            layers = 5;
            [G, W, rock] = getSPE10setup(layers);
            re
        case "plot solution"
            plotSolution2D(in)
        case "plot solution 3D"
            plotSolution3D(in)
    end
end

function ret = setupModel()
    % kr_deck:
    %load('/Users/sindregrostad/git/sindre-msc-thesis.jl/mrst-2018a/examples/data/SPE10/model1_data.mat')
    % The case neglect gravity
    gravity off
    layers = 5;
    [G, W, rock] = getSPE10setup(layers);
    [state, model, schedule]  = setupSPE10_AD('layers',5);
    G = model.G;
    rock = model.rock;
    low = 1e-4;
    rock.poro(rock.poro < low) = low;
    initSat = [0 1 0];
    state0 = initResSol(G, 6000*psia, initSat);
    schedule = "ten days steps, 3 years";
    % twenty days initializing:
    dt = [.001*day; 0.009*day; 0.09*day; .1*day*ones(4,1); 0.25*day*ones(10,1); ...
                0.5*day*ones(14,1); 1*day*ones(10,1)];
    switch schedule
        case "ten days steps, 2.5years"
            dt = [dt; 10*day*ones(89,1)];
        case "one day steps, 2.5years"
            dt = [dt; 1*day*ones(890,1)];
        case "one day steps, 3 years"
            dt = [dt; 5*day; 1*day*ones(1070,1)];
        case "five days steps, 3 years"
            dt = [dt; 5*day*ones(215,1)];
        case "ten days steps, 3 years"
            dt = [dt; 5*day; 10*day*ones(107,1)];
        case "ten days steps, 5 years"
            dt = [dt; 10*day*ones(180,1)];
        case "ten days steps, 20 years"
            dt = [dt; 10*day*ones(728,1)];
        otherwise
            error("unvalid schedule type");
    end
    Tf = sum(dt);
    nstep = numel(dt);
    N  = double(G.faces.neighbors);
    intInx = all(N ~= 0, 2);
    N  = N(intInx, :);                            % Interior neighbors
    hT = computeTrans(G, rock);                   % Half-transmissibilities
    cf = G.cells.faces(:,1);                      % Map: cell -> face number
    nf = G.faces.num;                             % Number of faces
    T  = 1 ./ accumarray(cf, 1 ./ hT, [nf, 1]);   % Harmonic average
    T  = T(intInx);
    pv_r = poreVolume(G, rock);
    inRate = 0.135*sum(pv_r)/Tf;
    p0 = state0.pressure;
    s0 = state0.s(:,1);
    injectorIx = W(5).cells;
    outPres = [W(1:4).val]';
    outIx = [W(1:4).cells]';
    WI = [W(1:4).WI]';
    well = {inRate, injectorIx, outPres, outIx, WI};
    ret = {G, N, T, pv_r, well, dt, Tf, p0, s0};
end

function plotSolution2D(in)
    figure
    s = in{1};
    dt = in{2};
    steps = in{3};
    
    layers = 5;
    [G, W, rock] = getSPE10setup(layers);
    if length(steps) == 1
        show = true([G.cells.num, 1]);
        plotCellData(G, s, show, 'EdgeColor', 'k')
        % plotGridVolumes(G, s);
        plotWell(G, W);
        plotGrid(G, 'facea', 0, 'edgea', .05);
        %view(-60,  70);
        axis tight off
        title(['Water front after ' formatTimeRange(sum(dt(1:steps(1))))])
        colorbar
    else
        intervals = floor(length(steps)/20);
        for i = 1:intervals:length(steps)
            clf
            show = true([G.cells.num, 1]);
            plotCellData(G, s{i}, show, 'EdgeColor', 'k')
            axis tight off
            title(['Water front after ' formatTimeRange(sum(dt(1:steps(i))))])
            colorbar
            drawnow
            pause(0.5)
        end
    end
end
function plotSolution3D(in)
    figure
    s = in{1};
    dt = in{2};
    steps = in{3};
    
    layers = 5;
    [G, W, rock] = getSPE10setup(layers);
    intervals = floor(length(steps)/15);
    i = 1;
    while i < length(steps)
        clf;
        plotGrid(G, s{i} > 0, 'facea', .3, 'facec', 'red', 'edgea', 0);
        % Uncomment for slower, but prettier volume plotting
    %     plotGridVolumes(G, data);
        plotWell(G, W);
        plotGrid(G, 'facea', 0, 'edgea', .05);
        view(-60,  70);
        axis tight off
        title(['Water front after ' formatTimeRange(sum(dt(1:i)))])
        pause(.1)
        if i == length(steps)
            break;
        elseif i+intervals > length(steps)
            i = length(steps);
        else
            i = i + intervals;
        end
    end
end



function runMatlabModel()
    % Read and process file.
    %current_dir = fileparts(mfilename('fullpath'));
    %fn    = fullfile(current_dir, 'SPE10-S3.DATA.txt');

    %deck = readEclipseDeck(fn);

    % The deck is given in field units, MRST uses metric.
    %deck = convertDeckUnits(deck);
    % kr_deck and rock:
    load('/Users/sindregrostad/git/sindre-msc-thesis.jl/mrst-2018a/examples/data/SPE10/model1_data.mat')

    % Create a special ADI fluid which can produce differentiated fluid
    % properties.
    

    % The case neglect gravity
    gravity off
    
    layers = 5;
    
    [G, W, rock] = getSPE10setup(layers);
    [state, model, schedule]  = setupSPE10_AD('layers',5);
    [ws, states, report] = simulateScheduleAD(state, model, schedule)
    
    % SPE10 contains zero and extremely low porosities. For the purpose of this
    % tutorial, we will mask away these values. An alternative would be to set
    % these cells to inactive by using extractSubgrid and removing the
    % corresponding cells.
    low = 1e-4;
    rock.poro(rock.poro < low) = low;
    
    clf;
    plotCellData(G, log10(rock.perm(:,1)));
    
    % The initial reservoir is at 6000 psi and is fully oil saturated. The well
% solution gets its initial pressure from the bottom hole pressure values
% provided.
initSat = [0 1 0];
state0 = initResSol(G, 6000*psia, initSat);
state0.wellSol = initWellSolLocal(W, state0);

for i = 1:numel(W)
    state0.wellSol(i).pressure = W(i).val;
    % Set well sign
    if strcmpi(W(i).name(1), 'p')
        W(i).sign = -1;
    else
        W(i).sign = 1;
    end
end

% Set up a Water / Oil system using CPR preconditioner. Alternatively we
% could have used a specialized elliptic solver using the option 'cprEllipticSolver'
% to exploit the nature of the variables involving pressure.

system = initADISystem({'Water', 'Oil'}, G, rock, fluid, 'cpr', true, 'cprRelTol', 2e-2);
% If an alternative solver for the pressure subproblem was installed, it
% could be added using
%
%    system.nonlinear.cprEllipticSolver = @(A,b) solver(A,b)
%
% This can greatly speed up the solution process, as the default option
% uses MATLABs direct solver @mldivide which is very expensive for a
% preconditioner.

dt = [.001*day; .1*day*ones(5,1); 1*day*ones(10,1); 10*day*ones(100,1)];
nstep = numel(dt);

states = cell(nstep,1);
its = zeros(nstep,1);
time = zeros(nstep,1);

state = state0;
for t = 1 : nstep
    timer = tic();

    [state, it] = solvefiADI(state, dt(t), W, G, system);

    states{t} = state;
    its(t) = it;
    time(t) = toc(timer);
end 

for i = 1:numel(states)
    clf;
    data = states{i}.s(:,1);
    plotGrid(G, data > 0, 'facea', .3, 'facec', 'red', 'edgea', 0);
    % Uncomment for slower, but prettier volume plotting
%     plotGridVolumes(G, data);
    plotWell(G, W);
    plotGrid(G, 'facea', 0, 'edgea', .05);
    view(-60,  70);
    axis tight off
    title(['Water front after ' formatTimeRange(sum(dt(1:i)))])
    pause(.1)
end
end