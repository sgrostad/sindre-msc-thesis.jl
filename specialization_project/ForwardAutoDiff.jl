module ForwardAutoDiff
using LinearAlgebra, SparseArrays
export AD, initialize_AD, print_dimensions
import Base: -, +, *, /, ^, exp, getindex, inv, ==, copy, iterate
import Base: length, setindex!, cat, vcat
struct AD
    val
    jac
end

##### All operators are overladed to be elementwise operations #####
### Overload +:
function +(A::AD, B::AD)
    if length(A) == length(B.val)
        return AD(A.val + B.val, broadcast(+, A.jac, B.jac))
    elseif length(A) == 1
        A = convert_scalar_AD(A,length(B.val))
        return AD(A.val + B.val, broadcast(+, A.jac, B.jac))
    elseif length(B.val) == 1
        B = convert_scalar_AD(B,length(A))
        return AD(A.val + B.val, broadcast(+, A.jac, B.jac))
    else
        error("AD vectors have uncompatible lengths")
    end
end
+(A::AD, B::Number) = AD(A.val .+ B, A.jac)
+(A::Number, B::AD) = B+A
+(A::AD, B::AbstractVector) = AD(A.val .+ B, A.jac)
+(A::AbstractVector, B::AD) = B + A

### Overload -:
-(A::AD) = minus(A)
-(A::AD, B::AD) = A + minus(B)
-(A::AD, B::Number) = AD(A.val .- B, A.jac)
-(A::Number, B::AD) = A + minus(B)
-(A::AD, B::AbstractVector) = AD(A.val .- B, A.jac)
-(A::AbstractVector, B::AD) = A + minus(B)

### Overload *:
function *(A::AD, B::AD)
    if length(A) == length(B.val)
        jac = elementwise_multiply_jacobians(A, B)
        return AD(A.val .* B.val, jac)
    elseif length(A) == 1
        A = convert_scalar_AD(A,length(B.val))
        jac = elementwise_multiply_jacobians(A, B)
        return AD(A.val .* B.val, jac)
    elseif length(B.val) == 1
        B = convert_scalar_AD(B,length(A))
        jac = elementwise_multiply_jacobians(A, B)
        return AD(A.val .* B.val, jac)
    else
        error("AD vectors have uncompatible lengths")
    end
end
*(A::AD, B::Number) = AD(A.val .* B, broadcast(*, A.jac, B))
*(A::Number, B::AD) = B * A
function *(A::AD, B::AbstractVector)
    if length(A) != length(B) && length(A) != 1
        error("Uncompatible lengths on AD and vector.")
    end
    val = A.val .* B
    if length(A) == 1
        jac = vector_times_single_row_jacobian(B, A.jac)
    else
        jac = jac_with_chain_rule(B, A.jac)
    end
    return AD(val, jac)
end
*(A::AbstractVector, B::AD) = B * A
function *(A::AbstractArray, B::AD)
    if size(A,2) != length(B)
        error("Array and AD dimensions does not support array multiplication.")
    end
    val = A * B.val
    jac = matrix_times_jacobian(A, B.jac)
    return AD(val,jac)
end

### Overload /:
function /(A::AD, B::AD)
    return A * B^(-1)
end
/(A::AD, B::Number) = AD(A.val ./ B, broadcast(/, A.jac, B))
/(A::Number, B::AD) = A * B^(-1)
/(A::AbstractVector, B::AD) = A * B^(-1)
/(A::AD, B::AbstractVector) = A * (1 ./ B)

###Overload ^:
function ^(A::AD, k::Number)
    jac = jac_with_chain_rule(k .* A.val.^(k-1), A.jac)
    AD(A.val.^k, jac)
end

function inv(A::AD)
    jac = jac_with_chain_rule(-A.val.^(-2), A.jac)
    AD(A.val.^(-1), jac)
end
### Overload exp():
function exp(A::AD)
    jac = jac_with_chain_rule(exp.(A.val), A.jac)
    AD(exp.(A.val), jac)
end

### Overload ==:
function (==)(A::AD, B::AD)
    if A.val ≈ B.val && A.jac ≈ B.jac
        return true
    end
    return false
end

### Overload copy():
function copy(A::AD)
    val = copy(A.val)
    n = length(A.jac)
    jac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,n)
    for i in 1:n
        jac[i] = copy(A.jac[i])
    end
    return AD(val, jac)
end

function getindex(A::AD, ix)
    n = length(ix)
    if maximum(ix) > length(A)
        error("Index can not be higher than length of AD. \n
        Index = $ix, length AD = $(length(A))")
    elseif minimum(ix) <= 0
        error("Negative or zero indexes does not exist. \n
        Index = $ix.")
    end
    nJac = length(A.jac)
    jac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,nJac)
    for i = 1:nJac
        # TODO find out why this is necessary
        if n == 1
            jac[i] = transpose(A.jac[i][ix, :])
        else
            jac[i] = A.jac[i][ix, :]
        end
    end
    return AD(A.val[ix], jac)
end

function setindex!(A::AD, B::AD, ix::Int64)
    if ix > length(A)
        error("Index can not be higher than length of AD. \n
        Index = $ix, length AD = $(length(A))")
    end
    A.val[ix] = B.val
    for i = 1:length(A.jac)
        A.jac[i][ix,:] = B.jac[i]
        dropzeros!(A.jac[i])
    end
end

function setindex!(A::AD, B::AD, ix::AbstractVector)
    if length(B) != length(ix)
        error("Length of intervals needs to be equal.")
    end
    j = 1
    for i in ix
        A[i] = B[j]
        j += 1
    end
end

function iterate(iter::AD, state = 1)
    if state > length(iter.val)
        return nothing
    end
    return (iter[state], state + 1)
end

length(A::AD) = length(A.val)

function cat(A::AD, B::AD...)
    newAD = vcat(A, B...)
    jac = hcat(newAD.jac...)
    return AD(newAD.val, jac)
end

function vcat(A::AD, B::AD...)
    varargin = [A, B...]
    nArg = length(varargin)
    nJac = length(varargin[1].jac)
    vals = Array{Any}(undef, nArg)
    for i = 1:nArg
        vals[i] = varargin[i].val
    end
    equalVarJacs = Array{SparseMatrixCSC{Float64,Int64}}(undef,nArg)
    allJacs = Array{SparseMatrixCSC{Float64,Int64}}(undef,nArg)
    for jacNum = 1:nJac
        for argNum = 1:nArg
            equalVarJacs[argNum] = varargin[argNum].jac[jacNum]
        end
        allJacs[jacNum] = vcat(equalVarJacs...)
    end
    return AD(vcat(vals...), allJacs)
end


## Help functions:
function minus(A::AD)
    val = -A.val
    n = length(A.jac)
    jac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,n)
    for i in 1:n
        jac[i] = -A.jac[i]
    end
    return AD(val, jac)
end

function convert_scalar_AD(A::AD, dimB)
    val = ones(dimB) .* A.val
    # TODO See if there is a more efficient way of handling scalars.
    n = length(A.jac)
    jac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,n)
    for i in 1:length(A.jac)
        dimA = length(A.jac[i])
        jac[i] = ones(dimB,1) * A.jac[i]
    end
    return AD(val,jac)
end

function elementwise_multiply_jacobians(A::AD, B::AD)
    nVal = length(A)
    diagIndex = 1:nVal
    aValDiag = sparse(diagIndex, diagIndex, A.val)
    bValDiag = sparse(diagIndex, diagIndex, B.val)
    nJac = length(A.jac)
    jac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,nJac)
    for i = 1:nJac
        jac[i] = aValDiag * B.jac[i] + bValDiag * A.jac[i]
    end
    return jac
end

function jac_with_chain_rule(val, jac)
    nVal = length(val)
    diagIndex = 1:nVal
    valDiag = sparse(diagIndex, diagIndex, val)
    nJac = length(jac)
    newJac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,nJac)
    for i = 1:nJac
        newJac[i] = valDiag * jac[i]
    end
    return newJac
end

function matrix_times_jacobian(matrix, jac)
    nJac = length(jac)
    newJac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,nJac)
    for i = 1:nJac
        newJac[i] = matrix * jac[i]
    end
    return newJac
end

function vector_times_single_row_jacobian(A::Vector, jac)
    nJac = length(jac)
    newJac = Array{SparseMatrixCSC{Float64, Int64},1}(undef, nJac)
    for i = 1:nJac
        newJac[i] = A.*jac[i]
    end
    return newJac
end

function print_dimensions(A::AD)
    println("AD.val = ",typeof(A.val), size(A.val))
    if size(A.jac,2) == 1
        for i = 1:length(A.jac)
            println("AD.jac[$i] = ", typeof(A.jac[i]), size(A.jac[i]))
        end
    else
        println("AD.jac = ", typeof(A.jac), size(A.jac))
    end
end

function initialize_AD(x1,xn...)
    # Don't have a way to check number of output parameters yet.
    variables = [x1,xn...]
    m = length(variables)
    output = Vector{AD}(undef,m)
    for i in 1:m
        jac = Array{SparseMatrixCSC{Float64,Int64},1}(undef,m)
        nI = length(variables[i])
        for j in 1:m
            nJ = length(variables[j])
            if i == j
                jac[j] = sparse(I,nI,nI)
            else
                jac[j] = spzeros(nI, nJ)
            end
        end
        output[i] = AD(variables[i],jac)
    end
    if length(output) == 1
        return output[1]
    end
    return output
end

end
