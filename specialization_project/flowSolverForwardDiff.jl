using ForwardDiff, MATLAB, Plots, LinearAlgebra, Printf
# Line below needs to run each time you begin running matlab-scripts/functions
mxcall(:runMatlabCode, 1, "initializeMRST");

struct solution
    time
    pressure
    bhp
    qS
end

function flowSolver(makePlots)
    barsa = 1e5
    ## Set up model geometry
    (nx, ny, nz) = (10.0, 10.0, 10.0)
    (Dx, Dy, Dz) = (200.0, 200.0, 50.0)
    geometryInput = mxarray([nx ny nz Dx Dy Dz])
    G = mxcall(:runMatlabCode, 1, "compute geometry", geometryInput)

    ## Define compressible rock model
    rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
    cr = 1e-11
    p_r = 200e5
    pv_r = rock["poro"].*G["cells"]["volumes"]
    pv(p,i) = pv_r[i]* exp(cr * (p-p_r)) ## This was difficult in Julia given I have understood correctly
    pv(p) = [pv(p[i],i) for i in 1:length(p)]
    pvPlot(p) = pv_r[1] .* exp(cr * (p-p_r)) # Not sure why plot uses different
    #plot(p, map(pvPlot,p))

    ##Define model for compressible fluid
    mu = 0.0050
    c = 1e-8
    rho_r = 850.0
    rhoS = 750.0
    rho(p) = rho_r * exp(c * (p-p_r))

    ##Add well to geometry
    nperf = 8.0
    I = mxarray(ones(8,1)*2.0)
    J = mxarray(collect(2:9)*1.0)
    K = mxarray(ones(8,1)*5.0)

    W = mxcall(:runMatlabCode, 1, "add well",
                mxarray([mxarray(G) mxarray(rock) I J K ]))

    ##Derive initial pressure
    g = 9.806649999999999
    p_init = mxcall(:runMatlabCode, 1, "derive initial pressure",
                    mxarray([mxarray(G) p_r rho_r c]))

    ## Plot well and initial pressure
    if makePlots
        mxcall(:runMatlabCode, 1, "plot well and initial pressure",
                mxarray([mxarray(G) mxarray(W) nperf I J K mxarray(p_init)]))
    end

    ## compute transmissibilities on interior connections
    Ntemp = G["faces"]["neighbors"]
    intInx = [Int(n != 0) for n in Ntemp]
    intInx = convert(Array{Bool,1},intInx[:,1].* intInx[:,2])
    # TODO need to be a more elegant way to create N...
    N = zeros(2700,2)
    N[:,1] = [Ntemp[i,1] for i in 1:size(Ntemp,1) if intInx[i]]
    N[:,2] = [Ntemp[i,2] for i in 1:size(Ntemp,1) if intInx[i]]
    hT = mxcall(:runMatlabCode, 1, "computeTrans",
                mxarray([mxarray(G) mxarray(rock)]))
    cf = G["cells"]["faces"][:,1]
    nf = G["faces"]["num"]
    T = mxcall(:runMatlabCode, 1, "Harmonic average in interior",
            mxarray([mxarray(cf) mxarray(hT) nf mxarray(intInx)]))

    ## Define discrete operators
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)])) #TODO make this matrix in Julia
    N = convert(Array{Int64,2},N)
    gradient(x) = C * x
    divergence(x) = -C' * x
    average(x) = 0.5 * ([x[n] for n in N[:,1]] + [x[n] for n in N[:,2]])
    if makePlots
        spy(C) #Very slow and can have problems with collision with function in
        #MATLAB library
    end

    ## Define flow equations
    gradz = gradient(G["cells"]["centroids"][:,3])
    flux(p) = -(T / mu).* (gradient(p) - g*average(rho.(p)).*gradz)
    np = length(p_init)
    presEq(p, p0, dt) = (1/dt) * (pv(p).*rho.(p) - pv(p0).*rho.(p0)) +
                        divergence(average(rho.(p)).* flux(p))
                        #not 100% equal answer as matlab.(Machine error?)
    ## Define well equations
    wc = convert(Array{Int64,1},W["cells"])
    WI = W["WI"]
    dz = W["dZ"]

    p_conn(bhp) = bhp.+ g*dz.*rho(bhp)
    #q_conn(p,bhp) = WI.* (map(rho,p[wc])./mu).* (p_conn(bhp) - p[wc])
    q_conn(in) =  WI.* (rho.(in[wc])./mu).* (p_conn(in[end]) - in[wc])

    #rateEQ(p,bhp,qS) = qS - sum(q_conn(p,bhp))/rhoS
    rateEq(in) = in[end] - sum(q_conn(in[1:end-1]))/rhoS
    ctrlEq(bhp) = bhp - 100*barsa

    ## Initialize loop
    #"AD"-Variables...
    p_ad = p_init
    bhp_ad = p_init[Int(wc[1])]
    qS_ad = 0

    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    sol = Array{solution,1}(undef, numSteps + 1)
    sol[1] = solution(0, p_ad, bhp_ad, qS_ad)

    ## Main loop
    t = 0.0
    stepNumber = 0

    p0 = p_ad
    pe(x) = presEq(x, p0, dt)
    jac1 = p_ad -> ForwardDiff.jacobian(pe,p_ad)
    jac2 = ad -> ForwardDiff.jacobian(q_conn,ad)
    jac3 = ad -> ForwardDiff.jacobian(q_conn,ad)
    while t < totTime
        t += dt
        stepNumber += 1
        @printf("\nTime step %d: Time %.2f -> %.2f days\n",
        stepNumber, (t-dt)/secondsInDay, t/secondsInDay)
        #println("\nTime step $stepNumber: Time $((t-dt)/secondsInDay) -> $(t/secondsInDay) days")

        #Newton loop
        converged = false
        p0 = p_ad
        nit = 0
        while !converged && (nit < maxIts)
            eq1 = pe(p_ad)
            eq1[wc] -= q_conn([p_ad; bhp_ad])
            eq = [eq1; rateEq([p_ad; bhp_ad; qS_ad]); ctrlEq(bhp_ad)]
            # build jacobian:
            eqjac = zeros(1002,1002)
            eqjac[1:1000,1:1000] = ForwardDiff.jacobian(pe,p_ad)
            eqjac[wc,1:1001] -= ForwardDiff.jacobian(q_conn,[p_ad;bhp_ad])
            ## Hard code row "end-1" in jac since ForwardDiff does not support "sum":
            # eqjac[end-1,:] =  ForwardDiff.jacobian(rateEQ, [p_ad;bhp_ad;qS_ad])
            q_conn_jac = ForwardDiff.jacobian(q_conn,[p_ad;bhp_ad])
            rateEQ_jac = [sum(q_conn_jac,dims=1)./rhoS 1]
            eqjac[end-1,:] = rateEQ_jac
            ## Last row are all zeros except:
            eqjac[end,end-1] = 1
            #jacobian build finished.
            upd = -(eqjac\eq)
            #Update variables:
            p_ad += upd[pIx]
            bhp_ad += upd[bhpIx]
            qS_ad += upd[qSIx]

            residual = norm(eq)
            converged = (residual<tol)
            nit       = nit + 1;
            @printf("\tIteration %3d:  Res = %.4e\n", nit, residual)
        end
        if !converged
            error("Newton solver did not converge")
        else
            sol[stepNumber + 1] = solution(t, p_ad, bhp_ad, qS_ad)
        end
    end
    return sol, G, W, nperf, dt
end

sol, G, W, nperf, dt = flowSolver(false)
mxcall(:runMatlabCode, 1, "plot solution",
            mxarray([mxstructarray(sol) mxarray(G) mxarray(W) dt nperf]));
