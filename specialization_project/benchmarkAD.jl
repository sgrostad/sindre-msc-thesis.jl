using ForwardAutoDiff, ForwardDiff, LinearAlgebra, Plots, Statistics, MATLAB
using BenchmarkTools, SparseArrays, Plots.PlotMeasures
# Line below needs to run each time you begin running matlab-scripts/functions
mxcall(:runMatlabCode, 1, "initializeMRST");

function analytic_solution(x,y,z)
    f = exp.(2*x.*y) - 4*x.*z.^2 + 13*x .- 7
    fx = 2*y.*exp.(2*x.*y) - 4*z.^2 .+ 13
    fy = 2*x.*exp.(2*x.*y)
    fz = -8*x.*z
end
function analytic_solution_in_loop(x,y,z)
    n = length(x)
    f = Vector{Float64}(undef,n)
    fx = Vector{Float64}(undef,n)
    fy = Vector{Float64}(undef,n)
    fz = Vector{Float64}(undef,n)
    for i = 1:n
        f[i] = exp(2*x[i]*y[i]) - 4*x[i]*z[i]^2 + 13*x[i] - 7
        fx[i] = 2*y[i]*exp(2*x[i]*y[i]) - 4*z[i]^2 + 13
        fy[i] = 2*x[i]*exp(2*x[i]*y[i])
        fz[i] = -8*x[i]*z[i]
    end
end
function forward_auto_diff(xi,yi,zi)
    (x_ad,y_ad,z_ad) = initialize_AD(xi,yi,zi)
    f(x,y,z) = exp(2*x*y) - 4*x*z^2 + 13*x - 7
    f_ad = f(x_ad,y_ad,z_ad)
end
function forward_auto_diff_in_loop(xi,yi,zi)
    f(x,y,z) = exp(2*x*y) - 4*x*z^2 + 13*x - 7
    n = length(xi)
    (x_ad,y_ad,z_ad) = initialize_AD(xi,yi,zi)
    val = zeros(n)
    jac = [spzeros(n,n), spzeros(n,n), spzeros(n,n)]
    f_ad = AD(val,jac)
    for i = 1:n
        f_ad[i] = f(x_ad[i],y_ad[i],z_ad[i])
    end
end
function forward_diff(xi,yi,zi)
    n = length(xi)
    xIndx = 1:n
    yIndx = (n+1):2*n
    zIndx = (2*n+1):3*n
    f(in) = exp.(2*in[xIndx].*in[yIndx]) - 4*in[xIndx].*in[zIndx].^2 +
        13*in[xIndx] .- 7
    val = f([xi; yi; zi])
    jac = ForwardDiff.jacobian(f,[xi; yi; zi])
end
function forward_diff_for_loop(xi,yi,zi)
    n = length(xi)
    # compute function value:
    xIndx = 1:n
    yIndx = (n+1):2*n
    zIndx = (2*n+1):3*n
    f_all(in) = exp.(2*in[xIndx].*in[yIndx]) - 4*in[xIndx].*in[zIndx].^2 +
        13*in[xIndx] .- 7
    f = f_all([xi; yi; zi])
    # compute jacobian:
    f_single(in) = exp(2*in[1]*in[2]) - 4*in[1]*in[3]^2 +
        13*in[1] - 7
    jac = Array{SparseMatrixCSC{Float64,Int64}}(undef,3)
    jac[1] = spzeros(n,n)
    jac[2] = spzeros(n,n)
    jac[3] = spzeros(n,n)
    for i = 1:n
        g = ForwardDiff.gradient(f_single,[xi[i]; yi[i]; zi[i]])
        for j = 1:3
            jac[j][i,i] = g[j]
        end
    end
end

## Benchmarks:

function benchmark_all_AD()
    nanoseconds = 10^(-9)
    k = 10
    lengthVectors = zeros(Int64, k)
    recordedTimes = zeros(k,4)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 0.3
    for i = 1:k
        println("i = ", i)
        lengthVectors[i] = 3^(i-1)
        x = rand(lengthVectors[i])
        y = rand(lengthVectors[i])
        z = rand(lengthVectors[i])
        recordedTimes[i,1] =
        median(@benchmark forward_diff($x,$y,$z)).time * nanoseconds
        recordedTimes[i,2] =
        median(@benchmark forward_auto_diff($x,$y,$z)).time * nanoseconds
        recordedTimes[i,4] =
        median(@benchmark analytic_solution($x,$y,$z)).time * nanoseconds
    end
    println("Matlab:")
    matlabRes = mxcall(:benchmark_mrst_ad, 1, mxarray(lengthVectors))
    recordedTimes[:,3] = matlabRes[:,1]
    p = plot(lengthVectors, recordedTimes, label=["ForwardDiff",
    "ForwardAutoDiff","MRST", "Analytic"], legend=:topleft,
    shape = [:circle])
    xaxis!("Length vectors",:log10)
    yaxis!("Time [s]",:log10)
    return p
end

function for_loop_benchmark()
    nanoseconds = 10^(-9)
    k = 10
    lengthVectors = zeros(Int64, k)
    recordedTimes = zeros(k,6)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 0.3
    for i = 1:k
        println("i = ", i)
        lengthVectors[i] = 3^(i-1)
        x = rand(lengthVectors[i])
        y = rand(lengthVectors[i])
        z = rand(lengthVectors[i])
        recordedTimes[i,1] =
        median(@benchmark forward_diff($x,$y,$z)).time * nanoseconds
        recordedTimes[i,2] =
        median(@benchmark forward_diff_for_loop($x,$y,$z)).time * nanoseconds
        recordedTimes[i,3] =
        median(@benchmark forward_auto_diff($x,$y,$z)).time * nanoseconds
        recordedTimes[i,4] =
        median(@benchmark forward_auto_diff_in_loop($x,$y,$z)).time *nanoseconds
        recordedTimes[i,5] =
        median(@benchmark analytic_solution($x,$y,$z)).time * nanoseconds
        recordedTimes[i,6] =
        median(@benchmark analytic_solution_in_loop($x,$y,$z)).time *nanoseconds
    end

    #C(g::ColorGradient) = RGB[g[z] for z=linspace(0,1,30)]
    num = 400
    g1 = :plasma
    g2 = :blues
    g3 = :viridis
    p = plot(lengthVectors, recordedTimes,
    label=["ForwardDiff", "ForwardDiff in loop", "ForwardAutoDiff",
    "ForwardAutoDiff in loop", "Analytic", "Analytic in loop"],
    legend=:topleft,
    shape = [:circle :square :circle :square :circle :square],
    xaxis = ("Length vectors",:log10),
    yaxis = ("Time [s]",:log10))

    return p
end

function long_vectors_benchmark()
    nanoseconds = 10^(-9)
    k = 15
    lengthVectors = zeros(Int64, k)
    recordedTimes = zeros(k,4)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 0.3
    for i = 1:k
        println("i = ", i)
        lengthVectors[i] = 3^(i-1)
        x = rand(lengthVectors[i])
        y = rand(lengthVectors[i])
        z = rand(lengthVectors[i])
        recordedTimes[i,1] = median(@benchmark analytic_solution($x,$y,$z)).time *
            nanoseconds
        recordedTimes[i,2] = median(@benchmark forward_auto_diff($x,$y,$z)).time *
            nanoseconds
    end
    println("Matlab:")
    matlabRes = mxcall(:benchmark_mrst_ad, 1, mxarray(lengthVectors))
    recordedTimes[:,3] = matlabRes[:,1]
    recordedTimes[:,4] = matlabRes[:,2]
    p = plot(lengthVectors, recordedTimes, label=["Analytic", "ForwardAutoDiff",
        "MRST old", "MRST new"], legend=:topleft, shape = [:circle])
    xaxis!("Length vectors",:log10)
    yaxis!("Time [s]",:log10)
    return p
end

p = benchmark_all_AD()
#p = for_loop_benchmark()
#p = long_vectors_benchmark()
