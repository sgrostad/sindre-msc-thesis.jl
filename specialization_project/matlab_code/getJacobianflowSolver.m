function J = getJacobianflowSolver(d)
    name = strcat('variables/flowSolverVariablesMATLAB','Disc',...
            num2str(d)); 
    load(name)
    [p_ad, bhp_ad, qS_ad] = initVariablesADI(p_init, p_init(wc(1)), 0);
    p0  = double(p_ad);
    eq1     = presEq(p_ad, p0, dt);
    eq1(wc) = eq1(wc) - q_conn(p_ad, bhp_ad);
    eqs = {eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad)};
    eq  = cat(eqs{:});
    J   = eq.jac{1};
end