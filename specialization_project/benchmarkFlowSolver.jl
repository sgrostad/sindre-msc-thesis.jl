using ForwardAutoDiff, ForwardDiff, MATLAB, LinearAlgebra, Printf
using BenchmarkTools, Statistics, Profile
# Line below needs to run each time you begin running matlab scripts/functions
mxcall(:runMatlabCode, 1, "initializeMRST");

function setup_forward_diff()
    barsa = 1e5
    ## Set up model geometry
    (nx, ny, nz) = (10.0, 10.0, 10.0)
    (Dx, Dy, Dz) = (200.0, 200.0, 50.0)
    geometryInput = mxarray([nx ny nz Dx Dy Dz])
    G = mxcall(:runMatlabCode, 1, "compute geometry", geometryInput)

    ## Define compressible rock model
    rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
    cr = 1e-11
    p_r = 200e5
    pv_r = rock["poro"].*G["cells"]["volumes"]
    pv2(p,i) = pv_r[i]* exp(cr * (p-p_r)) ## This was difficult in Julia given I have understood correctly
    pv(p) = [pv2(p[i],i) for i in 1:length(p)]

    ##Define model for compressible fluid
    mu = 0.0050
    c = 1e-8
    rho_r = 850.0
    rhoS = 750.0
    rho(p) = rho_r * exp(c * (p-p_r))

    ##Add well to geometry
    nperf = 8.0
    I = mxarray(ones(8,1)*2.0)
    J = mxarray(collect(2:9)*1.0)
    K = mxarray(ones(8,1)*5.0)

    W = mxcall(:runMatlabCode, 1, "add well",
                mxarray([mxarray(G) mxarray(rock) I J K ]))

    ##Derive initial pressure
    g = 9.806649999999999
    p_init = mxcall(:runMatlabCode, 1, "derive initial pressure",
                    mxarray([mxarray(G) p_r rho_r c]))

    ## Plot well and initial pressure


    ## compute transmissibilities on interior connections
    Ntemp = G["faces"]["neighbors"]
    intInx = [Int(n != 0) for n in Ntemp]
    intInx = convert(Array{Bool,1},intInx[:,1].* intInx[:,2])
    # TODO need to be a more elegant way to create N...
    N = zeros(2700,2)
    N[:,1] = [Ntemp[i,1] for i in 1:size(Ntemp,1) if intInx[i]]
    N[:,2] = [Ntemp[i,2] for i in 1:size(Ntemp,1) if intInx[i]]
    hT = mxcall(:runMatlabCode, 1, "computeTrans",
                mxarray([mxarray(G) mxarray(rock)]))
    cf = G["cells"]["faces"][:,1]
    nf = G["faces"]["num"]
    T = mxcall(:runMatlabCode, 1, "Harmonic average in interior",
            mxarray([mxarray(cf) mxarray(hT) nf mxarray(intInx)]))

    ## Define discrete operators
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)])) #TODO make this matrix in Julia
    N = convert(Array{Int64,2},N)
    gradient(x) = C * x
    divergence(x) = -C' * x
    average(x) = 0.5 * ([x[n] for n in N[:,1]] + [x[n] for n in N[:,2]])


    ## Define flow equations
    gradz = gradient(G["cells"]["centroids"][:,3])
    flux(p) = -(T / mu).* (gradient(p) - g*average(rho.(p)).*gradz)
    np = length(p_init)
    presEq(p, p0, dt) = (1/dt) * (pv(p).*rho.(p) - pv(p0).*rho.(p0)) +
                        divergence(average(rho.(p)).* flux(p))
                        #not 100% equal answer as matlab.(Machine error?)
    ## Define well equations
    wc = convert(Array{Int64,1},W["cells"])
    WI = W["WI"]
    dz = W["dZ"]

    p_conn(bhp) = bhp.+ g*dz.*rho(bhp)
    #q_conn(p,bhp) = WI.* (map(rho,p[wc])./mu).* (p_conn(bhp) - p[wc])
    q_conn(in) =  WI.* (rho.(in[wc])./mu).* (p_conn(in[end]) - in[wc])

    #rateEQ(p,bhp,qS) = qS - sum(q_conn(p,bhp))/rhoS
    rateEq(in) = in[end] - sum(q_conn(in[1:end-1]))/rhoS
    ctrlEq(bhp) = bhp - 100*barsa

    ## Main loop
    t = 0.0
    stepNumber = 0
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    p0 = p_init
    return ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv, pv2, pv_r, p_r, cr, G
end

function benchmark_forward_diff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv, pv2, pv_r, p_r, cr, G)
    ## Initialize loop
    #"AD"-Variables...
    p_ad = p_init
    bhp_ad = p_init[Int(wc[1])]
    qS_ad = 0

    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10
    t = 0.0
    stepNumber = 0
    while t < totTime
        t += dt
        pe(x) = presEq(x, p0, dt)
        jac1 = p_ad -> ForwardDiff.jacobian(pe,p_ad)
        jac2 = ad -> ForwardDiff.jacobian(q_conn,ad)
        jac3 = ad -> ForwardDiff.jacobian(q_conn,ad)
        stepNumber += 1
        #Newton loop
        converged = false
        p0 = p_ad
        nit = 0
        while !converged && (nit < maxIts)
            eq1 = pe(p_ad)
            eq1[wc] -= q_conn([p_ad; bhp_ad])
            eq = [eq1; rateEq([p_ad; bhp_ad; qS_ad]); ctrlEq(bhp_ad)]
            # build jacobian:
            eqjac = zeros(1002,1002)
            eqjac[1:1000,1:1000] = ForwardDiff.jacobian(pe,p_ad)
            eqjac[wc,1:1001] -= ForwardDiff.jacobian(q_conn,[p_ad;bhp_ad])
            ## Hard code row "end-1" in jac since ForwardDiff does not support "sum":
            # eqjac[end-1,:] =  ForwardDiff.jacobian(rateEQ, [p_ad;bhp_ad;qS_ad])
            q_conn_jac = ForwardDiff.jacobian(q_conn,[p_ad;bhp_ad])
            rateEQ_jac = [sum(q_conn_jac,dims=1)./rhoS 1]
            eqjac[end-1,:] = rateEQ_jac
            ## Last row are all zeros except:
            eqjac[end,end-1] = 1
            eqjac = eqjac
            #jacobian build finished.
            upd = -(eqjac\eq)
            #Update variables:
            p_ad += upd[pIx]
            bhp_ad += upd[bhpIx]
            qS_ad += upd[qSIx]

            residual = norm(eq)
            converged = (residual<tol)
            nit       = nit + 1;
        end
    end
end

function setup_forward_auto_diff(disc)
    barsa = 1e5
    ## Set up model geometry
    (nx, ny, nz) = (10.0 * disc, 10.0 * disc, 10.0 * disc)
    (Dx, Dy, Dz) = (200.0, 200.0, 50.0)
    geometryInput = mxarray([nx ny nz Dx Dy Dz])
    G = mxcall(:runMatlabCode, 1, "compute geometry", geometryInput)

    ## Define compressible rock model
    rock = mxcall(:runMatlabCode, 1, "create rock", mxarray(G))
    cr = 1e-11
    p_r = 200e5
    pv_r = rock["poro"].*G["cells"]["volumes"]
    pv(p) = pv_r .* exp.(cr * (p.-p_r))
    pv_ad(p) = pv_r * exp(cr * (p-p_r))
    p = collect(range(100e5,length = 50, stop = 220e5))

    ##Define model for compressible fluid
    mu = 0.0050
    c = 1e-8
    rho_r = 850.0
    rhoS = 750.0
    rho(p) = rho_r * exp(c * (p-p_r))

    ##Add well to geometry
    nperf = 8.0 * disc
    i = mxarray(ones(Int(nperf),1)*2.0)
    j = mxarray(collect(2:Int(nperf)+1)*1.0)
    k = mxarray(ones(Int(nperf),1)*5.0)

    W = mxcall(:runMatlabCode, 1, "add well",
                mxarray([mxarray(G) mxarray(rock) i j k ]))

    ##Derive initial pressure
    g = 9.806649999999999
    p_init = mxcall(:runMatlabCode, 1, "derive initial pressure",
                    mxarray([mxarray(G) p_r rho_r c]))

    ## Plot well and initial pressure

    ## compute transmissibilities on interior connections
    Ntemp = G["faces"]["neighbors"]
    intInx = [Int(n != 0) for n in Ntemp]
    intInx = convert(Array{Bool,1},intInx[:,1].* intInx[:,2])
    # TODO need to be a more elegant way to create N...
    ## Hardcoding of size of N:
    sizeN = [2700, 22800, 78300]
    N = zeros(sizeN[disc],2)
    N[:,1] = [Ntemp[i,1] for i in 1:size(Ntemp,1) if intInx[i]]
    N[:,2] = [Ntemp[i,2] for i in 1:size(Ntemp,1) if intInx[i]]
    hT = mxcall(:runMatlabCode, 1, "computeTrans",
                mxarray([mxarray(G) mxarray(rock)]))
    cf = G["cells"]["faces"][:,1]
    nf = G["faces"]["num"]
    T = mxcall(:runMatlabCode, 1, "Harmonic average in interior",
            mxarray([mxarray(cf) mxarray(hT) nf mxarray(intInx)]))

    ## Define discrete operators
    n = Float64(size(N,1)) # casting for matlab code to work.
    C = mxcall(:runMatlabCode, 1, "get matrix C",
            mxarray([mxarray(G) n mxarray(N)])) #TODO make this matrix in Julia
    N = convert(Array{Int64,2},N)
    gradient(x) = C * x
    divergence(x) = -C' * x
    average(x) = 0.5 * (x[N[:,1]] + x[N[:,2]])

    ## Define flow equations
    gradz = gradient(G["cells"]["centroids"][:,3])
    flux(p) = -(T / mu) * (gradient(p) - g*average(rho(p))*gradz)
    np = length(p_init)
    presEq(p, p0, dt) = (1/dt) * (pv_ad(p)*rho(p) - pv(p0).*rho.(p0)) +
                        divergence(average(rho(p)) * flux(p))
                        #not 100% equal answer as matlab.(Machine error?)
    ## Define well equations
    wc = convert(Array{Int64,1},W["cells"])
    WI = W["WI"]
    dz = W["dZ"]

    p_conn(bhp) = bhp + g*dz*rho(bhp)
    q_conn(p,bhp) = WI * (rho(p[wc])/mu) * (p_conn(bhp) - p[wc])

    rateEq(p,bhp,qS) = qS - sum(q_conn(p,bhp))/rhoS
    ctrlEq(bhp) = bhp - 100*barsa

    return ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G
end

function benchmark_forward_auto_diff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)

    (p_ad, bhp_ad, qS_ad)= initialize_AD(p_init, p_init[Int(wc[1])], 0)
    nc = G["cells"]["num"]
    pIx = 1:convert(Int, nc)
    bhpIx = convert(Int, nc + 1)
    qSIx = convert(Int, nc + 2)

    #Simulation parameters
    numSteps = 52
    secondsInDay = 86400.0
    totTime = 365*secondsInDay
    dt = totTime / numSteps
    tol = 1e-5
    maxIts = 10

    ## Main loop
    t = 0.0
    stepNumber = 0

    while t < totTime
        t += dt
        stepNumber += 1
        #Newton loop
        converged = false
        p0 = p_ad.val
        nit = 0
        while !converged && (nit < maxIts)
            eq1 = presEq(p_ad, p0, dt)
            eq1[wc] = eq1[wc] - q_conn(p_ad, bhp_ad)

            eq = cat(eq1, rateEq(p_ad, bhp_ad, qS_ad), ctrlEq(bhp_ad))

            upd = -(eq.jac\eq.val)
            #Update variables:
            p_ad += upd[pIx]
            bhp_ad += upd[bhpIx]
            qS_ad += upd[qSIx]

            residual = norm(eq.val)
            converged = (residual<tol)
            nit       = nit + 1;
        end
    end
end


function benchmark_all_flow_solvers()
    nanoseconds = 10^(-9)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 1

    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv, pv2, pv_r, p_r, cr, G =
    setup_forward_diff()

    println("Benchmark ForwardDiff on flow solver:")
    forwardDiffTime = median(
    @benchmark benchmark_forward_diff($ctrlEq, $rateEq, $q_conn, $p_conn, $dz, $WI,
    $wc,$presEq, $np, $flux, $gradz, $average, $divergence, $gradient, $N, $C,
    $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c, $mu, $pv, $pv2,
    $pv_r, $p_r, $cr, $G)).time * nanoseconds


    ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
    presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
    hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
    setup_forward_auto_diff(1)

    println("Benchmark ForwardAutoDiff on flow solver:")
    forwardAutoDiffTime = median(
    @benchmark benchmark_forward_auto_diff($ctrlEq, $rateEq, $q_conn,
    $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
    $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
    $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G)).time * nanoseconds

    println("Benchmark MRST on flow solver:")
    mrstTime = mxcall(:benchmarkFlowSolverMRST, 1, mxarray(1))

    println("\nResults:\n")
    println("ForwardDiff:           $forwardDiffTime")
    println("forwardAutoDiffTime:   $forwardAutoDiffTime")
    println("mrst:                  $mrstTime")
end

function benchmark_larger_flow_solvers()
    nanoseconds = 10^(-9)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 1

    println("Benchmark ForwardAutoDiff on flow solver:")
    maxDisc = 2
    forwardAutoDiffTime = zeros(maxDisc)
    for i = 1:maxDisc
        ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
        presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
        hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
        setup_forward_auto_diff(i)

        forwardAutoDiffTime[i] = median(
        @benchmark benchmark_forward_auto_diff($ctrlEq, $rateEq, $q_conn,
        $p_conn, $dz, $WI, $wc, $presEq, $np, $flux, $gradz, $average, $divergence,
        $gradient, $N, $C, $T, $nf, $cf, $hT, $p_init, $g, $rho, $rhoS, $rho_r, $c,
        $mu, $pv_ad, $pv, $pv_r, $p_r, $cr, $G)).time * nanoseconds
    end

    println("Benchmark MRST on flow solver:")
    mrstTime = mxcall(:benchmarkFlowSolverMRST, 1, mxarray(collect(1:maxDisc)))
    println("Result:")
    println(forwardAutoDiffTime)
    println(mrstTime)
end

function profile_flow_solver()

end

#benchmark_all_flow_solvers()
benchmark_larger_flow_solvers()

# uncomment to profile:
#=
ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G =
setup_forward_auto_diff(1)


benchmark_forward_auto_diff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)

Profile.init(n = 10^9, delay = 0.001)
Profile.clear()
Juno.@profiler benchmark_forward_auto_diff(ctrlEq, rateEq, q_conn, p_conn, dz, WI, wc,
presEq, np, flux, gradz, average, divergence, gradient, N, C, T, nf, cf,
hT, p_init, g, rho, rhoS, rho_r, c, mu, pv_ad, pv, pv_r, p_r, cr, G)
=#
