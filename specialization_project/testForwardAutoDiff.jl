using Pkg
#Pkg.add("Test")
pathToForwardAutoDiff = pwd() * "/specialization_project/"
push!(LOAD_PATH, pathToForwardAutoDiff)
using ForwardAutoDiff, Test, SparseArrays, LinearAlgebra

@testset "testing ForwardAutoDiff.jl" begin
    @testset "initialization" begin
        x_init = collect(1:1000)
        y_init = 1001
        z_init = 1002
        (x, y, z) = initialize_AD(x_init, y_init, z_init)

        corrJacX = [sparse(I,1000,1000), spzeros(1000,1), spzeros(1000,1)]
        @test x == AD(x_init, corrJacX)

        corrJacY = [spzeros(1,1000), sparse(I,1,1), spzeros(1,1)]
        @test y == AD(y_init, corrJacY)

        corrJacZ = [spzeros(1,1000), spzeros(1,1), sparse(I,1,1)]
        @test z == AD(z_init, corrJacZ)
    end

    x_init = collect(1:5)
    y_init = 6
    z_init = collect(7:11)
    (x, y, z) = initialize_AD(x_init, y_init, z_init)
    @testset "addition" begin
        corrJac = [sparse(2*I,5,5), spzeros(5,1), spzeros(5,5)]
        @test x+x == AD(2*x_init, corrJac)
        corrJac = [sparse(I,5,5), spzeros(5,1), spzeros(5,5)]
        @test x+2 == AD(2 .+ x_init, corrJac)
        @test 2+x == AD(2 .+ x_init, corrJac)
        corrJac = [sparse(I,5,5), ones(5,1), spzeros(5,5)]
        @test x+y == AD(7:11, corrJac)
        @test y+x == AD(7:11, corrJac)
        corrJac = [sparse(I,5,5), ones(5,1), sparse(I,5,5)]
        @test y+z+x == AD([14, 16, 18, 20, 22], corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [4, 5, 7, 9, 11]
        @test x + vector == AD(corrVal, x.jac)
        @test vector + x == AD(corrVal, x.jac)
    end
    @testset "subtraction" begin
        corrJac = [sparse(I,5,5), spzeros(5,1), spzeros(5,5)]
        @test x-2 == AD(-1:3, corrJac)
        corrJac = [-sparse(I,5,5), spzeros(5,1), spzeros(5,5)]
        @test 2-x == AD(1:-1:-3, corrJac)
        corrJac = [sparse(I,5,5), -ones(5,1), spzeros(5,5)]
        @test x-y == AD(-5:-1, corrJac)
        corrJac = [-sparse(I,5,5), ones(5,1), spzeros(5,5)]
        @test y-x == AD(5:-1:1, corrJac)
        corrJac = [sparse(I,5,5), -ones(5,1), -sparse(I,5,5)]
        @test x-y-z == AD(-12 .* ones(5,1), corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [-2, -1, -1, -1, -1]
        @test x - vector == AD(corrVal, x.jac)
        corrVal = [2, 1, 1, 1, 1]
        @test vector - x == AD(corrVal, -x.jac)
    end
    @testset "multiplication" begin
        dIx = collect(1:5)
        diagVal = [2, 4, 6, 8, 10]
        corrJac = [sparse(dIx,dIx,diagVal),
                spzeros(5,1),spzeros(5,5)]
        @test x*x == AD(x_init .* x_init, corrJac)
        corrJac = [6 .* sparse(I,5,5), 1:5, spzeros(5,5)]
        @test x*y == AD([6, 12, 18, 24, 30], corrJac)
        @test y*x == AD([6, 12, 18, 24, 30], corrJac)
        corrJac = [sparse(dIx, dIx, 7:11), spzeros(5,1), sparse(dIx, dIx, 1:5)]
        @test x*z == AD([7, 16, 27, 40, 55], corrJac)
        @test z*x == AD([7, 16, 27, 40, 55], corrJac)
        corrJac = [2*corrJac[1], 2*corrJac[2], 2*corrJac[3]]
        @test 2*x*z == AD(2 .* [7, 16, 27, 40, 55], corrJac)
        @test x*z*2 == AD(2 .* [7, 16, 27, 40, 55], corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [3, 6, 12, 20, 30]
        diagVal = [3, 3, 4, 5, 6]
        corrJac = [sparse(dIx, dIx, diagVal), spzeros(5,1),
                spzeros(5,5)]
        @test x * vector == AD(corrVal, corrJac)
        @test vector * x == AD(corrVal, corrJac)
        corrVal = [18, 18, 24, 30, 36]
        corrJac = [spzeros(5,5),[3, 3, 4, 5, 6], spzeros(5,5)]
        @test vector * y == AD(corrVal, corrJac)
        @test y * vector == AD(corrVal, corrJac)
        v = transpose(vector)
        m = [v; v; v; v; v]
        corrVal = ones(5,1)*71
        corrJacRow = v
        corrJac = [[corrJacRow;corrJacRow;corrJacRow;
                corrJacRow;corrJacRow], spzeros(5,1), spzeros(5,5)]
        @test m*x == AD(corrVal, corrJac)
    end
    @testset "power" begin
        dIx = collect(1:5)
        corrVal = [1, 4, 9, 16, 25]
        corrJac = [sparse(dIx, dIx, [2, 4, 6, 8, 10]), spzeros(5,1),
                spzeros(5,5)]
        @test x^2 == AD(corrVal, corrJac)
        corrVal = [1/6, 1/12, 1/18, 1/24, 1/30]
        corrJac1 = sparse(dIx, dIx, [-0.166666666666667, -0.041666666666667,
                -0.018518518518519, -0.010416666666667, -0.006666666666667])
        corrJac2 = [-0.027777777777778, -0.013888888888889, -0.009259259259259,
                -0.006944444444444, -0.005555555555556]
        corrJac = [corrJac1, corrJac2, spzeros(5,5)]
        @test (x*y)^(-1) == AD(corrVal, corrJac)
        corrVal = z_init.^0.5
        corrJac = [spzeros(5,5), spzeros(5,1), sparse(dIx, dIx,
                [0.188982236504614, 0.176776695296637, 0.166666666666667,
                0.158113883008419, 0.150755672288882])]
        @test z^0.5 == AD(corrVal, corrJac)
    end
    @testset "division" begin
        dIx = collect(1:5)
        corrVal = [1, 1/2, 1/3, 1/4, 1/5]
        corrJac = [sparse(dIx, dIx, [-1, -1/4, -1/9, -1/16, -1/25]),
                spzeros(5,1), spzeros(5,5)]
        @test 1/x == AD(corrVal, corrJac)
        corrVal = x_init./2
        corrJac[1] = sparse(dIx, dIx, 0.5 .* ones(5))
        @test x/2 == AD(corrVal, corrJac)
        corrVal = [7, 4, 3, 2.5, 2.2]
        corrJac1 = sparse(dIx, dIx, [-7, -2, -1, -0.625, -0.44])
        corrJac3 = sparse(dIx, dIx, [1, 1/2, 1/3, 1/4, 1/5])
        corrJac = [corrJac1, spzeros(5,1), corrJac3]
        @test z/x == AD(corrVal, corrJac)
        vector = [3, 3, 4, 5, 6]
        corrVal = [1/3, 2/3, 3/4, 4/5, 5/6]
        diagVal = [1/3, 1/3, 1/4, 1/5, 1/6]
        corrJac = [sparse(dIx, dIx, diagVal), spzeros(5,1),
                spzeros(5,5)]
        @test x / vector == AD(corrVal, corrJac)
        corrVal = 1 ./ corrVal
        diagVal = -vector./x.val.^2
        corrJac = [sparse(dIx, dIx, diagVal), spzeros(5,1),
                spzeros(5,5)]
        @test vector / x == AD(corrVal, corrJac)
    end
    @testset "exp" begin
        corrVal = 4.034287934927351e+02
        corrJac = [spzeros(1,5), [4.034287934927351e+02], spzeros(1,5)]
        @test exp(y) == AD(corrVal, corrJac)
        (x2, y2, z2) = initialize_AD(-1:1, 1, -1.5:0.5:-0.5)
        dIx = collect(1:3)
        corrVal = [4.48168907033806, 1, 0.606530659712633]
        corrJac1 = sparse(dIx, dIx,[-6.722533605507097, -1, -0.303265329856317])
        corrJac2 = [6.722533605507097, 0, -0.303265329856317]
        corrJac3 = sparse(dIx, dIx, [-4.481689070338065, 0, 0.606530659712633])
        corrJac = [corrJac1, corrJac2, corrJac3]
        @test exp(x2*y2*z2) == AD(corrVal, corrJac)
    end
    @testset "index" begin
        corrVal = [2,3]
        corrJac = [sparse([0 1 0 0 0; 0 0 1 0 0]), spzeros(2,1), spzeros(2,5)]
        @test x[2:3] == AD(corrVal, corrJac)
        xTemp = copy(x)
        xTemp[1:5] = z[1:5]
        @test xTemp == z
        xTemp = copy(x)
        xTemp[1:3] = z[1:3]
        @test xTemp[1:3] == z[1:3] && xTemp[4:5] == x[4:5]
    end
    @testset "sum" begin
        corrVal = 15
        corrJac = [[1 1 1 1 1], [0], [0 0 0 0 0]]
        @test sum(x) == AD(corrVal, corrJac)
        corrVal = [22, 23, 24, 25, 26]
        corrJac = [ones(5,5), spzeros(5,1), sparse(I,5,5)]
        @test z + sum(x) == AD(corrVal, corrJac)
    end
    @testset "length" begin
        @test length(x) == 5
        @test length(y) == 1
    end
    @testset "cat" begin
        corrVal = [1,2,3,4,5,7,8,9,10,11,14,16,18,20,22]
        corrJac =  [1 0 0 0 0 0 0 0 0 0 0;
                    0 1 0 0 0 0 0 0 0 0 0;
                    0 0 1 0 0 0 0 0 0 0 0;
                    0 0 0 1 0 0 0 0 0 0 0;
                    0 0 0 0 1 0 0 0 0 0 0;
                    1 0 0 0 0 1 0 0 0 0 0;
                    0 1 0 0 0 1 0 0 0 0 0;
                    0 0 1 0 0 1 0 0 0 0 0;
                    0 0 0 1 0 1 0 0 0 0 0;
                    0 0 0 0 1 1 0 0 0 0 0;
                    1 0 0 0 0 1 1 0 0 0 0;
                    0 1 0 0 0 1 0 1 0 0 0;
                    0 0 1 0 0 1 0 0 1 0 0;
                    0 0 0 1 0 1 0 0 0 1 0;
                    0 0 0 0 1 1 0 0 0 0 1]
        @test cat(x,x+y,x+y+z) == AD(corrVal, corrJac)
    end
end
x_init = collect(1:5)
y_init = 6
z_init = collect(7:11)
(x, y, z) = initialize_AD(x_init, y_init, z_init);
